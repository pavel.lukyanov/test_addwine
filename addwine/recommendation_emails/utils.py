from django.conf import settings

from order.models import Line
from django.template.loader import render_to_string
from django.template import Context, Template
from catalogue.models import Product, ProductAlsoBuying
from .models import RecommendationEmailsSettings
from django.contrib.sites.models import Site


def get_recommended_products_for_line(line, limit, exclude_ids):
    temp = []

    rec_prods = ProductAlsoBuying.objects.filter(primary=line.product.id).\
        exclude(recommendation__id__in=exclude_ids).\
        order_by('ranking')
        
    for rec in rec_prods[:limit]:
        temp.append(rec.recommendation)
        exclude_ids.append(rec.recommendation.id)

    return temp


def recommendation_logic(lines_in_order):
    result_products = []
    count_products = len(lines_in_order)
    exclude_ids = [l.product.id for l in lines_in_order]

    if count_products == 1:
        result_products.extend(get_recommended_products_for_line(lines_in_order[0], 9, exclude_ids))

    elif count_products == 2:
        result_products.extend(get_recommended_products_for_line(lines_in_order[0], 5, exclude_ids))
        result_products.extend(get_recommended_products_for_line(lines_in_order[1], 4, exclude_ids))

    elif count_products == 3:
        for i in range(3):
            result_products.extend(get_recommended_products_for_line(lines_in_order[i], 3, exclude_ids))

    elif count_products == 4:
        result_products.extend(get_recommended_products_for_line(lines_in_order[0], 3, exclude_ids))
        for i in range(1, 4):
            result_products.extend(get_recommended_products_for_line(lines_in_order[i], 2, exclude_ids))

    elif count_products == 5:
        for i in range(4):
            result_products.extend(get_recommended_products_for_line(lines_in_order[i], 2, exclude_ids))
        result_products.extend(get_recommended_products_for_line(lines_in_order[4], 1, exclude_ids))

    elif count_products == 6:
        for i in range(3):
            result_products.extend(get_recommended_products_for_line(lines_in_order[i], 2, exclude_ids))
        for i in range(3, 6):
            result_products.extend(get_recommended_products_for_line(lines_in_order[i], 1, exclude_ids))

    elif count_products == 7:
        for i in range(2):
            result_products.extend(get_recommended_products_for_line(lines_in_order[i], 2, exclude_ids))
        for i in range(2, 7):
            result_products.extend(get_recommended_products_for_line(lines_in_order[i], 1, exclude_ids))

    elif count_products == 8:
        result_products.extend(get_recommended_products_for_line(lines_in_order[0], 2, exclude_ids))
        for i in range(1, 8):
            result_products.extend(get_recommended_products_for_line(lines_in_order[i], 1, exclude_ids))

    elif count_products >= 9:
        for i in range(9):
            result_products.extend(get_recommended_products_for_line(lines_in_order[i], 1, exclude_ids))

    return result_products


def create_email_html(email, re_settings=None):
    if email.sent:
        return email.rendered_html

    if not re_settings:
        re_settings = RecommendationEmailsSettings.get_instance()

    lines_in_order = Line.objects.filter(order=email.order.id).order_by('-line_price_incl_tax').all()
    result_products = recommendation_logic(lines_in_order)

    if len(result_products) < 9:
        result_products.extend(Product.objects.order_by('-rating')[:(9-len(result_products))])

    greeting = Template(re_settings.greeting).render(Context({
        'firstname': email.order.client.first_name,
        'lastname': email.order.client.last_name
    }))

    ctx = {
        'detail_email_url': email.get_absolute_url(),
        'main_image': re_settings.main_image,
        'greeting': greeting,
        'body_text': re_settings.text,
        'recommended_products': result_products[:9],
        'slide': re_settings.slide,
        'article_one': re_settings.article_one,
        'article_two': re_settings.article_two,
        'DOMAIN': 'http://' + Site.objects.get(id=settings.SITE_ID).domain if not settings.DEBUG else 'http://127.0.0.1:8000'
    }

    message = render_to_string('recommendation_emails/recommended_products_email.html', ctx)
    return message


def save_email_as_file(email):
    html = create_email_html(email)
    with open('preview.html', 'w') as f:
        f.write(html)
