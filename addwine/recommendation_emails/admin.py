from oscar.apps.customer.admin import *
from .models import RecommendationEmails, RecommendationEmailsSettings
from django.http import HttpResponseRedirect
from django.urls import reverse


@admin.register(RecommendationEmails)
class RecommendationEmails(admin.ModelAdmin):
    fields = ('order', 'send_date', 'will_send', 'sent_datetime')
    list_display = ('order', 'send_date', 'will_send', 'sent', 'sent_datetime')
    list_filter = ('will_send', 'sent')


@admin.register(RecommendationEmailsSettings)
class RecommendationEmailsSettingsAdmin(admin.ModelAdmin):
    fields = ('main_image', 'subject', 'greeting', 'text', 'slide', 'article_one', 'article_two', 'date_delta')

    def response_change(self, request, obj):
        return HttpResponseRedirect(reverse('admin:index'))

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False
