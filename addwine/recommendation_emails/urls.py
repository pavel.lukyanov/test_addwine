from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'email/show/(?P<email_uuid>[0-9A-Fa-f-]+)', views.show_email_view, name='show-recommendation-email'),
]
