from django.http import HttpResponse, HttpResponseForbidden
from .utils import create_email_html
from .models import RecommendationEmails
from uuid import UUID


def show_email_view(request, email_uuid):
    try:
        email_uuid = UUID(email_uuid, version=4)
        email = RecommendationEmails.objects.get(uuid=email_uuid)
        return HttpResponse(create_email_html(email))
    
    except ValueError or RecommendationEmails.DoesNotExist:
        return HttpResponseForbidden()


