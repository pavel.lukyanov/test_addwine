import logging

from django.conf import settings
from django.core import mail
from django.utils import timezone
from celery import shared_task

from .utils import create_email_html
from .models import RecommendationEmails, RecommendationEmailsSettings

logger = logging.getLogger(__name__)


TASKS_CONFIG_SEND_RECOMMENDATION_EMAILS = {
    'max_retries': 3,
    'default_retry_delay': 30
}


@shared_task(bind=True, **TASKS_CONFIG_SEND_RECOMMENDATION_EMAILS)
def send_recommendation_email(self, email_id):
    email = RecommendationEmails.objects.select_related('order__shipping_contacts').get(pk=email_id)
    try:
        re_settings = RecommendationEmailsSettings.get_instance()
        html = create_email_html(email, re_settings)
        recipients = email.email_address
        mail.send_mail(re_settings.subject, html, settings.FEEDBACK_FROM, [recipients],
                       html_message=html)

        email.sent_datetime = timezone.now()
        email.rendered_html = html
        email.save()
        logger.info('Sent RecommendationEmail<{}> to {}'.format(email.id, email.email_address))
    except Exception as e:
        raise self.retry(exc=e, max_retries=TASKS_CONFIG_SEND_RECOMMENDATION_EMAILS['max_retries'])


@shared_task(**TASKS_CONFIG_SEND_RECOMMENDATION_EMAILS)
def send_recommendation_emails():
    emails_to_send = RecommendationEmails.objects \
        .filter(send_date__lte=timezone.now().date(), will_send=True, sent=False)
    for email in emails_to_send:
        send_recommendation_email.delay(email.pk)
