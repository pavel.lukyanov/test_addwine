from django.template.defaulttags import register


@register.filter
def get_facet_result(facet, value):
    for result in facet['results']:
        if result['name'] == value:
            return result
    return None
