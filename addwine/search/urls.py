from rest_framework import routers
from django.conf.urls import url

from search.views_api import ProductSearchView, save_quertystring, SearchResultView

router = routers.DefaultRouter()
router.register("v1/product/search", ProductSearchView, base_name="product-search")

urlpatterns = router.urls

urlpatterns += [
    url(r"v1/product/search/save", save_quertystring, name='product-search-save'),
    url(r"^search/load/(?P<query>[\w ]+)", SearchResultView.as_view(), name='product-search-load'),
]
