import warnings
from collections import defaultdict

import datetime

import haystack
from django.conf import settings
from haystack import DEFAULT_ALIAS
from haystack.backends.elasticsearch2_backend import Elasticsearch2SearchEngine as Elasticsearch2SearchBaseEngine
from haystack.backends.elasticsearch2_backend import Elasticsearch2SearchQuery as Elasticsearch2SearchBaseQuery
from haystack.backends.elasticsearch2_backend import Elasticsearch2SearchBackend as Elasticsearch2SearchBaseBackend
from haystack.constants import DEFAULT_OPERATOR, DJANGO_CT
from haystack.query import SearchQuerySet
from haystack.utils import get_model_ct


class Elasticsearch2SearchQuerySet(SearchQuerySet):
    def add_aggregation(self, field_name, agg):
        clone = self._clone()
        clone.query.add_aggregation(field_name, agg)
        return clone

    def remove_aggregation(self, field_name, agg):
        clone = self._clone()
        clone.query.remove_aggregation(field_name, agg)
        return clone

    def get_aggregations(self):
        if self.query.has_run():
            return self.query.get_aggregations()
        else:
            clone = self._clone()
            return clone.query.get_aggregations()


class Elasticsearch2SearchBackend(Elasticsearch2SearchBaseBackend):
    def build_search_kwargs(self, query_string, sort_by=None, start_offset=0, end_offset=None, fields='',
                            highlight=False, facets=None, date_facets=None, query_facets=None, narrow_queries=None,
                            spelling_query=None, within=None, dwithin=None, distance_point=None, models=None,
                            limit_to_registered_models=None, result_class=None, **extra_kwargs):
        from search.facets import get_facets
        """
                    Started as complete copy from base class, then optimized for additional functions.
                    """
        index = haystack.connections[self.connection_alias].get_unified_index()
        content_field = index.document_field
        facets_map = get_facets()

        # Compose query items and merge
        queries = []
        if query_string != '*:*':
            queries.append({
                'query_string': {
                    'default_field': content_field,
                    'default_operator': DEFAULT_OPERATOR,
                    'query': query_string,
                    'analyze_wildcard': True,
                    'auto_generate_phrase_queries': True,
                }})
        if not queries and query_string == '*:*':
            queries.append({
                "match_all": {}
            })

        kwargs = {
            'query': self.generate_bool_must(queries),
        }

        # so far, no filters
        filters = []

        if fields:
            if isinstance(fields, (list, set)):
                fields = " ".join(fields)

            kwargs['fields'] = fields

        if sort_by is not None:
            order_list = []
            for f, direction in sort_by:
                if f == 'distance' and distance_point:
                    # Do the geo-enabled sort.
                    lng, lat = distance_point['point'].get_coords()
                    sort_kwargs = {
                        "_geo_distance": {
                            distance_point['field']: [lng, lat],
                            "order": direction,
                            "unit": "km"
                        }
                    }
                else:
                    if f == 'distance':
                        warnings.warn("In order to sort by distance, you must call the '.distance(...)' method.")

                    # Regular sorting.
                    sort_kwargs = {f: {'order': direction}}

                order_list.append(sort_kwargs)

            kwargs['sort'] = order_list

        # From/size offsets don't seem to work right in Elasticsearch's DSL. :/
        # if start_offset is not None:
        #     kwargs['from'] = start_offset

        # if end_offset is not None:
        #     kwargs['size'] = end_offset - start_offset

        if highlight is True:
            kwargs['highlight'] = {
                'fields': {
                    content_field: {'store': 'yes'},
                }
            }

        if self.include_spelling:
            kwargs['suggest'] = {
                'suggest': {
                    'text': spelling_query or query_string,
                    'term': {
                        # Using content_field here will result in suggestions of stemmed words.
                        'field': '_all',
                    },
                },
            }

        narrow_filters = []
        if narrow_queries:
            for q in narrow_queries:
                narrow_filters.append(
                    (q, {
                        'fquery': {
                            'query': {
                                'query_string': {
                                    'query': q
                                },
                            },
                            '_cache': True,
                        }
                    }))

        kwargs['aggs'] = extra_kwargs.pop('aggs', {})

        for key, value in kwargs['aggs'].items():
            facet_filter = [f[1] for f in narrow_filters]
            kwargs['aggs'][key] = {
                'filter': self.generate_bool_must(facet_filter),
                'aggs': {
                    key: value
                }
            }

        # Term aggregations
        if facets is not None:

            for facet_field_name, extra_options in facets.items():
                facet_options = {
                    'terms': {
                        'field': facet_field_name,
                        'size': 50,
                    },
                }
                # Special cases for options applied at the facet level (not the terms level).
                if extra_options.pop('global_scope', False):
                    # Renamed "global_scope" since "global" is a python keyword.
                    facet_options['global'] = True
                    raise NotImplemented(
                        u'Not migrated to aggregations, see http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/search-facets-migrating-to-aggs.html')

                if 'facet_filter' in extra_options:
                    facet_options['facet_filter'] = extra_options.pop('facet_filter')
                    raise NotImplemented(
                        u'facet filters not supported any more through extra_options, used by narrow instead')

                facet_options['terms'].update(extra_options)

                # Set the narrow filters (except the filter for this field) as facet_filter
                facet_field_name_we = facet_field_name[:-6]  # Remove _exact
                facet_filter = self.generate_facet_filter(narrow_filters, facet_field_name,
                                                          facets_map['fields'][facet_field_name_we]['use_and_rule'])
                if facet_filter:
                    kwargs['aggs']['filter.' + facet_field_name] = {
                        'filter': self.generate_bool_must(facet_filter),
                        'aggs': {
                            'terms.' + facet_field_name: facet_options,
                        }
                    }
                    # facet_options['facet_filter'] = self.generate_bool_must(facet_filter)
                    # kwargs['aggregations'][facet_field_name] = facet_options
                else:
                    kwargs['aggs']['terms.' + facet_field_name] = facet_options

        if date_facets is not None:
            kwargs.setdefault('aggs', {})

            for facet_field_name, value in date_facets.items():
                # Need to detect on gap_by & only add amount if it's more than one.
                interval = value.get('gap_by').lower()

                # Need to detect on amount (can't be applied on months or years).
                if value.get('gap_amount', 1) != 1 and not interval in ('month', 'year'):
                    # Just the first character is valid for use.
                    interval = "%s%s" % (value['gap_amount'], interval[:1])

                kwargs['aggs'][facet_field_name] = {
                    'date_histogram': {
                        'field': facet_field_name,
                        'interval': interval,
                    },
                    'facet_filter': {
                        "range": {
                            facet_field_name: {
                                'from': self._from_python(value.get('start_date')),
                                'to': self._from_python(value.get('end_date')),
                            }
                        }
                    }
                }

        if query_facets is not None:
            kwargs.setdefault('aggs', {})

            for facet_field_name, value in query_facets:
                kwargs['aggs'][facet_field_name] = {
                    'filter': {
                        'query': {
                            'query_string': {
                                'query': value,
                            }
                        }
                    }
                }

        if limit_to_registered_models is None:
            limit_to_registered_models = getattr(settings, 'HAYSTACK_LIMIT_TO_REGISTERED_MODELS', True)

        if models and len(models):
            model_choices = sorted(get_model_ct(model) for model in models)
        elif limit_to_registered_models:
            # Using narrow queries, limit the results to only models handled
            # with the current routers.
            model_choices = self.build_models_list()
        else:
            model_choices = []

        if len(model_choices) > 0:
            filters.append({"terms": {DJANGO_CT: model_choices}})

        if within is not None:
            from haystack.utils.geo import generate_bounding_box

            ((south, west), (north, east)) = generate_bounding_box(within['point_1'], within['point_2'])
            within_filter = {
                "geo_bounding_box": {
                    within['field']: {
                        "top_left": {
                            "lat": north,
                            "lon": west
                        },
                        "bottom_right": {
                            "lat": south,
                            "lon": east
                        }
                    }
                },
            }
            filters.append(within_filter)

        if dwithin is not None:
            lng, lat = dwithin['point'].get_coords()
            dwithin_filter = {
                "geo_distance": {
                    "distance": u'{d}km'.format(d=dwithin['distance'].km),
                    dwithin['field']: {
                        "lat": lat,
                        "lon": lng
                    }
                }
            }
            filters.append(dwithin_filter)

        # if we want to filter, change the query type to filtered
        if filters:
            kwargs["query"] = {
                "filtered": {
                    "query": kwargs.pop("query"),
                    "filter": self.generate_bool_must(filters)
                }
            }

        # narrow filters are added as top level filters; otherwise results are filtered before facet counts are calculated
        if narrow_filters:
            kwargs['filter'] = self.generate_bool_must([f[1] for f in narrow_filters])

        return kwargs

    @staticmethod
    def generate_facet_filter(filters, facet_field_name, use_and_rule):
        """
        Each facet needs a filter to include all selections made, except its own
        """
        return [f[1] for f in filters if facet_field_name not in f[0] or use_and_rule]

    @staticmethod
    def generate_bool_must(parts):
        """
        Combine multiple filters into a bool/must clause, or return a single filter
        """
        if len(parts) == 1:
            return parts[0]
        else:
            return {"bool": {"must": parts}}

    @classmethod
    def process_aggregation(cls, facets, d):
        for name, data in d.items():
            try:
                type, facet_fieldname = name.split('.')
                if type == 'terms':
                    facets['fields'][facet_fieldname] = [(bucket['key'], bucket['doc_count']) for bucket in
                                                         data['buckets']]
                if type == 'filter':
                    cls.process_aggregation(facets, data)
            except ValueError as e:
                pass

    def _process_results(self, raw_results, highlight=False,
                         result_class=None, distance_point=None,
                         geo_sort=False):
        results = super(Elasticsearch2SearchBaseBackend, self)._process_results(raw_results, highlight,
                                                                                result_class, distance_point,
                                                                                geo_sort)
        aggregations = {}
        facets = {}
        if 'aggregations' in raw_results:
            facets = {
                'fields': {},
                'dates': {},
                'queries': {},
            }

            self.process_aggregation(facets, raw_results['aggregations'])
            for agg_fieldname, agg_info in raw_results['aggregations'].items():
                if 'meta' in agg_info:
                    facet_type = agg_info['meta']['_type']
                    if facet_type == 'date_histogram':
                        # Elasticsearch provides UTC timestamps with an extra three
                        # decimals of precision, which datetime barfs on.
                        facets['dates'][agg_fieldname] = [
                            (datetime.datetime.utcfromtimestamp(individual['key'] / 1000), individual['doc_count']) for
                            individual in agg_info['buckets']]
                    elif facet_type == 'query':
                        facets['queries'][agg_fieldname] = agg_info['doc_count']

                if '.' in agg_fieldname:
                    type, facet_fieldname = agg_fieldname.split('.')
                    if type == 'terms':
                        facets['fields'][facet_fieldname] = [(bucket['key'], bucket['doc_count']) for bucket in
                                                             agg_info['buckets']]
                    if type == 'filter':
                        self.process_aggregation(facets, agg_info)
                    continue

                field_name, agg_type = agg_fieldname.rsplit('_')
                agg_dict = aggregations.get(field_name)
                if not agg_dict:
                    agg_dict = {}
                    aggregations[field_name] = agg_dict
                agg_dict[agg_type] = agg_info[agg_fieldname]['value']
        results['facets'] = facets
        results['aggregations'] = aggregations
        return results


class Elasticsearch2SearchQuery(Elasticsearch2SearchBaseQuery):
    def __init__(self, using=DEFAULT_ALIAS):
        super(Elasticsearch2SearchQuery, self).__init__(using)
        self.aggs = defaultdict(list)
        self._aggregations = None

    def add_aggregation(self, field_name, aggregation):
        self.aggs[field_name].append(aggregation)

    def remove_aggregation(self, field_name, aggregation):
        self.aggs[field_name].remove(aggregation)

    def build_params(self, spelling_query=None, **kwargs):
        params = super(Elasticsearch2SearchQuery, self).build_params(spelling_query, **kwargs)
        if len(self.aggs.keys()) > 0:
            aggs = {}
            params['aggs'] = aggs
            for field_name, aggs_list in self.aggs.items():
                for agg_type in aggs_list:
                    agg_dict = {}
                    aggs[field_name + "_" + agg_type] = agg_dict
                    agg_dict[agg_type] = {
                        'field': field_name
                    }

        return params

    def get_aggregations(self):
        if self._aggregations is None:
            self.run()

        return self._aggregations

    def run(self, spelling_query=None, **kwargs):
        """Builds and executes the query. Returns a list of search results."""
        final_query = self.build_query()
        search_kwargs = self.build_params(spelling_query, **kwargs)

        if kwargs:
            search_kwargs.update(kwargs)

        results = self.backend.search(final_query, **search_kwargs)
        self._results = results.get('results', [])
        self._hit_count = results.get('hits', 0)
        self._facet_counts = self.post_process_facets(results)
        self._spelling_suggestion = results.get('spelling_suggestion', None)
        self._aggregations = results.get('aggregations', None)

    def _clone(self, klass=None, using=None):
        cloned = super()._clone(klass, using)
        cloned.aggs = self.aggs.copy()
        return cloned


class Elasticsearch2SearchEngine(Elasticsearch2SearchBaseEngine):
    backend = Elasticsearch2SearchBackend
    query = Elasticsearch2SearchQuery
