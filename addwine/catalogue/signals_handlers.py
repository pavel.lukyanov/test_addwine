from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from catalogue.tasks import sync_moisklad_product_info, sync_moisklad_product_price


@receiver(post_save, sender='catalogue.Product')
def sync_product_with_MS(sender, instance, created,  **kwargs):
    if settings.MOI_SKLAD_SYNC_ENABLED and instance.sync_with_moi_sklad:
        if not created:
            sync_moisklad_product_info.delay(instance.id, created)


@receiver(post_save, sender='partner.StockRecord')
def sync_product_price_with_MS(sender, instance, created,  **kwargs):
    from_webhook = getattr(instance, 'from_webhook', False)
    if settings.MOI_SKLAD_SYNC_ENABLED and instance.product.sync_with_moi_sklad:
        if not created and not from_webhook:
            sync_moisklad_product_price.delay(instance.id)



