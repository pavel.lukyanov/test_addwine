from datetime import timedelta
from decimal import Decimal

from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.sites.models import Site
from django.db.models import Max, Prefetch, F
from django.utils import timezone
from django.db.models.signals import post_init
from django.forms import model_to_dict
from oscar.apps.catalogue.abstract_models import *
from django.utils.translation import ugettext_lazy as _
from sortedm2m.fields import SortedManyToManyField
from oscar.models import fields as oscar_fields

from catalogue.signals import product_available
from catalogue.managers import ProductManager as BaseProductManager
from marketing.models import SommelierCTA, MailCTA, SEOText
from offer.models import Range, RangeDiscount
from offer.utils import send_notification_about_discount
from partner.models import StockRecord
from seo.models import AbstractSeoConfigs
from django.db.models.signals import post_save


class Brand(AbstractSeoConfigs, models.Model):
    title = models.CharField('название бренда', max_length=512)
    slug = models.SlugField('адрес страницы')
    logo = models.ImageField('логотип')
    description = RichTextUploadingField('описание')
    order_field = models.PositiveIntegerField(verbose_name='нажать и тащить', default=0)
    date_created = models.DateTimeField(_("Date Created"), auto_now_add=True)

    mail_cta = models.ForeignKey(MailCTA, verbose_name="призыв к действию: рассылка", null=True, blank=True,
                                 on_delete=models.SET_NULL)
    rules_return = RichTextField(verbose_name='условия возврата', default='', blank=True)
    rules_guarantee = RichTextField('гарантия', default='', blank=True)

    def get_absolute_url(self):
        return reverse('brand', args=[self.slug, ])

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'бренд'
        verbose_name_plural = 'бренды'
        indexes = [models.Index(fields=['title'])]
        ordering = ['order_field', ]


class BrandImage(models.Model):
    brand = models.ForeignKey(Brand, verbose_name='бренд', related_name='images')
    img = models.ImageField('изображения')
    order_field = models.PositiveIntegerField(verbose_name='нажать и тащить', default=0)

    class Meta:
        verbose_name = 'изображение'
        verbose_name_plural = 'изображения'
        ordering = ['order_field', ]


class Category(AbstractCategory, AbstractSeoConfigs):
    description = None
    image = None
    mail_cta = models.ForeignKey(MailCTA, verbose_name="призыв к действию: рассылка", null=True, blank=True,
                                 on_delete=models.SET_NULL)
    sommelier_cta = models.ForeignKey(SommelierCTA, verbose_name="призыв к действию: сомелье", null=True, blank=True,
                                      on_delete=models.SET_NULL, related_name='attached_categories')
    rules_guarantee = RichTextField('гарантия', default='', blank=True)
    display_in_bottom_section_order = models.PositiveSmallIntegerField('порядок отображения в нижней секции',
                                                                       blank=True, null=True, unique=True)
    date_created = models.DateTimeField(_("Date Created"), auto_now_add=True)
    is_active = models.BooleanField("отображать на сайте", default=True)
    ms_id = models.UUIDField('ID Мой склад', null=True, default=None)

    def as_dict(self):
        return model_to_dict(self, fields=('name',))

    def get_active_children(self):
        return super().get_children().filter(is_active=True)


class CategorySEOText(models.Model):
    category = models.ForeignKey(Category, verbose_name='категория',
                                 on_delete=models.CASCADE, related_name='seo_texts')
    seo_text = models.ForeignKey(SEOText, verbose_name='SEO тект', related_name='categories')

    def __str__(self):
        return 'SEO текст для категория %s' % self.category.name

    class Meta:
        verbose_name = 'SEO текст'
        verbose_name_plural = 'SEO тексты'


class ProductCategory(AbstractProductCategory):
    def __str__(self):
        return "категория продукта '%s'" % self.product.title

    class Meta:
        app_label = 'catalogue'
        ordering = ['product', 'category']
        unique_together = ('product', 'category')
        verbose_name = _('Product category')
        verbose_name_plural = _('Product categories')


class ProductClass(AbstractProductClass):
    track_stock = models.BooleanField(_("Track stock levels?"), default=False)
    order_field = models.PositiveIntegerField("Нажать и тащить", default=0, blank=True, null=False)

    class Meta:
        app_label = 'catalogue'
        ordering = ['order_field', 'name']
        indexes = [models.Index(fields=['name'])]
        verbose_name = _("Product class")
        verbose_name_plural = _("Product classes")


class ProductManager(BaseProductManager):
    def get_queryset(self):
        return super(ProductManager, self).get_queryset().prefetch_related(
            'children',
            'product_options',
            'product_class__options',
            'stockrecords',
            'images',
            'attribute_values', Prefetch(
                'attribute_values',
                ProductAttributeValue.objects.filter(attribute__display_in_preview=True),
                'preview_attribute_values'
            ))


class Partner(models.Model):
    name = models.CharField(verbose_name='Имя', max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'контрагент'
        verbose_name_plural = 'контрагенты'


class Product(AbstractProduct, AbstractSeoConfigs):
    PACK_OPTION_NAME = 'подарочная упаковка'
    PACK_OPTION_CODE = 'pack'

    objects = ProductManager()

    gtin = models.CharField('GTIN код', max_length=100, null=True, blank=True)
    search_key_words = models.CharField('Поисковые ключевые слова', max_length=250, blank=True, default='',
                                        help_text='Введите ключевые слова для поиска через пробел')
    brand = models.ForeignKey(Brand, verbose_name='бренд', null=True, blank=True, related_name='products',
                              on_delete=models.SET_NULL)
    discount = models.ForeignKey(RangeDiscount, verbose_name='Скидка', null=True, blank=True,
                                 related_name='selected_products', on_delete=models.SET_NULL)
    present = models.ForeignKey('self', verbose_name='Подарок', related_name='present_list', null=True, blank=True, on_delete=models.SET_NULL)
    also_buying_products = models.ManyToManyField(
        'catalogue.Product', through='ProductAlsoBuying', blank=True, related_name='also_buying_products_instances',
        verbose_name='С этим товаром обычно покупают')
    mail_cta = models.ForeignKey(MailCTA, verbose_name="призыв к действию: рассылка", null=True, blank=True,
                                 on_delete=models.SET_NULL)
    sommelier_cta = models.ForeignKey(SommelierCTA, verbose_name="призыв к действию: сомелье", null=True, blank=True,
                                      on_delete=models.SET_NULL, related_name='attached_products')
    partner = models.ForeignKey(Partner, verbose_name='Контрагент', on_delete=models.SET_NULL, null=True, blank=True)
    description = RichTextUploadingField(_('Description'), blank=True)
    rules_guarantee = RichTextField('гарантия', default='', blank=True)
    rules_return = RichTextField(verbose_name='условия возврата', default='', blank=True)

    DDATE_TODAY = 0
    DDATE_TOMORROW = 1
    DDATE_AFTER_TOMORROW = 2
    DDATE_AVAILABLE = 3
    DELIVERY_CHOICES = (
        (None, 'Не указано'),
        (DDATE_TODAY, 'Сегодня'),
        (DDATE_TOMORROW, 'Следующий рабочий день'),
        (DDATE_AFTER_TOMORROW, 'Через рабочий день'),
        (DDATE_AVAILABLE, 'Есть в наличии'),
    )
    delivery_day = models.PositiveSmallIntegerField(verbose_name='Статус возможной доставки',
                                                    choices=DELIVERY_CHOICES, default=None, blank=True, null=True)
    quantity = models.PositiveSmallIntegerField(verbose_name='Кол-во',
                                                default=None, blank=True, null=True)
    forbidden_for_delivery = models.BooleanField(verbose_name='ДП', default=False, blank=True)
    is_available = models.BooleanField('в наличии', default=True)
    is_active = models.BooleanField("отображать", default=True)
    is_exported_in_ym = models.BooleanField("ЯМ", default=True,
                                            help_text='Этот флаг указывает, включена ли синхронизация с Яндекс Маркетом')
    is_exported_in_gm = models.BooleanField("GM", default=False,
                                            help_text='Этот флаг указывает, включена ли синхронизация с Google Мerchant')
    ms_uuid = models.UUIDField("UUID мой склад", default=None, null=True)
    sync_with_moi_sklad = models.BooleanField("МС", default=False,
                                              help_text='Этот флаг указывает, включена ли синхронизация с Моим Складом')
    is_discountable = models.BooleanField(
        "Скидки?", default=True, help_text=_(
            "This flag indicates if this product can be used in an offer "
            "or not"))
    rating = models.FloatField(_('Rating'), default=0, null=True, editable=False)
    exported_to_wl = models.BooleanField("Экспортировано в WL?", default=False)

    @property
    def price(self):
        strategy = Selector().strategy()
        result = None
        if self.is_parent:
            result = strategy.fetch_for_parent(self)
        elif self.has_stockrecords:
            result = strategy.fetch_for_product(self)

        if result:
            if result.price.has_range_discount:
                if result.price.is_tax_known:
                    return result.price.incl_range_discount_incl_tax
                return result.price.excl_range_discount_incl_tax
            else:
                if result.price.is_tax_known:
                    return result.price.incl_tax
                return result.price.excl_tax

    @property
    def base_price(self):
        strategy = Selector().strategy()
        result = None
        if self.is_parent:
            result = strategy.fetch_for_parent(self)
        elif self.has_stockrecords:
            result = strategy.fetch_for_product(self)

        if result:
            if result.price.is_tax_known:
                return result.price.incl_tax
            return result.price.excl_tax

    def get_recommended_products(self):
        return [link.recommendation for link in
                ProductRecommendation.objects.filter(primary=self).select_related(
                    'recommendation', 'recommendation__product_class', 'recommendation__discount')
                    .prefetch_related('recommendation__product_class__options')
                ]

    def get_also_buying_products(self):
        return [link.recommendation for link in
                ProductAlsoBuying.objects.filter(primary=self).select_related(
                    'recommendation', 'recommendation__product_class', 'recommendation__discount')
                    .prefetch_related("recommendation__product_class__options")
                ]

    def get_price(self):
        try:
            return self.stockrecords.first().price_excl_tax
        except AttributeError:
            return self._create_empty_stockrecord().price_excl_tax
    get_price.short_description = 'Цена'

    def get_ms_sync_status(self):
        return bool(self.ms_uuid)
    get_ms_sync_status.short_description = 'Синхр.'
    get_ms_sync_status.boolean = True

    def get_price_retail(self):
        try:
            return self.stockrecords.first().price_retail
        except AttributeError:
            return self._create_empty_stockrecord().price_retail
    get_price_retail.short_description = 'Себест.'

    def _create_empty_stockrecord(self):
        return StockRecord.objects.create(
            product=self,
            price_excl_tax=Decimal('0.00'),
            price_retail=Decimal('0.00'),
            price_currency='руб.'
        )

    def active_images(self):
        return self.images.filter(is_active=True)

    def primary_image(self):
        """
                Returns the primary image for a product. Usually used when one can
                only display one product image, e.g. in a list of products.
                """
        images = self.active_images()
        ordering = self.images.model.Meta.ordering
        if not ordering or ordering[0] != 'display_order':
            # Only apply order_by() if a custom model doesn't use default
            # ordering. Applying order_by() busts the prefetch cache of
            # the ProductManager
            images = images.order_by('display_order')
        try:
            return images[0]
        except IndexError:
            if self.is_child:
                # By default, Oscar's dashboard doesn't support child images.
                # We just serve the parents image instead.
                return self.parent.primary_image()
            else:
                # We return a dict with fields that mirror the key properties of
                # the ProductImage class so this missing image can be used
                # interchangeably in templates.  Strategy pattern ftw!
                return {
                    'original': self.get_missing_image(),
                    'caption': '',
                    'is_missing': True}

    @property
    def active_packs(self):
        return self.packs.filter(is_active=True)

    def as_dict(self):
        data = model_to_dict(self, fields=('title',))
        data['url'] = ''.join(('https://', Site.objects.all()[0].domain, self.get_absolute_url()))
        return data

    def update_discount(self):
        discounts = [discount for discount in RangeDiscount.objects.filter(active=True).select_related('range', )
                     if discount.is_available(update_if_state_changed=False)]
        for discount in discounts:
            if discount.range.all_products().filter(pk=self.pk).exists():
                self.apply_discount(discount)
                break

    def apply_discount(self, discount):
        is_new_discount = self.discount is None and discount is not None

        self.discount = discount
        self.save()

        if is_new_discount:
            send_notification_about_discount(self)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.get_title())
        return super(AbstractProduct, self).save(*args, **kwargs)

    @classmethod
    def period_rating_decrease(cls, multiplier=0.97):
        cls.objects.all().update(rating=F('rating') * multiplier)

    def increment_rating(self, value=1):
        Product.objects.filter(id=self.id).update(rating=F('rating') + value)

    @cached_property
    def default_price(self):
        return Selector().strategy().fetch_for_product(self)

    @staticmethod
    def post_save(sender, **kwargs):
        instance = kwargs.get('instance')
        # Need to look around before playing with next 3 rows
        prev_is_available = instance.prev_is_available
        instance = Product.objects.get(id=instance.id)
        if not prev_is_available and instance.is_available:
            instance.prev_is_available = True
            product_available.send(sender=Product, product=instance)
        option, created = Option.objects.get_or_create(code=Product.PACK_OPTION_CODE,
                                                       defaults={
                                                           'name': Product.PACK_OPTION_NAME,
                                                           'code': Product.PACK_OPTION_CODE,
                                                           'type': Option.OPTIONAL,
                                                       }
                                                       )
        try:
            instance.product_options.get(id=option.id)
        except Option.DoesNotExist:
            instance.product_options.add(option)

    @staticmethod
    def remember_prev_state(sender, **kwargs):
        instance = kwargs.get('instance')
        instance.prev_is_available = instance.is_available


post_save.connect(Product.post_save, sender=Product)
post_init.connect(Product.remember_prev_state, sender=Product)


class ProductAlsoBuying(models.Model):
    primary = models.ForeignKey(
        'catalogue.Product',
        on_delete=models.CASCADE,
        related_name='primary_also_buying',
        verbose_name=_("Primary product"))
    recommendation = models.ForeignKey(
        'catalogue.Product',
        on_delete=models.CASCADE,
        verbose_name=_("Recommended product"),
        related_name='recommendation_also_buying_primaries'
    )
    ranking = models.PositiveSmallIntegerField(
        _('Ranking'), default=0,
        help_text=_('Determines order of the products. A product with a higher'
                    ' value will appear before one with a lower ranking.'))

    def __str__(self):
        return "покупаемый товар для '%s'" % (self.primary.title,)

    class Meta:
        verbose_name = 'С этим товаром так же покупают'
        verbose_name_plural = 'С этим товаром так же покупают'
        unique_together = ('primary', 'recommendation')
        ordering = ['ranking', ]


class ProductRecommendation(AbstractProductRecommendation):
    primary = models.ForeignKey(
        'catalogue.Product',
        on_delete=models.CASCADE,
        related_name='primary_recommendations',
        verbose_name=_("Primary product"))
    recommendation = models.ForeignKey(
        'catalogue.Product',
        on_delete=models.CASCADE,
        verbose_name=_("Recommended product"),
        related_name='recommendation_primaries'
    )

    def __str__(self):
        return "рекомандация для товара '%s'" % (self.primary.title,)

    class Meta:
        verbose_name = 'рекомендуемый продукт'
        verbose_name_plural = 'рекомендуемые продукты'
        unique_together = ('primary', 'recommendation')
        ordering = ['ranking', ]


class ProductImage(AbstractProductImage):
    is_active = models.BooleanField(verbose_name='Отображать на сайте', default=True, blank=True)

    def delete(self, *args, **kwargs):
        """
        Always keep the display_order as consecutive integers. This avoids
        issue #855.
        """
        super(AbstractProductImage, self).delete(*args, **kwargs)
        for idx, image in enumerate(self.product.images.exclude(id=self.id)):
            image.display_order = idx
            image.save()

    def __str__(self):
        return "фотография товара '%s'" % (self.product.title,)

    class Meta:
        ordering = ('display_order',)
        verbose_name = 'фотография товара'
        verbose_name_plural = 'фотографии товара'


class ProductAttribute(AbstractProductAttribute):
    TYPE_CHOICES = (
        (AbstractProductAttribute.TEXT, 'текст'),
        (AbstractProductAttribute.INTEGER, 'целое число'),
        (AbstractProductAttribute.BOOLEAN, 'чекбокс'),
        (AbstractProductAttribute.FLOAT, 'дробное число'),
        # (AbstractProductAttribute.RICHTEXT, 'форматированный текст'),
        (AbstractProductAttribute.DATE, 'дата'),
        (AbstractProductAttribute.DATETIME, 'дата и время'),
        (AbstractProductAttribute.OPTION, 'опция'),
        (AbstractProductAttribute.MULTI_OPTION, 'множественная опция'),
        # (AbstractProductAttribute.FILE, 'файл'),
        # (AbstractProductAttribute.IMAGE, 'изображение'),
    )
    type = models.CharField(
        choices=TYPE_CHOICES, default=TYPE_CHOICES[0][0],
        max_length=20, verbose_name=_("Type"))
    use_and_rule = models.BooleanField(verbose_name='использовать правило "И"', default=False)
    facets_on = models.BooleanField(verbose_name='фильтр', default=True)
    display_in_preview = models.BooleanField(verbose_name='отображать в превью', default=False)
    order_field = models.PositiveIntegerField(verbose_name='нажать и тащить', default=0)
    code = models.SlugField(
        _('Code'), max_length=128,
        validators=[
            RegexValidator(
                regex=r'^[a-zA-Z_][0-9a-zA-Z_]*$',
                message=_(
                    "Код может содержать только буквы a-z, A-Z, числа, "
                    "и _, и не может начинаться с числа")),
            non_python_keyword
        ])

    @staticmethod
    def post_save(sender, **kwargs):
        instance = kwargs.get('instance')
        product_order_exist = True
        try:
            instance.order
        except FacetOrder.DoesNotExist:
            product_order_exist = False
        if instance.prev_code != instance.code or not product_order_exist:
            if product_order_exist:
                product_attrs = ProductAttribute.objects.filter(code=instance.prev_code)
                if len(product_attrs) == 0:
                    instance.order.delete()
                else:
                    instance.order.attribute = product_attrs[0]
                    instance.order.save()

            try:
                FacetOrder.objects.get(attribute__code=instance.code)
            except FacetOrder.DoesNotExist:
                order_field_max = FacetOrder.objects.aggregate(max_order=Max('order_field'))['max_order'] or 0
                FacetOrder.objects.create(attribute=instance, order_field=order_field_max + 1)

    @staticmethod
    def remember_prev_state(sender, **kwargs):
        instance = kwargs.get('instance')
        instance.prev_code = instance.code

    class Meta:
        ordering = ('order_field',)
        indexes = [
            models.Index(fields=['code']),
            models.Index(fields=['type']),
            models.Index(fields=['facets_on', 'type']),
        ]
        verbose_name = 'атрибут товара'
        verbose_name_plural = 'атрибуты товара'


post_save.connect(ProductAttribute.post_save, sender=ProductAttribute)
post_init.connect(ProductAttribute.remember_prev_state, sender=ProductAttribute)


class ProductAttributeValueManager(models.Manager):
    def get_queryset(self):
        return super(ProductAttributeValueManager, self).get_queryset() \
            .select_related('attribute', 'value_option') \
            .prefetch_related('value_multi_option')


class ProductAttributeValue(AbstractProductAttributeValue):
    objects = ProductAttributeValueManager()

    value_multi_option = SortedManyToManyField(
        'catalogue.AttributeOption', blank=True,
        related_name='multi_valued_attribute_values',
        verbose_name=_("Value multi option"))

    def is_empty(self):
        return False if self.value else True

    class Meta:
        app_label = 'catalogue'
        unique_together = ('attribute', 'product')
        ordering = ('attribute__order_field',)
        indexes = [
            models.Index(fields=['product']),
            models.Index(fields=['attribute']),
            models.Index(fields=['value_option']),
            models.Index(fields=['value_integer']),
            models.Index(fields=['value_float']),
        ]
        verbose_name = _('Product attribute value')
        verbose_name_plural = _('Product attribute values')


class AttributeOption(AbstractAttributeOption):
    use_in_filter = models.BooleanField('использовать в фильтрах', default=True)
    order_field = models.PositiveIntegerField(verbose_name='нажать и тащить', default=0)

    class Meta:
        ordering = ('order_field',)
        app_label = 'catalogue'
        unique_together = ('group', 'option')
        indexes = [
            models.Index(fields=['use_in_filter']),
        ]
        verbose_name = _('Attribute option')
        verbose_name_plural = _('Attribute options')


from oscar.apps.catalogue.models import *


class Pack(models.Model):
    product = models.ForeignKey(Product, verbose_name='товар', on_delete=models.CASCADE, related_name='packs')
    count = models.PositiveIntegerField('количество товаров')
    price = models.DecimalField('стоимось', max_digits=12, decimal_places=2)
    price_retail = models.DecimalField('себестоимость', max_digits=12, decimal_places=2)
    is_active = models.BooleanField('активно', default=True)

    @property
    def actual_price(self):
        price = self.get_price()

        if price.has_range_discount:
            if price.is_tax_known:
                price = price.incl_range_discount_incl_tax
            else:
                price = price.excl_range_discount_incl_tax
        else:
            if price.is_tax_known:
                price = price.incl_tax
            else:
                price = price.excl_tax
        return price

    @property
    def base_price(self):
        price = self.get_price()

        if price.is_tax_known:
            price = price.incl_tax
        else:
            price = price.excl_tax
        return price

    def get_price(self):
        strategy = Selector().strategy()
        purchase_info = strategy.fetch_for_product(self.product)
        purchase_info.price.excl_tax = self.price
        return purchase_info.price

    def active_images(self):
        return self.images.filter(is_active=True)

    def primary_image(self):
        images = self.active_images()
        ordering = self.images.model._meta.ordering
        if not ordering or ordering[0] != 'display_order':
            images = images.order_by('display_order')
        try:
            return images[0]
        except IndexError:
            return {
                'original': self.get_missing_image(),
                'caption': '',
                'is_missing': True}

    def get_missing_image(self):
        return MissingProductImage()

    def __str__(self):
        return 'комплект из %s шт.' % (self.count,)

    class Meta:
        verbose_name = 'комплект'
        verbose_name_plural = 'комплекты'


class PackImage(models.Model):
    pack = models.ForeignKey(
        Pack,
        on_delete=models.CASCADE,
        related_name='images',
        verbose_name='комплект')
    original = models.ImageField(
        _("Original"), upload_to=settings.OSCAR_IMAGE_FOLDER, max_length=255)
    caption = models.CharField(_("Caption"), max_length=200, blank=True)
    is_active = models.BooleanField('Отображать на сайте', default=True, blank=True)

    display_order = models.PositiveIntegerField(
        _("Display order"), default=0,
        help_text=_("An image with a display order of zero will be the primary"
                    " image for a product"))
    date_created = models.DateTimeField(_("Date created"), auto_now_add=True)

    class Meta:
        ordering = ["display_order"]
        verbose_name = 'изображение'
        verbose_name_plural = 'изображения'

    def __str__(self):
        return "изображение для '%s'" % self.pack

    def is_primary(self):
        return self.display_order == 0

    def delete(self, *args, **kwargs):
        super(PackImage, self).delete(*args, **kwargs)
        for idx, image in enumerate(self.pack.images.all()):
            image.display_order = idx
            image.save()


class FacetOrder(models.Model):
    attribute = models.OneToOneField(ProductAttribute, verbose_name='аттрибут',
                                     on_delete=models.CASCADE, related_name='order')
    order_field = models.PositiveIntegerField(verbose_name='нажать и тащить', default=0, blank=False, null=False)

    class Meta:
        ordering = ('order_field',)
        verbose_name = 'порядок'
        verbose_name_plural = 'порядок фильтров'


class OnProductAvailableDiscountInfo(models.Model):
    STAFF_RANGE_SLUG_PREFIX = 'staff_range_for_discount_'
    STAFF_DISCOUNT_RANGE_SLUG_PREFIX = 'staff_discount_range_'

    product = models.ForeignKey(Product, related_name='on_product_available_discount_info', on_delete=models.CASCADE)

    PERCENTAGE, FIXED = ("Percentage", "Absolute",)
    TYPE_CHOICES = (
        (PERCENTAGE, _("Discount is a percentage off of the product's value")),
        (FIXED, _("Discount is a fixed amount off of the product's value")),
    )
    type = models.CharField(
        _("Type"), max_length=128, choices=TYPE_CHOICES)

    # The value to use with the designated type
    value = oscar_fields.PositiveDecimalField(
        _("Value"), decimal_places=2, max_digits=12)

    hours_count = models.PositiveIntegerField(verbose_name='Время действия с начала акции (в часах)')

    def create_range_discount(self):
        start_datetime = timezone.now()

        range_slug = self.STAFF_RANGE_SLUG_PREFIX + str(self.product.id)

        range_for_discount, created = Range.objects.get_or_create(slug=range_slug, defaults={
            'name': range_slug,
            'hidden_in_admin': True
        })

        range_for_discount.included_products.clear()
        range_for_discount.add_product(self.product)
        range_for_discount.save()

        discount_range_slug = self.STAFF_DISCOUNT_RANGE_SLUG_PREFIX + str(self.product.id)

        range_discount, created = RangeDiscount.objects.get_or_create(
            code=discount_range_slug,
            defaults={
                'range': range_for_discount,
                'name': self.product.title,
                'type': self.type,
                'value': self.value,
                'hidden_in_admin': True,
                'display_for_users': False,
                'end_datetime': start_datetime + timedelta(hours=self.hours_count)
            }
        )

        if created:
            return range_discount

        range_discount.type = self.type
        range_discount.value = self.value
        range_discount.end_datetime = start_datetime + timedelta(hours=self.hours_count)
        range_discount.save()
        return range_discount

    def __str__(self):
        return "акция на товар '%s'" % self.product.title

    class Meta:
        verbose_name = 'Акция при поступлении товара в продажу'
        verbose_name_plural = 'Акции при поступлении товара в продажу'
