from .base import BaseApi
from .utills import build_base64_dict, get_group_for_product, get_buy_price, get_sale_prices

import pprint

class MoysklProductAPI(BaseApi):
    PRODUCT_URL = 'product'

    def build_url(self, uuid=None, upc=None):
        url = self.URL + self.PRODUCT_URL
        if uuid:
            url += '/' + str(uuid)
        elif upc:
            url += '?filter=article={}'.format(upc)
        return url

    def create_product(self, p):
        data = {
            'name': p.title,
            'code': p.upc,
            'article': p.upc,
        }

        data.update(get_group_for_product(p))
        data.update(build_base64_dict(p.primary_image()))
        data.update(get_buy_price(p))
        data.update(get_sale_prices(p))

        resp = self.post(self.build_url(), data=data)
        if resp:
            p.ms_uuid = resp.json()['id']
            p.save()

        return resp

    def update_product_all(self, p):
        if not p.ms_uuid:
            return None

        data = {
            'name': p.title,
            'code': p.upc,
            'article': p.upc,
        }

        data.update(get_group_for_product(p))
        data.update(build_base64_dict(p.primary_image()))
        data.update(get_buy_price(p))
        data.update(get_sale_prices(p))

        resp = self.put(self.build_url(p.ms_uuid), data=data)
        return resp

    def update_product_price(self, p):
        if not p.ms_uuid:
            return None

        data = {}
        data.update(get_buy_price(p))
        data.update(get_sale_prices(p))

        resp = self.put(self.build_url(p.ms_uuid), data=data)
        return resp

    def update_product_info(self, p):
        if not p.ms_uuid:
            return None
        data = {
            'name': p.title,
            'code': p.upc,
            'article': p.upc,
        }

        data.update(get_group_for_product(p))
        data.update(build_base64_dict(p.primary_image()))

        resp = self.put(self.build_url(p.ms_uuid), data=data)
        return resp

    def fetch_product_by_upc(self, p):
        resp = self.get(self.build_url(upc=p.upc))
        try:
            uuid = resp.json()['rows'][0]['id']
            p.ms_uuid = uuid
            p.save()
            return p
        except IndexError:
            return None

    def first_time_sync(self, p):
        ret = self.fetch_product_by_upc(p)
        if ret:
            self.update_product_all(ret)
        else:
            self.create_product(p)

    def get_product_by_url(self, url):
        return self.get(url)
