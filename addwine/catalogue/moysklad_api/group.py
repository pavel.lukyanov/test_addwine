from .base import BaseApi, logger


class MoyskladGroupAPI(BaseApi):
    GROUP_URL_FILTER = 'productfolder?filter=name={}'

    def build_url(self, name):
        return self.URL + self.GROUP_URL_FILTER.format(name)

    def get_productfolder_id(self, c):
        resp = self.get(self.build_url(c.name))
        try:
            productfolder_id = resp.json()['rows'][0]['id']
            c.ms_id = productfolder_id
            c.save()
            return productfolder_id
        except (KeyError, IndexError):
            print('Group {} does not exists'.format(c.name))
            return None








