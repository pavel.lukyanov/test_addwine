import json
import warnings
from collections import OrderedDict
from math import floor

from cacheops import cached_view_as, cached_view
from django.conf import settings
from django.core.paginator import InvalidPage
from django.db.models import prefetch_related_objects, Prefetch
from django.http.response import JsonResponse, HttpResponse
from django.template import loader
from django.urls import reverse
from django.utils.datastructures import MultiValueDict
from oscar.apps.analytics.scores import ProductRecord
from oscar.apps.catalogue import views as catalogue_views
from oscar.apps.customer import history

from catalogue.models import Product, Category, ProductAttribute, ProductRecommendation, ProductAlsoBuying, get_model, \
    AttributeOption
from common.models import AboutCompany, SiteConfiguration
from common.views import PageViewMixin
from marketing.forms import SommelierCTAForm
from marketing.models import SEOText
from marketing.views import SommelierCardsMixin
from offer.forms import SubscriptionForDiscountForm
from offer.models import Range, RangeGroup
from seo.views import SeoParamsMixin
from web.mixins import MenuViewMixin


def get_field_from_category(category, field_name):
    data = getattr(category, field_name)
    if data:
        return data
    for parent in category.get_ancestors().reverse():
        data = getattr(parent, field_name)
        if data:
            return data

    return None


def get_field_from_product_or_parents(product, request_params, field_name, additional_lookups=None):
    data = getattr(product, field_name)
    if additional_lookups:
        for additional_lookup in additional_lookups:
            data = getattr(additional_lookup, field_name)
            if data:
                return data
    if not data:
        if request_params.get('range'):
            try:
                range = Range.objects.get(slug=request_params.get('range'))
                data = getattr(range, field_name)
            except Range.DoesNotExist:
                pass

        if not data:
            if request_params.get('category'):
                try:
                    category = Category.objects.get(id=request_params.get('category'))
                except Category.DoesNotExist:
                    category = None

                if category:
                    data = get_field_from_category(category, field_name)

            if not data:
                categories = product.categories.all()
                for category in categories:
                    if getattr(category, field_name):
                        data = getattr(category, field_name)
                        break

                if not data:
                    for category in categories:
                        data = get_field_from_category(category, field_name)
                        if data:
                            break

    return data


class PopularProductsMixin(object):
    def get_context_data(self, **kwargs):
        context = super(PopularProductsMixin, self).get_context_data(**kwargs)
        count = settings.POPULAR_PRODUCTS_COUNT
        context['popular_products'] = Product.objects.filter(id__in=ProductRecord.objects
                                                             .filter(product__is_active=True).order_by('score')[:count]
                                                             .values_list('product')).select_related('product_class', 'discount')
        return context


class RecentlyViewedProductsMixin(object):
    def get_context_data(self, **kwargs):
        context = super(RecentlyViewedProductsMixin, self).get_context_data(**kwargs)
        context['recently_viewed_products'] = [product for product in history.get(self.request) if product.is_active]
        return context


class CataloguePageMixin(SommelierCardsMixin, PageViewMixin, MenuViewMixin, RecentlyViewedProductsMixin, object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_sommelier_cta'] = SommelierCTAForm()
        context['catalogue_menu'] = RangeGroup.objects.filter(depth=1)

        return context


class CatalogueSearchMixin(object):
    AJAX_PRODUCTS_TEMPLATE = 'web/catalogue/partials/product_category_list.html'

    def get(self, request, *args, **kwargs):
        if hasattr(self, 'get_category'):
            self.category = self.get_category()
        self.base_search_handler = self.get_search_handler(
            MultiValueDict(), self.request.get_full_path(),
            **self.get_search_kwargs())
        if request.is_ajax():
            try:
                self.search_handler = self.get_search_handler(
                    self.request.GET, request.get_full_path(),
                    **self.get_search_kwargs())
            except InvalidPage:
                return JsonResponse({})
            context = self.get_context_data(**kwargs)
            return HttpResponse(context['filters_state_json'], content_type='application/json')
        return super(CatalogueSearchMixin, self).get(request, *args, **kwargs)

    def get_search_kwargs(self):
        return {}

    def sort_arttributes_dict(self, attributes_dict, attributes_order_map):
        return OrderedDict(sorted(attributes_dict.items(),
                                  key=lambda item: attributes_order_map[item[0]]))

    def create_attributes_order_map(self):
        order_map = {}
        cur_index = 0
        for key in settings.OSCAR_SEARCH_FACETS['fields'].keys():
            order_map[key] = cur_index
            cur_index += 1

        for key in settings.SEARCH_RANGES.keys():
            order_map[key] = cur_index
            cur_index += 1

        for attr in ProductAttribute.objects.filter(
                type__in=[ProductAttribute.INTEGER, ProductAttribute.FLOAT,
                          ProductAttribute.OPTION, ProductAttribute.MULTI_OPTION]
        ).values('code', 'order__order_field'):
            if attr['order__order_field'] is not None:
                order_map[attr['code']] = cur_index + attr['order__order_field']

        return order_map

    def sort_variants(self, variants_order, variants):
        variants.sort(key=lambda variant: variants_order.get(variant['name'], 0))

    def patch_context(self, context, render_products):
        attributes_order_map = self.create_attributes_order_map()

        context['base_filter'] = self.base_search_handler.get_search_context_data()
        context['base_filter']['facet_data'] = self.sort_arttributes_dict(context['base_filter']['facet_data'],
                                                                          attributes_order_map)
        facet_data = context['base_filter']['facet_data']
        primary_facet_data = []
        for key, search_facet in settings.OSCAR_SEARCH_FACETS['fields'].items():
            primary_facet = facet_data.pop(key)
            primary_facet_data.append((key, primary_facet))

            app_label, app_name = search_facet['model'].split('.')
            SearchModel = get_model(app_label, app_name)
            variants_order = {str(search_model): search_model.order_field for search_model in SearchModel.objects.all()}
            self.sort_variants(variants_order, primary_facet['results'])

        for key, search_facet in facet_data.items():
            ao_list = AttributeOption.objects.filter(group__name=search_facet['name'], use_in_filter=True)\
                .values('option', 'order_field')
            items_order = {ao['option']: ao['order_field'] for ao in ao_list}
            search_facet['results'] = [sf for sf in search_facet['results'] if sf['name'] in items_order]
            self.sort_variants(items_order, search_facet['results'])

        context['base_filter']['facet_data'] = {
            'primary': OrderedDict(primary_facet_data),
            'attrs': OrderedDict(facet_data)
        }
        context['base_filter']['range_data'] = self.sort_arttributes_dict(context['base_filter']['range_data'],
                                                                          attributes_order_map)

        template = loader.get_template(self.AJAX_PRODUCTS_TEMPLATE)
        context['filters_state_json'] = {
            'facet_data': context['facet_data'],
            'range_data': context['range_data'],
            'count': context['paginator'].count,
            'num_pages': context['paginator'].num_pages,
        }

        if render_products:
            context['filters_state_json']['products_html'] = template.render(
                {'products': context['products']}, self.request)

        context['filters_state_json'] = json.dumps(context['filters_state_json'])

        return context

    def get_context_data(self, **kwargs):
        context = super(CatalogueSearchMixin, self).get_context_data(**kwargs)

        return self.patch_context(context, self.request.is_ajax())


class CatalogueView(CatalogueSearchMixin, CataloguePageMixin, catalogue_views.CatalogueView):
    page_code = 'catalogue'
    template_name = 'catalogue/category.html'


class ProductCategoryView(CatalogueSearchMixin, CataloguePageMixin, SeoParamsMixin,
                          catalogue_views.ProductCategoryView):
    def get_search_kwargs(self):
        return {'categories': self.get_categories()}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category = context['category']
        context['summary'] = category.name
        context.update(self.extract_seo_configs(category))
        context['seo_texts'] = SEOText.objects.filter(categories__in=category.seo_texts.all())
        context['mail_cta'] = get_field_from_category(category, 'mail_cta')
        context['product_parent_pointer'] = '?category=%s' % (category.id,)

        context['breadcrumbs'] = [
            {
                'title': 'каталог',
                'href': reverse('catalogue:index')
            },
        ]

        for cur_category in category.get_ancestors_and_self():
            context['breadcrumbs'].append({
                'title': cur_category.name,
                'href': cur_category.get_absolute_url()
            })

        return context


class ProductDetailView(CataloguePageMixin, SeoParamsMixin, catalogue_views.ProductDetailView):
    def get_context_data(self, **kwargs):
        context = super(ProductDetailView, self).get_context_data(**kwargs)
        product = context['product']
        product.increment_rating()

        context['form_subscribe_for_discount'] = SubscriptionForDiscountForm()

        sommelier_cta = get_field_from_product_or_parents(product, self.request.GET, 'sommelier_cta')
        context['sommelier_cta'] = sommelier_cta

        rules_guarantee = get_field_from_product_or_parents(product, self.request.GET, 'rules_guarantee',
                                                            [product.brand])
        if not rules_guarantee:
            rules_guarantee = AboutCompany.get_instance().rules_guarantee
        context['rules_guarantee'] = rules_guarantee

        rules_return = product.rules_return
        if not rules_return:
            rules_return = product.brand.rules_return
        if not rules_return:
            rules_return = AboutCompany.get_instance().rules_return
        context['rules_return'] = rules_return

        attribute_values = product.attribute_values.all()
        attribute_values = [attr_value for attr_value in attribute_values if not attr_value.is_empty() \
                            and not attr_value.attribute.display_in_preview]

        center = int(floor(len(attribute_values) / 2))
        context['left_column'] = []
        context['right_column'] = []
        context['attribute_values'] = [attribute_value for attribute_value in attribute_values if
                                       attribute_value.attribute.display_in_preview]
        for i, value in enumerate(attribute_values):
            context['left_column' if i < center else 'right_column'].append(value)

        if len(attribute_values) % 2 != 0:
            context['column_alone_element'] = context['right_column'].pop()

        context.update(self.extract_seo_configs(product))
        context['mail_cta'] = get_field_from_product_or_parents(product, self.request.GET, 'mail_cta')

        context['breadcrumbs'] = [
            {
                'title': 'каталог',
                'href': reverse('catalogue:index')
            },
        ]

        category_id = self.request.GET.get('category')
        range_slug = self.request.GET.get('range')
        if category_id:
            try:
                category = Category.objects.get(id=category_id)
            except Category.DoesNotExist:
                category = None

            if category:
                for cur_category in category.get_ancestors_and_self():
                    context['breadcrumbs'].append({
                        'title': cur_category.name,
                        'href': cur_category.get_absolute_url()
                    })
        elif range_slug:
            try:
                range = Range.objects.get(slug=range_slug)
            except Range.DoesNotExist:
                range = None

            if range:
                context['breadcrumbs'].append({
                    'title': range.name,
                    'href': range.get_absolute_url()
                })

        context['breadcrumbs'].append({
            'title': product.title,
            'href': product.get_absolute_url()
        })

        context['recommended_products'] = [link.recommendation for link in
                                           ProductRecommendation.objects.filter(primary=product).select_related(
                                               "recommendation", 'recommendation__product_class', 'recommendation__discount')
                                                .prefetch_related("recommendation__product_class__options")
                                           ]
        context['also_buying_products'] = [link.recommendation for link in
                                           ProductAlsoBuying.objects.filter(primary=product).select_related(
                                               "recommendation", 'recommendation__product_class', 'recommendation__discount')
                                               .prefetch_related("recommendation__product_class__options")
                                           ]

        return context

    def get_alert_status(self):
        return None

    def get_alert_form(self):
        return None


product_detail_view_cached = cached_view(ProductDetailView.as_view())
