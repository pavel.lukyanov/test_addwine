from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from catalogue.models import Brand, Product, ProductAttributeValue, Pack, ProductClass, Partner, ProductCategory, \
    Category, ProductImage, OnProductAvailableDiscountInfo
from customer.models import ProductAlert
from partner.models import StockRecord


class ProductAttributeValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductAttributeValue
        fields = '__all__'


class ProductAlertSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductAlert
        fields = ('first_name', 'email', 'phone_number', 'count', 'product')
        extra_kwargs = {
            'first_name': {'required': True},
            'email': {'required': True},
        }
        validators = [
            UniqueTogetherValidator(
                queryset=ProductAlert.objects.all(),
                fields=['email', 'product'],
                message='Вы уже оформили заявку на предзаказ этого товара'
            )
        ]

class BrandListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ('title', 'slug', 'logo')

    def to_representation(self, instance):
        rpr = super().to_representation(instance)
        if instance.logo:
            rpr['logo'] = instance.logo.name
        return rpr


class RangeSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    slug = serializers.CharField(max_length=256)


class SubRangeGroupSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=1024)
    items = RangeSerializer(source='ranges', many=True)


class RangeGroupSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=1024)
    subcategories = SubRangeGroupSerializer(source='get_children', many=True)


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImage
        fields = ('original', 'caption', 'display_order', 'is_active')

    def to_representation(self, instance):
        rpr = super().to_representation(instance)
        if instance.original:
            rpr['original'] = instance.original.name
        return rpr


class PackSerializer(serializers.ModelSerializer):
    images = ImageSerializer(many=True)

    class Meta:
        model = Pack
        fields = ('id', 'count', 'price', 'price_retail', 'is_active', 'images')


class ProductClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductClass
        fields = ('name', 'slug', 'requires_shipping', 'track_stock', 'order_field')


class PartnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Partner
        fields = ('name',)


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('name', 'rules_guarantee', 'slug', 'display_in_bottom_section_order', 'is_active', 'ms_id',
                  'sommelier_cta', 'mail_cta')


class ProductCategorySerializer(serializers.ModelSerializer):
    category = CategorySerializer(read_only=True)

    class Meta:
        model = ProductCategory
        fields = ('category',)


class StockRecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = StockRecord
        fields = ('price_currency', 'price_excl_tax', 'price_retail')


class OnProductAvailableDiscountInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = OnProductAvailableDiscountInfo
        fields = ('type', 'value', 'hours_count')


class ProductDetailSerializer(serializers.ModelSerializer):
    title = serializers.CharField(source='get_title')
    price = serializers.CharField(source='get_price')
    images = ImageSerializer(many=True)
    present = serializers.SlugRelatedField(slug_field='upc', read_only=True)
    discount = serializers.SlugRelatedField(slug_field='code', read_only=True)
    mail_cta = serializers.PrimaryKeyRelatedField(read_only=True)
    sommelier_cta = serializers.PrimaryKeyRelatedField(read_only=True)
    partner = PartnerSerializer(read_only=True)
    product_class = ProductClassSerializer(read_only=True)
    productcategory_set = ProductCategorySerializer(many=True, read_only=True)
    stockrecords = StockRecordSerializer(many=True, read_only=True)
    on_product_available_discount_info = OnProductAvailableDiscountInfoSerializer(many=True, read_only=True)

    brand = BrandListSerializer(read_only=True)
    attribute_values = ProductAttributeValueSerializer(many=True, read_only=True)
    packs = PackSerializer(many=True)
    recommendations = serializers.SlugRelatedField(source='get_recommended_products', slug_field='upc', many=True, read_only=True)
    also_buying = serializers.SlugRelatedField(source='get_also_buying_products', slug_field='upc', many=True, read_only=True)

    class Meta:
        model = Product
        fields = ('id', 'title', 'slug', 'upc', 'product_class', 'price', 'base_price', 'quantity', 'gtin',
                  'seo_title', 'seo_description', 'seo_keywords', 'seo_og_title', 'seo_og_description', 'seo_og_image',
                  'search_key_words', 'productcategory_set', 'stockrecords', 'on_product_available_discount_info',
                  'discount', 'mail_cta', 'sommelier_cta', 'partner', 'rules_guarantee', 'rules_return',
                  'delivery_day', 'forbidden_for_delivery', 'is_available', 'is_active', 'is_exported_in_ym',
                  'is_exported_in_gm', 'ms_uuid', 'sync_with_moi_sklad', 'is_discountable', 'rating',
                  'is_available', 'delivery_day', 'forbidden_for_delivery',
                  'brand', 'images', 'present',
                  'attribute_values', 'packs', 'description', 'recommendations', 'also_buying')

    def to_representation(self, instance):
        rpr = super().to_representation(instance)
        if instance.seo_og_image:
            rpr['seo_og_image'] = instance.seo_og_image.name
        return rpr
