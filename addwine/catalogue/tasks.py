from celery import shared_task
from django.conf import settings
from catalogue.models import Product

from search.tasks import update_index
from catalogue.moysklad_api.assortiment import MoysklAssortmentAPI
from catalogue.moysklad_api.product import MoysklProductAPI
from partner.models import StockRecord

TASKS_CONFIGS_UPDATE_RATING = {
    'name': "catalogue_rating.update",
    'max_retries': 2
}
TASKS_CONFIGS_UPDATE_RATING.update(settings.CATALOGUE_RATING_TASK_CONFIG)


@shared_task(**TASKS_CONFIGS_UPDATE_RATING)
def period_rating_decrease():
    # Decrease rating for products
    Product.period_rating_decrease()
    # Update index with new rating
    update_index.apply_async(countdown=1)


TASKS_CONFIG_PULL_MOYSKLAD_ASSORIMENT = {
    'max_retries': 3,
    'default_retry_delay': 30
}


@shared_task(**TASKS_CONFIG_PULL_MOYSKLAD_ASSORIMENT)
def pull_moisklad_assortiment():
    MoysklAssortmentAPI().scan_assortiment(force=True)


TASKS_CONFIG_SYNC_MOYSKLAD = {
    'max_retries': 3,
    'default_retry_delay': 30
}


@shared_task(**TASKS_CONFIG_SYNC_MOYSKLAD)
def sync_moisklad_product_info(product_id, created):
    instance = Product.objects.get(pk=product_id)
    if not instance.ms_uuid:
        MoysklProductAPI().first_time_sync(instance)
    else:
        MoysklProductAPI().update_product_all(instance)


@shared_task(**TASKS_CONFIG_SYNC_MOYSKLAD)
def sync_moisklad_product_price(info_id):
    instance = StockRecord.objects.get(pk=info_id)
    MoysklProductAPI().update_product_price(instance.product)
