from django.template.defaulttags import register
from django.utils import timezone
from datetime import time, timedelta
from catalogue.models import Product
from django.template.defaultfilters import date as _date


@register.inclusion_tag('catalogue/get_delivery_day.html')
def get_delivery_day(product):
    current_datetime = timezone.localtime()
    dayofweek = current_datetime.isoweekday()
    current_time = current_datetime.time()
    default = None
    if product.is_available:
        if product.quantity and product.quantity >= 1 or product.delivery_day == Product.DDATE_AVAILABLE:
            delivery_status = delivery_logic_with_moysklad(dayofweek, current_time)
            pickup_status = pickup_logic_with_moysklad(dayofweek, current_time)
        elif product.delivery_day == Product.DDATE_TODAY:
            delivery_status = delivery_logic_admin_today(dayofweek, current_time)
            pickup_status = pickup_logic_admin_today(dayofweek, current_time)
        elif product.delivery_day == Product.DDATE_AFTER_TOMORROW:
            delivery_status = delivery_logic_admin_two_days(dayofweek, current_time)
            pickup_status = pickup_logic_admin_two_days(dayofweek, current_time)
        else:
            delivery_status = delivery_logic_admin_nextday(dayofweek, current_time)
            pickup_status = pickup_logic_admin_nextday(dayofweek, current_time)
    else:
        delivery_status = None
        pickup_status = None
        default = 'по предзаказу'

    output = {
        'delivery': delivery_status,
        'pickup': pickup_status,
        'default': default
    }
    return output


def next_workday(current_daytime):
    if current_daytime.isoweekday() in [1, 2, 3, 4]:
        return current_daytime.date() + timedelta(days=1)
    else:
        return current_daytime.date() + timedelta(days=(7-current_daytime.weekday()))


DELIVERY_AFTER_12 = 'доставим после 12.00'
DELIVERY_AFTER_TWO_HOURS = 'доставим через 2-3 часа'
DELIVERY_TODAY = 'доставим сегодня'
DELIVERY_TOMORROW = 'доставим завтра'
DELIVERY_AFTER_TOMORROW = 'доставим послезавтра'
DELIVERY_AFTER_TWO_DAYS = 'доставим через 2 дня'
DELIVERY_AFTER_THREE_DAYS = 'доставим через 3 дня'
DELIVERY_AFTER_FOUR_DAYS = 'доставим через 4 дня'

PICKUP_TODAY_12_20 = 'самовывоз сегодня с 12.00 до 20.00'
PICKUP_TODAY_16_20 = 'самовывоз сегодня с 16.00 до 20.00'
PICKUP_NOW = 'самовывоз сейчас'
PICKUP_TOMORROW = 'самовывоз завтра'
PICKUP_TOMORROW_16_20 = 'самовывоз завтра с 16.00 до 20.00'
PICKUP_AFTER_TOMORROW = 'самовывоз послезавтра'
PICKUP_AFTER_TWO_DAYS = 'самовывоз через 2 дня'
PICKUP_AFTER_THREE_DAYS = 'самовывоз через 3 дня'
PICKUP_AFTER_FOUR_DAYS = 'самовывоз через 4 дня'

time00 = time(hour=0, minute=0)
time11 = time(hour=11, minute=0)
time12 = time(hour=12, minute=0)
time14 = time(hour=14, minute=0)
time17 = time(hour=17, minute=0)
time20 = time(hour=20, minute=0)
workdays = [1, 2, 3, 4, 5]
holidays = [6, 7]

time_in_00_11 = lambda t: time00 <= t <= time11
time_in_12_17 = lambda t: time12 <= t <= time17
time_in_00_12 = lambda t: time00 <= t <= time12
time_in_00_14 = lambda t: time00 <= t <= time14
time_in_12_20 = lambda t: time12 <= t <= time20


def delivery_logic_with_moysklad(dayofweek, current_time):
    if dayofweek in workdays and time_in_00_12(current_time):
        return DELIVERY_AFTER_12
    elif dayofweek in workdays and time_in_12_17(current_time):
        return DELIVERY_AFTER_TWO_HOURS
    elif dayofweek == 5 and current_time > time17:
        return DELIVERY_AFTER_TWO_DAYS
    elif dayofweek == 6:
        return DELIVERY_AFTER_TOMORROW
    else:
        return DELIVERY_TOMORROW


def pickup_logic_with_moysklad(dayofweek, current_time):
    if dayofweek in workdays and time_in_00_12(current_time):
        return PICKUP_TODAY_12_20
    elif dayofweek in workdays and time_in_12_20(current_time):
        return PICKUP_NOW
    elif dayofweek == 5 and current_time > time20:
        return PICKUP_AFTER_TWO_DAYS
    elif dayofweek == 6:
        return PICKUP_AFTER_TOMORROW
    else:
        return PICKUP_TOMORROW


def delivery_logic_admin_today(dayofweek, current_time):
    if dayofweek in workdays and time_in_00_14(current_time):
        return DELIVERY_TODAY
    elif dayofweek == 5 and current_time > time14:
        return DELIVERY_AFTER_TWO_DAYS
    elif dayofweek == 6:
        return DELIVERY_AFTER_TOMORROW
    else:
        return DELIVERY_TOMORROW


def pickup_logic_admin_today(dayofweek, current_time):
    if dayofweek in workdays and time_in_00_14(current_time):
        return PICKUP_TODAY_16_20
    elif dayofweek == 5 and current_time > time14:
        return PICKUP_AFTER_TWO_DAYS
    elif dayofweek == 6:
        return PICKUP_AFTER_TOMORROW
    else:
        return PICKUP_TOMORROW


def delivery_logic_admin_nextday(dayofweek, current_time):
    if dayofweek in [1, 2, 3, 4] and time_in_00_14(current_time):
        return DELIVERY_TOMORROW
    elif dayofweek == 4 and current_time > time14:
        return DELIVERY_AFTER_THREE_DAYS
    elif dayofweek == 5:
        return DELIVERY_AFTER_TWO_DAYS
    elif dayofweek == 7:
        return DELIVERY_TOMORROW
    else:
        return DELIVERY_AFTER_TOMORROW


def pickup_logic_admin_nextday(dayofweek, current_time):
    if dayofweek in [1, 2, 3, 4] and time_in_00_14(current_time):
        return PICKUP_TOMORROW_16_20
    elif dayofweek == 4 and current_time > time14:
        return PICKUP_AFTER_THREE_DAYS
    elif dayofweek == 5:
        return PICKUP_AFTER_TWO_DAYS
    elif dayofweek == 7:
        return PICKUP_TOMORROW
    else:
        return PICKUP_AFTER_TOMORROW


def delivery_logic_admin_two_days(dayofweek, current_time):
    if dayofweek in [1, 2, 3] and time_in_00_14(current_time):
        return DELIVERY_AFTER_TOMORROW
    elif dayofweek == 7:
        return DELIVERY_AFTER_TOMORROW  # ??? DELIVER_TOMORROW
    elif dayofweek == 3 and current_time > time14:
        return DELIVERY_AFTER_FOUR_DAYS
    elif dayofweek in [4, 5]:
        return DELIVERY_AFTER_THREE_DAYS
    else:
        return DELIVERY_AFTER_TWO_DAYS


def pickup_logic_admin_two_days(dayofweek, current_time):
    if dayofweek in [1, 2, 3] and time_in_00_14(current_time):
        return PICKUP_AFTER_TOMORROW
    elif dayofweek == 7:
        return PICKUP_AFTER_TOMORROW
    elif dayofweek == 3 and current_time > time14:
        return PICKUP_AFTER_FOUR_DAYS
    elif dayofweek in [4, 5]:
        return PICKUP_AFTER_THREE_DAYS
    else:
        return PICKUP_AFTER_TWO_DAYS

