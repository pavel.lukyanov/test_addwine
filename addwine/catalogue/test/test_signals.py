import os, json
import requests_mock
from model_mommy import mommy
from django.urls import reverse
from django.test.testcases import TestCase
from decimal import Decimal


def response_from_file(filename):
    with open(os.path.join(os.getcwd(), 'catalogue', 'test', 'mock_responses', filename), 'r', encoding='utf8') as f:
        data = json.load(f)
    return data


class TestSignals(TestCase):
    def test_success_create(self):
        pass

        # p = mommy.make_recipe('catalogue.rcp_product')
        # mommy.make_recipe('partner.rcp_stock_record', product=p)
        # p.upc = '123-456'
        # p.sync_with_moi_sklad = True
        # p.save()
