# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-03-21 00:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0077_product_discount'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='is_exported_in_ym',
            field=models.BooleanField(default=True, verbose_name='экспортировать в Яндекс.Маркет'),
        ),
    ]
