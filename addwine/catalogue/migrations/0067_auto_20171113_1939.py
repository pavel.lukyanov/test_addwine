# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-11-13 16:39
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0066_auto_20171113_1845'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='also_buying_products',
            field=models.ManyToManyField(blank=True, related_name='also_buying_products_instances', through='catalogue.ProductAlsoBuying', to='catalogue.Product', verbose_name='С этим товаром обычно покупают'),
        ),
        migrations.AlterField(
            model_name='productalsobuying',
            name='recommendation',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='recommendation_also_buying_primaries', to='catalogue.Product', verbose_name='Recommended product'),
        ),
        migrations.AlterField(
            model_name='productrecommendation',
            name='recommendation',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='recommendation_primaries', to='catalogue.Product', verbose_name='Recommended product'),
        ),
    ]
