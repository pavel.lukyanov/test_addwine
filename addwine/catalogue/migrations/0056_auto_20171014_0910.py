# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-14 06:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0055_auto_20171014_0310'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='display_in_catalogue_menu_order',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='порядок отображения в меню каталога'),
        ),
        migrations.AddField(
            model_name='product',
            name='display_in_catalogue_menu_order',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Порядок отображения в меню каталога'),
        ),
        migrations.AlterField(
            model_name='category',
            name='display_in_bottom_section_order',
            field=models.PositiveSmallIntegerField(blank=True, null=True, unique=True, verbose_name='порядок отображения в нижней секции'),
        ),
    ]
