# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-09-11 16:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0042_auto_20170908_1932'),
    ]

    operations = [
        migrations.AddField(
            model_name='attributeoption',
            name='use_in_filter',
            field=models.BooleanField(default=True, verbose_name='использовать в фильтрах'),
        ),
    ]
