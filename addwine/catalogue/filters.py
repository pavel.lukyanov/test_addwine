from django.contrib.admin import SimpleListFilter


class FooterOrderingFilter(SimpleListFilter):
    title = 'отображается в подвальчике'
    parameter_name = 'display_in_bottom_section_order'

    def lookups(self, request, model_admin):
        return [('yes', 'да'), ('no', 'нет')]

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(display_in_bottom_section_order__isnull=False)
        elif self.value() == 'no':
            return queryset.filter(display_in_bottom_section_order=None)
