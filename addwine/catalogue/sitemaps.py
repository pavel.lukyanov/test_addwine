from django.contrib.sitemaps import GenericSitemap

from catalogue.models import Category, Product, Brand
from offer.models import Range

sitemaps = {
    'categories': GenericSitemap({
        'queryset': Category.objects.filter(is_active=True),
        'date_field': 'date_created'
    }, priority=0.7),
    'products': GenericSitemap({
        'queryset': Product.objects.filter(is_active=True),
        'date_field': 'date_created'
    }, priority=1),
    'ranges': GenericSitemap({
        'queryset': Range.objects.filter(is_public=True),
        'date_field': 'date_created'
    }, priority=0.8),
    'brands': GenericSitemap({
        'queryset': Brand.objects.all(),
        'date_field': 'date_created'
    }, priority=0.8),
}