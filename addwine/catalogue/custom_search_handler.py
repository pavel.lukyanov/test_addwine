import logging

from django.conf import settings

from catalogue.models import Product
from search.forms import BrowseCategoryForm
from search.search_handlers import SearchHandler

logger = logging.getLogger(__name__)


class CustomProductSearchHandler(SearchHandler):
    """
    Search handler specialised for searching products.  Comes with optional
    category filtering. To be used with an ElasticSearch search backend.
    """
    form_class = BrowseCategoryForm
    model_whitelist = [Product]
    paginate_by = settings.OSCAR_PRODUCTS_PER_PAGE

    def __init__(self, request_data, full_path, categories=None, product_ranges=None):
        self.categories = categories
        self.product_ranges = product_ranges
        super(CustomProductSearchHandler, self).__init__(request_data, full_path)

    def get_search_queryset(self):
        sqs = super(CustomProductSearchHandler, self).get_search_queryset()
        filter_rule = None
        if self.categories:
            filter_rule = 'category_exact:(%s)' % ' OR '.join([cat.full_slug.replace('/', '\/') for cat in self.categories])
        if self.product_ranges:
            filter_rule = 'product_range_exact:(%s)' % 'OR'.join([range.slug for range in self.product_ranges])

        if filter_rule:
            sqs = sqs.narrow(filter_rule)

        sqs = sqs.narrow('active:1')
        return sqs