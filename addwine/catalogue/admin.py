from random import randint

from adminsortable2.admin import SortableInlineAdminMixin, SortableAdminMixin
from ckeditor.widgets import CKEditorWidget
from dal import autocomplete
from django import forms
from django.contrib import admin
from django.contrib.sites.models import Site
from django.db.models.signals import post_save
from django.forms.utils import ErrorList
from django.http import HttpResponseRedirect

from oscar.apps.catalogue import admin as catalogue_admin
from django.db import models
from oscar.apps.catalogue.models import AttributeOptionGroup
from treebeard.admin import TreeAdmin

from catalogue.doc_import import add_or_update_products_from_excel, update_products_from_excel
from catalogue.filters import FooterOrderingFilter
from catalogue.models import FacetOrder, ProductAlsoBuying, Partner
from treebeard.forms import movenodeform_factory

from catalogue.models import OnProductAvailableDiscountInfo
from catalogue.signals import product_saved_related
from catalogue.utils import export_product_to_wl
from partner.models import StockRecord

from catalogue.models import Product, Category, ProductImage, ProductClass, ProductCategory, ProductRecommendation, \
    Brand, BrandImage, Pack, reverse, PackImage, CategorySEOText, ProductAttributeValue
from catalogue.widgets import ProductAttributeSelectWidget
from search.admin import IndexUpdateAdminMixin, IndexUpdateRelatedAdminMixin
from seo.admin import SEOFieldsDefaultsAdminMixin
from yandex_market.admin import YandexMarketUpdateAdminMixin, YandexMarketUpdateRelatedAdminMixin


class AttributeInlineForm(forms.ModelForm):
    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None, use_required_attribute=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance,
                         use_required_attribute)

        if self.instance.pk:
            if self.instance.attribute.type in ('option', 'multi_option'):
                self.fields[
                    'value_' + self.instance.attribute.type].queryset = self.instance.attribute.option_group.options.all()

    class Meta:
        model = ProductAttributeValue
        fields = '__all__'


class AttributeInline(catalogue_admin.AttributeInline):
    template = 'admin/catalogue/product/inlines/attributeinline.html'
    form = AttributeInlineForm
    readonly_fields = ('attribute',)
    fields = ['attribute', 'value_text', 'value_float', 'value_integer', 'value_boolean',
              'value_date', 'value_datetime', 'value_multi_option', 'value_option']
    formfield_overrides = {
        models.ForeignKey: {'widget': ProductAttributeSelectWidget()},
        models.TextField: {'widget': CKEditorWidget()},
    }
    can_delete = False
    extra = 0

    def has_add_permission(self, request):
        return False


class ProductImageInline(SortableInlineAdminMixin, admin.TabularInline):
    model = ProductImage


class BrandImageInline(SortableInlineAdminMixin, admin.TabularInline):
    model = BrandImage


class PackImageInline(SortableInlineAdminMixin, admin.TabularInline):
    model = PackImage


@admin.register(Pack)
class PackAdmin(admin.ModelAdmin):
    model = Pack
    fields = ['count', 'price', 'price_retail', 'is_active']
    list_display = ['product', 'count', 'price', 'price_retail', 'is_active']
    list_editable = ['is_active', ]
    inlines = [PackImageInline, ]


class PackInline(admin.TabularInline):
    model = Pack
    fields = ('count', 'price', 'price_retail', 'change_form_link', 'is_active')
    readonly_fields = ('change_form_link',)

    def change_form_link(self, obj):
        if obj.pk:
            return '<a target="_blank" href="' \
                   + reverse('admin:catalogue_pack_change', args=[obj.pk, ]) + '">Добавить изобаржения</a> '
        else:
            return ''

    change_form_link.short_description = ''
    change_form_link.allow_tags = True


class ProductRecommendationInlineForm(forms.ModelForm):
    class Meta:
        model = ProductRecommendation
        fields = '__all__'
        widgets = {
            'recommendation': autocomplete.ModelSelect2(url='autocomplete_product'),
        }


class ProductRecommendationInline(SortableInlineAdminMixin, catalogue_admin.ProductRecommendationInline):
    form = ProductRecommendationInlineForm
    raw_id_fields = []


class ProductAlsoBuyingInlineForm(forms.ModelForm):
    class Meta:
        model = ProductAlsoBuying
        fields = '__all__'
        widgets = {
            'recommendation': autocomplete.ModelSelect2(url='autocomplete_product'),
        }


class ProductAlsoBuyingInline(SortableInlineAdminMixin, admin.TabularInline):
    form = ProductAlsoBuyingInlineForm
    model = ProductAlsoBuying
    fk_name = 'primary'
    raw_id_fields = []


class ProductCategoryInlineForm(forms.ModelForm):
    class Meta:
        model = ProductCategory
        fields = '__all__'
        widgets = {
            'category': autocomplete.ModelSelect2(url='autocomplete_category'),
        }


class CategoryInline(catalogue_admin.CategoryInline):
    form = ProductCategoryInlineForm


class BrandForm(forms.ModelForm):
    class Meta:
        model = Brand
        fields = '__all__'
        widgets = {
            'mail_cta': autocomplete.ModelSelect2(url='autocomplete_mail_cta'),
        }


@admin.register(Brand)
class BrandAdmin(SEOFieldsDefaultsAdminMixin, SortableAdminMixin, IndexUpdateAdminMixin, admin.ModelAdmin):
    form = BrandForm
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'logo', 'description', 'mail_cta', 'rules_return', 'rules_guarantee')
        }),
        ('SEO', {
            'classes': ('collapse',),
            'fields': (
                'seo_title', 'seo_description', 'seo_keywords',
                'seo_og_title', 'seo_og_description', 'seo_og_image',
            ),
        }),
    )
    list_display = ['title', 'slug']
    search_fields = ('title',)
    prepopulated_fields = {
        'slug': ('title',)
    }
    seo_fields_defaults = {
        'seo_title': 'title',
        'seo_description': 'description',
        'seo_og_title': 'title',
        'seo_og_description': 'description',
    }

    inlines = [BrandImageInline, ]


@admin.register(Partner)
class Partner(admin.ModelAdmin):
    pass


class ProductForm(forms.ModelForm):
    price = forms.DecimalField(label="Цена (без НДС)")
    price_retail = forms.DecimalField(label="Себестоимость")

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None, use_required_attribute=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance,
                         use_required_attribute)
        if self.instance.pk:
            self.init_product_attribute_values()

    def init_product_attribute_values(self):
        instance = self.instance
        for attribute in instance.product_class.attributes.all():
            ProductAttributeValue.objects.get_or_create(attribute=attribute, product=instance)

    def reinit_product_attribute_values(self):
        prev_product_class = self.initial.get('product_class')
        if not self.fields['product_class'].has_changed(prev_product_class, self.data.get('product_class')):
            return
        instance = self.instance
        if prev_product_class:
            old_values = instance.attribute_values.filter(attribute__product_class=prev_product_class)
            if len(old_values):
                old_values.delete()
        self.init_product_attribute_values()

    def get_price_info(self):
        return self.instance.stockrecords.all()[0] if self.instance.pk else None

    def get_initial_for_field(self, field, field_name):
        if self.instance.pk:
            if field_name == 'price':
                return self.instance.stockrecords.all()[0].price_excl_tax
            if field_name == 'price_retail':
                return self.instance.stockrecords.all()[0].price_retail
        return super().get_initial_for_field(field, field_name)

    def save(self, commit=True):

        is_created = self.instance.pk is None
        instance = super().save(commit=False)

        if is_created:
            instance.save()
            StockRecord.objects.create(
                product=instance,
                price_excl_tax=self.cleaned_data['price'],
                price_retail=self.cleaned_data['price_retail']
            )
        else:
            info = self.get_price_info()
            info.price_excl_tax = self.cleaned_data['price']
            info.price_retail = self.cleaned_data['price_retail']
            info.save()

        return instance

    class Meta:
        model = Product
        fields = '__all__'
        widgets = {
            'parent': autocomplete.ModelSelect2(url='autocomplete_product'),
            'present': autocomplete.ModelSelect2(url='autocomplete_product'),
            'mail_cta': autocomplete.ModelSelect2(url='autocomplete_mail_cta'),
            'sommelier_cta': autocomplete.ModelSelect2(url='autocomplete_sommelier_cta'),
            'product_class': autocomplete.ModelSelect2(url='autocomplete_product_class'),
            'brand': autocomplete.ModelSelect2(url='autocomplete_brand'),
            'partner': autocomplete.ModelSelect2(url='autocomplete_partner'),
        }


class OnProductAvailableDiscountInfoAdminInline(admin.StackedInline):
    model = OnProductAvailableDiscountInfo
    min_num = 1
    max_num = 1

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.unregister(Product)


@admin.register(Product)
class ProductAdmin(SEOFieldsDefaultsAdminMixin, catalogue_admin.ProductAdmin):
    form = ProductForm
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget()},
    }
    save_on_top = True
    raw_id_fields = []

    seo_fields_defaults = {
        'seo_title': 'title',
        'seo_description': 'description',
        'seo_og_title': 'title',
        'seo_og_description': 'description',
    }
    fieldsets = (
        (None, {
            'fields': ('product_class', 'brand', 'present', 'upc', 'gtin',
                       'title', 'search_key_words', 'slug', 'partner', 'description', 'price', 'price_retail',
                       'delivery_day', 'quantity',
                       'forbidden_for_delivery', 'is_active', 'is_available', 'is_discountable', 'is_exported_in_ym',
                       'is_exported_in_gm', 'sync_with_moi_sklad', 'exported_to_wl',
                       ),
        }),
        ('SEO', {
            'classes': ('collapse',),
            'fields': (
                'seo_title', 'seo_description', 'seo_keywords',
                'seo_og_title', 'seo_og_description', 'seo_og_image',
            ),
        }),
        ('Дополнительно', {
            'classes': ('collapse',),
            'fields': ('mail_cta', 'sommelier_cta', 'rules_guarantee', 'rules_return'),
        }),
    )
    list_display = (
        'get_title', 'upc', 'gtin', 'get_price', 'get_price_retail', 'quantity',  'delivery_day', 'forbidden_for_delivery', 'is_discountable', 'is_available',
        'is_active', 'is_exported_in_ym', 'is_exported_in_gm', 'sync_with_moi_sklad', 'get_ms_sync_status')

    list_filter = ('brand', 'partner', 'categories', 'forbidden_for_delivery', 'is_discountable',
                   'is_available', 'is_active', 'is_exported_in_ym', 'is_exported_in_gm', 'sync_with_moi_sklad',)
    list_editable = ('delivery_day', 'forbidden_for_delivery', 'is_discountable', 'is_available', 'is_active',
                     'is_exported_in_ym', 'is_exported_in_gm', 'sync_with_moi_sklad',)

    search_fields = ('title', 'upc')

    inlines = [OnProductAvailableDiscountInfoAdminInline, CategoryInline, ProductImageInline, AttributeInline,
               PackInline, ProductRecommendationInline, ProductAlsoBuyingInline]

    def save_model(self, request, obj, form, change):
        if hasattr(form, 'reinit_product_attribute_values'):
            form.reinit_product_attribute_values()
        super().save_model(request, obj, form, change)

    def save_related(self, request, form, formsets, change):
        super().save_related(request, form, formsets, change)
        if change:
            product_saved_related.send(sender=Product, instance=form.instance)
        if form.instance:
            form.instance.update_discount()
        product_id = form.instance.id
        export_product_to_wl(request, product_id)

    def clone_instance(self, obj):
        # Fetch related objects before cloning, because we erase id of the object
        product_categories = list(ProductCategory.objects.filter(product=obj))
        attribute_values = list(obj.attribute_values.all())
        recommendations = list(obj.primary_recommendations.all())
        also_buying_products = list(obj.primary_also_buying.all())
        obj_id = obj.id

        cloned_obj = obj
        cloned_obj.pk = None
        cloned_obj.title += ' Дубликат %s' % (randint(0, 100000))
        cloned_obj.upc += ' Дубликат %s' % (randint(0, 100000))
        cloned_obj.is_active = False
        cloned_obj.ms_uuid = None
        cloned_obj.save()

        stockrecord = obj.stockrecords.all()[0]
        StockRecord.objects.create(
            product=cloned_obj,
            price_excl_tax=stockrecord.price_excl_tax,
            price_retail=stockrecord.price_retail
        )

        for product_category in product_categories:
            ProductCategory.objects.create(
                product=cloned_obj,
                category=product_category.category
            )

        for attribute_value in attribute_values:
            if attribute_value.value_multi_option.count():
                cloned_attribute_value = ProductAttributeValue.objects.create(
                    product=cloned_obj,
                    attribute=attribute_value.attribute
                )
                cloned_attribute_value.value_multi_option.add(*attribute_value.value_multi_option.all())
                continue
            attribute_value.pk = None
            attribute_value.product = cloned_obj
            attribute_value.save()

        for recommendation in recommendations:
            recommendation.pk = None
            recommendation.primary = cloned_obj
            recommendation.save()

        for product_also_buying in also_buying_products:
            product_also_buying.pk = None
            product_also_buying.primary = cloned_obj
            product_also_buying.save()

        return cloned_obj.id

    def response_change(self, request, obj):
        if '_copy' in request.POST:
            id = self.clone_instance(obj)
            return HttpResponseRedirect(reverse('admin:catalogue_product_change', args=[id]))
        return super(ProductAdmin, self).response_change(request, obj)

    def changelist_view(self, request, extra_context=None):
        if '_import_excel' in request.POST:
            if request.POST['action'] == 'upload':
                stats = add_or_update_products_from_excel(request.FILES['doc'])
            else:
                stats = update_products_from_excel(request.FILES['doc'])
            if not extra_context:
                extra_context = {}
            extra_context['stats'] = stats
        return super().changelist_view(request, extra_context)


class CategorySEOTextInlineForm(forms.ModelForm):
    class Meta:
        model = CategorySEOText
        fields = '__all__'
        widgets = {
            'seo_text': autocomplete.ModelSelect2(url='autocomplete_seo_text'),
        }


class CategorySEOTextInline(admin.StackedInline):
    model = CategorySEOText
    form = CategorySEOTextInlineForm
    min_num = 0
    extra = 1


move_nodeform = movenodeform_factory(Category)


class CategoryForm(move_nodeform):
    class Meta:
        model = Category
        fields = '__all__'
        widgets = {
            'mail_cta': autocomplete.ModelSelect2(url='autocomplete_mail_cta'),
            'sommelier_cta': autocomplete.ModelSelect2(url='autocomplete_sommelier_cta'),
        }


admin.site.unregister(Category)


@admin.register(Category)
class CategoryAdmin(SEOFieldsDefaultsAdminMixin, YandexMarketUpdateAdminMixin, IndexUpdateAdminMixin, TreeAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget()},
    }
    form = CategoryForm

    seo_fields_defaults = {
        'seo_title': 'name',
        'seo_og_title': 'name'
    }

    fieldsets = (
        (None, {
            'fields': ('name', 'slug', '_position', '_ref_node_id',
                       'display_in_bottom_section_order', 'is_active')
        }),
        ('SEO', {
            'classes': ('collapse',),
            'fields': (
                'seo_title', 'seo_description', 'seo_keywords',
                'seo_og_title', 'seo_og_description', 'seo_og_image',
            ),
        }),
        ('Дополнительно', {
            'classes': ('collapse',),
            'fields': ('mail_cta', 'sommelier_cta', 'rules_guarantee'),
        }),
    )
    list_display = catalogue_admin.CategoryAdmin.list_display \
                   + ('display_in_bottom_section_order', 'is_active',)
    list_editable = ('display_in_bottom_section_order', 'is_active',)
    list_filter = (FooterOrderingFilter, 'is_active',)
    prepopulated_fields = {
        'slug': ('name',)
    }

    inlines = [CategorySEOTextInline, ]


class ProductAttributeInlineForm(forms.ModelForm):
    def clean_code(self):
        code = self.cleaned_data['code'].replace('-', '_')
        return code

    def save(self, commit=True):
        if self.fields['option_group'].has_changed(self.initial.get('option_group'), self.instance.option_group_id) \
                or self.fields['type'].has_changed(self.initial.get('type'), self.instance.type):
            ProductAttributeValue.objects.filter(attribute=self.instance).delete()
        return super().save(commit)

    class Meta:
        fields = '__all__'


class ProductAttributeInline(SortableInlineAdminMixin, catalogue_admin.ProductAttributeInline):
    form = ProductAttributeInlineForm
    prepopulated_fields = {
        'code': ('name',)
    }
    extra = 2


admin.site.unregister(ProductClass)


@admin.register(ProductClass)
class ProductClassAdmin(SortableAdminMixin, YandexMarketUpdateRelatedAdminMixin, IndexUpdateRelatedAdminMixin,
                        catalogue_admin.ProductClassAdmin):
    fields = ('name',)
    list_display = ('name',)
    inlines = [ProductAttributeInline, ]


class AttributeOptionInline(SortableInlineAdminMixin, catalogue_admin.AttributeOptionInline):
    pass


admin.site.unregister(AttributeOptionGroup)


@admin.register(AttributeOptionGroup)
class AttributeOptionGroupAdmin(catalogue_admin.AttributeOptionGroupAdmin):
    inlines = [AttributeOptionInline, ]


@admin.register(FacetOrder)
class FacetOrderAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ('attribute', 'attribute_code')
    readonly_fields = ('attribute', 'attribute_code')

    def attribute_code(self, obj):
        return obj.attribute.code

    attribute_code.short_description = 'Код аттрибута'

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False
