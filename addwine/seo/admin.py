from django.contrib import admin
from django.utils.html import strip_tags


class SEOFieldsDefaultsAdminMixin(object):
    seo_fields_defaults = {

    }

    def save_model(self, request, obj, form, change):
        for key in self.seo_fields_defaults.keys():
            if not getattr(obj, key):
                value = getattr(obj, self.seo_fields_defaults[key])
                field = obj._meta.get_field(key)
                max_length = getattr(field, 'max_length')
                if max_length:
                    value = value[:max_length]
                value = strip_tags(value)
                setattr(obj, key, value)
        super(SEOFieldsDefaultsAdminMixin, self).save_model(request, obj, form, change)
