from django.forms import model_to_dict
from django.shortcuts import render


class SeoParamsMixin(object):
    def extract_seo_configs(self, model_instance):
        return model_to_dict(model_instance, fields=('seo_title', 'seo_description', 'seo_keywords',
                                                     'seo_og_title', 'seo_og_description', 'seo_og_image')
                             )
