from django.apps import AppConfig


class SiteConfigsAppConfig(AppConfig):
    name = 'site_configs'
