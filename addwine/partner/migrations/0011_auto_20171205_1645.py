# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-05 13:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0010_auto_20171027_1444'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stockrecord',
            name='price_retail',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=12, verbose_name='Price (retail)'),
        ),
    ]
