from model_mommy.recipe import Recipe, foreign_key
from .models import StockRecord
from catalogue.mommy_recipes import rcp_product
import random


rcp_stock_record = Recipe(
    StockRecord,
    product=foreign_key(rcp_product),
    price_excl_tax=lambda: random.randrange(1000, 30000, 500)
)
