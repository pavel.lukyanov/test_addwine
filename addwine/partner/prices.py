from decimal import Decimal
from math import ceil

from oscar.apps.partner.prices import *


class PriceWithRangeDiscount(FixedPrice):
    range_discount_val = Decimal("0.00")
    product = None

    def __init__(self, currency, excl_tax, retail, product, tax=None, ):
        super().__init__(currency, excl_tax, tax)
        self.retail = retail
        self.product = product

    @property
    def has_range_discount(self):
        return self.product.discount is not None and \
               self.product.discount.is_available() and \
               self.product.is_discountable

    @property
    def range_discount(self):
        return self.product.discount

    @property
    def range_discount_val(self):
        if not self.product.discount or not self.product.discount.is_available():
            return 0
        return self.product.discount.calculate_discount(self.product, self.excl_tax)

    @property
    def _tax_ratio(self):
        if not self.incl_tax:
            return 0
        return self.excl_tax / self.incl_tax

    @property
    def incl_range_discount_excl_tax(self):
        return max(ceil(self.excl_tax - self.range_discount_val), 0)

    @property
    def incl_range_discount_incl_tax(self):
        return max(ceil(self.excl_tax - self.range_discount_val * self._tax_ratio), 0)
