#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.utils.text import Truncator

from customer.models import User, reverse, Contacts
from embed_video.fields import EmbedVideoField
from marketing.models import SEOText, MailCTA
from seo.models import AbstractSeoConfigs


def list_as_dicts(data_list):
    return list(map(lambda x: x.as_dict(), data_list))


def cut_text(text, length):
    return Truncator(text).chars(length, html=True)


class Category(models.Model):
    title = models.CharField(verbose_name='заголовок', max_length=1024)
    slug = models.SlugField('адрес категории', max_length=255, unique=False)
    order_field = models.PositiveIntegerField(verbose_name='нажать и тащить', default=0)
    created = models.DateTimeField(verbose_name="дата создания", auto_now_add=True)

    mail_cta = models.ForeignKey(MailCTA, verbose_name="призыв к действию: рассылка", null=True, blank=True,
                                 related_name='news_categories',
                                 on_delete=models.SET_NULL)

    def get_absolute_url(self):
        return reverse('article_category', args=[self.slug])

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'категория статьи'
        verbose_name_plural = 'категории статьи'
        ordering = ('order_field',)


class CategorySEOText(models.Model):
    category = models.ForeignKey(Category, verbose_name='категория',
                                 on_delete=models.CASCADE, related_name='seo_texts')
    seo_text = models.ForeignKey(SEOText, verbose_name='SEO текст', related_name='article_categories')

    def __str__(self):
        return 'SEO текст для категория %s' % self.category.title

    class Meta:
        verbose_name = 'SEO текст'
        verbose_name_plural = 'SEO тексты'


class Article(AbstractSeoConfigs, models.Model):
    category = models.ForeignKey(Category, verbose_name='категория', related_name='articles')
    slug = models.SlugField('адрес статьи', max_length=255, unique=False)
    cover = models.ImageField(verbose_name='обложка')
    title = models.CharField(verbose_name="заголовок", max_length=200)
    text = RichTextUploadingField(verbose_name="текст")
    snippet_length = models.PositiveIntegerField(verbose_name="маскимальная длина анонса", default=380)
    created = models.DateTimeField(verbose_name="дата создания")

    rating = models.FloatField(verbose_name='рейтинг', default=0)
    votes = models.PositiveIntegerField(verbose_name='проголосовало', default=0)

    @property
    def rating_in_percents(self):
        return self.rating/5 * 100

    def add_rate(self, rate, commit=True):
        self.rating = (self.rating*self.votes+rate)/(self.votes+1)
        self.votes += 1
        if commit:
            self.save()

    @property
    def snippet(self):
        return cut_text(self.text, self.snippet_length)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('article', args=[self.category.slug, self.slug])

    class Meta:
        verbose_name = "статья"
        verbose_name_plural = "статьи"


class Comment(Contacts):
    article = models.ForeignKey(Article, verbose_name='статья', related_name='comments')
    text = models.TextField('комментарий')
    created = models.DateTimeField('дата публикации', auto_now_add=True)

    class Meta:
        verbose_name = 'комментарий'
        verbose_name_plural = 'комментарии'
