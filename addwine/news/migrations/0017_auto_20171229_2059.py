# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-12-29 17:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0016_auto_20171112_1644'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='seo_description',
            field=models.TextField(blank=True, max_length=100, verbose_name='описание (description)'),
        ),
        migrations.AddField(
            model_name='article',
            name='seo_keywords',
            field=models.CharField(blank=True, max_length=1024, verbose_name='ключевые слова (keywords)'),
        ),
        migrations.AddField(
            model_name='article',
            name='seo_og_description',
            field=models.TextField(blank=True, max_length=100, verbose_name='описание в соц. сетях '),
        ),
        migrations.AddField(
            model_name='article',
            name='seo_og_image',
            field=models.ImageField(blank=True, upload_to='', verbose_name='превью для соц. сетей'),
        ),
        migrations.AddField(
            model_name='article',
            name='seo_og_title',
            field=models.CharField(blank=True, max_length=60, verbose_name='заголовок в соц. сетях'),
        ),
        migrations.AddField(
            model_name='article',
            name='seo_title',
            field=models.CharField(blank=True, max_length=60, verbose_name='заголовок страницы (title)'),
        ),
    ]
