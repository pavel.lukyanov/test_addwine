from django.conf import settings
from django.http import JsonResponse

from common.views import CatchFormView
from news.forms import CommentForm, RateForm
from news.utils import is_available_for_vote


class AddCommentView(CatchFormView):
    form_name = 'comment'
    form_class = CommentForm

    def generate_tail(self, success):
        return ''

    def form_valid(self, form):
        form.instance.save()
        return super().form_valid(form)


class RateView(CatchFormView):
    form_name = 'rate'
    form_class = RateForm

    def form_valid(self, form):
        article = form.cleaned_data['article']
        available_for_vote, voted_articles = is_available_for_vote(self.request, article)
        if available_for_vote:
            article.add_rate(form.cleaned_data['rate'])
            voted_articles.append(article.id)
        resp = JsonResponse({'rating': article.rating_in_percents})
        if available_for_vote:
            resp.set_cookie(settings.NEWS_RATING_COOKIE_KEY, voted_articles)
        return resp

