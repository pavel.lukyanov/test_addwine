from django.contrib.sitemaps import GenericSitemap

from news.models import Category, Article

sitemaps = {
    'articles_categories': GenericSitemap({
        'queryset': Category.objects.all(),
        'date_field': 'created'
    }, priority=0.7),
    'articles': GenericSitemap({
        'queryset': Article.objects.all(),
        'date_field': 'created'
    }, priority=0.8),
}