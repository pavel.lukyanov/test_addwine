from .base import *
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

sentry_sdk.init(
    dsn="https://388dcec1d2244b4a86a28fb6e72aaaa4@sentry.io/1292976",
    integrations=[DjangoIntegration()]
)


INSTALLED_APPS += [
    'djcelery_email',
]


DEBUG = False
TEMPLATE_DEBUG = DEBUG

# CELERY
CELERY_BROKER_URL = env('CELERY_BROKER', default='amqp://addwine:XdZ1E8I3@localhost:5672/addwine.ru')
BROKER_BACKEND = 'amqp'

if env('HEROKU_STAGING', default=False):
    CELERY_ALWAYS_EAGER = True
    CELERY_EAGER_PROPAGATES_EXCEPTIONS = True
    BROKER_BACKEND = 'memory'


ALLOWED_HOSTS = ['www.addwine.ru', 'addwine.ru', env('ALLOWED_HOST', default='127.0.0.1')]

# TEMPLATES
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG
TEMPLATES[0]['OPTIONS']['loaders'] = [('django.template.loaders.cached.Loader', TEMPLATES[0]['OPTIONS']['loaders'])]

# TELEGRAM
TELEGRAM_BOT_SET_WEBHOOK = False

# Email options
EMAIL_BACKEND = 'djcelery_email.backends.CeleryEmailBackend'

# Service syncs
YANDEX_MARKET_ENABLED = env('YANDEX_MARKET_ENABLED', default=True)
GOOGLE_MERCHANT_ENABLED = env('GOOGLE_MERCHANT_ENABLED', default=True)
AMOCRM_SYNC_ENABLED = env('AMOCRM_SYNC_ENABLED', default=True)
UNISENDER_SYNC_ENABLED = env('UNISENDER_SYNC_ENABLED', default=True)
MOI_SKLAD_SYNC_ENABLED = env('MOI_SKLAD_SYNC_ENABLED', default=True)

# DATABASE
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env('DATABASE_NAME', default='addwine'),
        'USER': env('POSTGRES_USER', default='postgres'),
        'PASSWORD': env('POSTGRES_PASSWORD', default='postgres'),
        'HOST': 'localhost',
        'PORT': '',
    }
}


# Used in heroku
if env('DATABASE_URL', default=None):
    import dj_database_url
    db_from_env = dj_database_url.config(conn_max_age=500)
    DATABASES['default'].update(db_from_env)

# STORAGES
# ------------------------------------------------------------------------------
# https://django-storages.readthedocs.io/en/latest/#installation
INSTALLED_APPS += ['storages']  # noqa F405
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_ACCESS_KEY_ID = env('DJANGO_AWS_ACCESS_KEY_ID')
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_SECRET_ACCESS_KEY = env('DJANGO_AWS_SECRET_ACCESS_KEY')
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_STORAGE_BUCKET_NAME = env('DJANGO_AWS_STORAGE_BUCKET_NAME')
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_AUTO_CREATE_BUCKET = True
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_QUERYSTRING_AUTH = False
# DO NOT change these unless you know what you're doing.
_AWS_EXPIRY = 60 * 60 * 24 * 7
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age={}, s-maxage={}, must-revalidate'.format(_AWS_EXPIRY, _AWS_EXPIRY),
}
AWS_IS_GZIPPED = True

# STATIC
# ------------------------

STATICFILES_STORAGE = 'settings.production.StaticRootS3BotoStorage'
STATIC_URL = 'https://{}.s3.amazonaws.com/static/'.format(AWS_STORAGE_BUCKET_NAME)

# MEDIA
# ------------------------------------------------------------------------------

# region http://stackoverflow.com/questions/10390244/
from storages.backends.s3boto3 import S3Boto3Storage  # noqa E402
StaticRootS3BotoStorage = lambda: S3Boto3Storage(location='static')  # noqa
MediaRootS3BotoStorage = lambda: S3Boto3Storage(location='media', file_overwrite=False)  # noqa
# endregion
DEFAULT_FILE_STORAGE = 'settings.production.MediaRootS3BotoStorage'


# django-compressor
# ------------------------------------------------------------------------------
# https://django-compressor.readthedocs.io/en/latest/settings/#django.conf.settings.COMPRESS_ENABLED
COMPRESS_ENABLED = env.bool('COMPRESS_ENABLED', default=False)
# https://django-compressor.readthedocs.io/en/latest/settings/#django.conf.settings.COMPRESS_STORAGE
COMPRESS_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
# https://django-compressor.readthedocs.io/en/latest/settings/#django.conf.settings.COMPRESS_URL
COMPRESS_URL = STATIC_URL
COMPRESS_CSS_FILTERS = ['compressor.filters.css_default.CssAbsoluteFilter',  'compressor.filters.cssmin.rCSSMinFilter']

# Collectfast
# ------------------------------------------------------------------------------
# https://github.com/antonagestam/collectfast#installation
INSTALLED_APPS = ['collectfast'] + INSTALLED_APPS  # noqa F405
AWS_PRELOAD_METADATA = True
COLLECTFAST_THREADS = 20

THUMBNAIL_STORAGE = 'django.core.files.storage.FileSystemStorage'

WINELEVEL_EXPORT_IS_ENABLED = env.bool('WINELEVEL_EXPORT_IS_ENABLED', default=True)
