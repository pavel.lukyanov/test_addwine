from .base import *

INSTALLED_APPS += [
    'debug_toolbar',
    'django_extensions',
    'rest_framework',
    'corsheaders'
]


DEBUG = True
TEMPLATE_DEBUG = DEBUG
THUMBNAIL_DEBUG = True

MOI_SKLAD_SYNC_ENABLED = False
GOOGLE_MERCHANT_ENABLED = False

TEMPLATES[0]['OPTIONS']['debug'] = DEBUG
ALLOWED_HOSTS = ['www.addwine.ru', 'addwine.ru', '127.0.0.1', 'localhost', '0.0.0.0', '192.168.1.187']

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env('DATABASE_NAME', default='addwine'),
        'USER': env('POSTGRES_USER', default='addwine_dev'),
        'PASSWORD': env('POSTGRES_PASSWORD', default='postgres'),
        'HOST': 'localhost',
        'PORT': '',
    }
}

BROKER_BACKEND = 'redis'

MIDDLEWARE_CLASSES.insert(1, 'debug_toolbar.middleware.DebugToolbarMiddleware')
MIDDLEWARE_CLASSES.insert(3, 'corsheaders.middleware.CorsMiddleware')
CORS_ORIGIN_ALLOW_ALL = True


TELEGRAM_BOT_SET_WEBHOOK = False
CELERY_BROKER_URL = env('CELERY_BROKER', default='redis://localhost:6379/3')
CELERY_TASK_ALWAYS_EAGER = False

INTERNAL_IPS = '127.0.0.1'
# DEBUG_TOOLBAR_CONFIG = {
#     "SHOW_TOOLBAR_CALLBACK": True,
# }


# STORAGES
# ------------------------------------------------------------------------------
# https://django-storages.readthedocs.io/en/latest/#installation
INSTALLED_APPS += ['storages']  # noqa F405
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_ACCESS_KEY_ID = env('DJANGO_AWS_ACCESS_KEY_ID')
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_SECRET_ACCESS_KEY = env('DJANGO_AWS_SECRET_ACCESS_KEY')
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_STORAGE_BUCKET_NAME = env('DJANGO_AWS_STORAGE_BUCKET_NAME')
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_AUTO_CREATE_BUCKET = True
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_QUERYSTRING_AUTH = False
# DO NOT change these unless you know what you're doing.
_AWS_EXPIRY = 60 * 60 * 24 * 7
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age={}, s-maxage={}, must-revalidate'.format(_AWS_EXPIRY, _AWS_EXPIRY),
}

# MEDIA
# ------------------------------------------------------------------------------

# region http://stackoverflow.com/questions/10390244/
from storages.backends.s3boto3 import S3Boto3Storage  # noqa E402
MediaRootS3BotoStorage = lambda: S3Boto3Storage(location='media', file_overwrite=False)  # noqa
# endregion
DEFAULT_FILE_STORAGE = 'settings.development.MediaRootS3BotoStorage'


THUMBNAIL_STORAGE = 'django.core.files.storage.FileSystemStorage'