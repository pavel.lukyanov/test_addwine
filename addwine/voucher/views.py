from django.http import JsonResponse
from django.shortcuts import render
from django.views import View

from basket.models import Basket
from customer.models import Contacts
from voucher.models import Voucher, GiftCard


class VoucherFetcherView(View):
    def build_response(self, context):
        return JsonResponse(context)

    def post(self, request, *args, **kwargs):
        code = request.POST.get("code")

        is_gift_card = False
        try:
            sale_obj = Voucher.objects.get(code=code)
        except Voucher.DoesNotExist:
            try:
                sale_obj = GiftCard.objects.get(code=code)
                is_gift_card = True
            except GiftCard.DoesNotExist:
                return self.build_response({
                    'fetched': False,
                })

        if is_gift_card:
            gift_basket = None
            try:
                gift_basket = sale_obj.basket
            except Basket.DoesNotExist:
                pass
            return self.build_response({
                'fetched': True,
                'name': sale_obj.name,
                'description': sale_obj.description,
                'active': sale_obj.is_available(),
                'enabled': sale_obj.active,
                'expired': False,
                'already_applied': gift_basket and gift_basket == request.basket,
                'is_gift_card': True,
            })

        email = request.GET.get('email')
        phone = request.GET.get('phone')
        max_applications_exceeded = False
        no_credentials = True
        if email or phone:
            contacts = Contacts(email=email, phone_number=phone)
            user = contacts.recognize_user()
            no_credentials = False
            max_applications_exceeded = user and user.voucher_applications.filter(voucher=sale_obj).count() >= sale_obj.max_applications_per_user

        offer = sale_obj.offers.all()[0]
        return self.build_response({
            'fetched': True,
            'name': sale_obj.name,
            'description': offer.description,
            'active': sale_obj.is_active() and offer.is_available(),
            'enabled': sale_obj.is_enabled(),
            'expired': sale_obj.is_expired(),
            'start_datetime': sale_obj.start_datetime,
            'end_datetime': sale_obj.end_datetime,
            'max_applications_exceeded': max_applications_exceeded,
            'already_applied': sale_obj in request.basket.vouchers.all(),
            'is_gift_card': False,
            'no_credentials': no_credentials
        })
