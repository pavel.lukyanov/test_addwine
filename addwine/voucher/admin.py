from dal import autocomplete
from django import forms
from django.contrib import admin
from django.db.models import Sum
from django.utils.translation import ugettext_lazy as _

from offer.models import Range, Condition, Benefit, ConditionalOffer
from voucher.models import Voucher, VoucherGroup, GiftCard


class VoucherGroupAdminForm(forms.ModelForm):
    predefined_fields = {
        'benefit_range': {
            'name': 'range',
            'obj_name': 'benefit'
        },
        'benefit_type': {
            'name': 'type',
            'obj_name': 'benefit'
        },
        'benefit_value': {
            'name': 'value',
            'obj_name': 'benefit'
        },
        'benefit_one_product_application': {
            'name': 'one_product_application',
            'obj_name': 'benefit'
        },

        'offer_description': {
            'name': 'description',
            'obj_name': 'offer'
        },

    }

    def get_initial_for_field(self, field, field_name):
        predefined_field = self.predefined_fields.get(field_name)
        if predefined_field is None or self.instance.pk is None:
            return super().get_initial_for_field(field, field_name)

        vouchers = self.instance.vouchers.all()
        if len(vouchers) == 0:
            return None

        voucher = vouchers[0]
        if predefined_field['obj_name'] == 'benefit':
            obj = voucher.benefit
        else:
            obj = voucher.offers.all()[0]
        return getattr(obj, predefined_field['name'])

    benefit_range = forms.ModelChoiceField(
        label=_('Which products get a discount?'),
        queryset=Range.objects.all(),
        widget=autocomplete.ModelSelect2(url='autocomplete_range'),
    )
    type_choices = (
        (Benefit.PERCENTAGE, _('Percentage off of products in range')),
        (Benefit.FIXED, _('Fixed amount off of products in range')),
    )
    benefit_type = forms.ChoiceField(
        choices=type_choices,
        label=_('Discount type'),
    )
    benefit_value = forms.DecimalField(
        min_value=0,
        label=_('Discount value'))
    benefit_one_product_application = forms.BooleanField(label='Применять только к 1 продукту', required=False)

    def clean_code(self):
        code = self.cleaned_data['code'].strip().upper()
        if not code:
            raise forms.ValidationError(_("Please enter a voucher code"))
        try:
            voucher = Voucher.objects.get(code=code)
        except Voucher.DoesNotExist:
            pass
        else:
            if (not self.instance) or (voucher.id != self.instance.id):
                raise forms.ValidationError(_("The code '%s' is already in"
                                              " use") % code)
        return code

    def clean(self):
        cleaned_data = super(VoucherGroupAdminForm, self).clean()
        start_datetime = cleaned_data.get('start_datetime')
        end_datetime = cleaned_data.get('end_datetime')
        if start_datetime and end_datetime and end_datetime < start_datetime:
            raise forms.ValidationError(_("The start date must be before the"
                                          " end date"))
        return cleaned_data

    def is_valid(self):
        valid = super().is_valid()
        return valid

    class Meta:
        model = VoucherGroup
        fields = '__all__'
        widgets = {
            'mail_cta': autocomplete.ModelSelect2(url='autocomplete_mail_cta'),
        }


class VoucherFormInline(forms.ModelForm):
    offer_max_global_applications = forms.IntegerField(
        min_value=0,
        label=_("Max global applications"),
        help_text=_("The number of times this offer can be used before it "
                    "is unavailable"),
        required=False
    )

    offer_max_discount = forms.DecimalField(
        label=_("Max discount"), decimal_places=2, max_digits=12,
        help_text=_("When an offer has given more discount to orders "
                    "than this threshold, then the offer becomes "
                    "unavailable"),
        required=False
    )

    def get_initial_for_field(self, field, field_name):
        if field_name.startswith("offer_") and self.instance.pk:
            offer = self.instance.offers.all()[0]
            if field_name == 'offer_max_global_applications':
                return offer.max_global_applications
            if field_name == 'offer_max_discount':
                return offer.max_discount

        value = super().get_initial_for_field(field, field_name)
        if field_name == 'code' and value:
            value = value.replace(self.instance.group.code + "_", '')
        return value

    class Meta:
        fields = '__all__'
        model = Voucher


class VoucherAdminInline(admin.TabularInline):
    model = Voucher
    form = VoucherFormInline
    readonly_fields = (
        'remaining_available_discount', 'remaining_num_of_applications', 'num_basket_additions', 'num_orders',
        'total_discount')
    fields = ('name', 'code', 'enabled', 'max_applications_per_user', 'offer_max_global_applications', 'offer_max_discount',
              'remaining_available_discount', 'remaining_num_of_applications', 'num_basket_additions', 'num_orders',
              'total_discount')

    def remaining_num_of_applications(self, obj):
        offer = obj.offers.all()[0]
        if offer.remaining_num_of_applications is not None:
            return offer.remaining_num_of_applications
        else:
            return '-'

    remaining_num_of_applications.short_description = 'Оставшееся количество применений'

    def remaining_available_discount(self, obj):
        offer = obj.offers.all()[0]
        if offer.remaining_available_discount is not None:
            return offer.remaining_available_discount
        else:
            return '-'

    remaining_available_discount.short_description = 'Остаток по скидке'


@admin.register(VoucherGroup)
class VoucherGroupAdmin(admin.ModelAdmin):
    form = VoucherGroupAdminForm

    list_display = ('name', 'code', 'enabled', 'display_for_users', 'num_basket_additions',
                    'total_remaining_num_of_applications', 'total_remaining_available_discount', 'num_orders',
                    'total_discount',
                    )
    list_editable = ('enabled', 'display_for_users')
    readonly_fields = (
        'total_remaining_num_of_applications', 'total_remaining_available_discount', 'num_basket_additions',
        'num_orders',
        'total_discount')
    fieldsets = (
        (None, {
            'fields': ('name', 'code', 'cover', 'description', 'rules', 'mail_cta', 'enabled', 'display_for_users')}
         ),
        ('Срок проведения', {
            'fields': (
                'start_datetime', 'end_datetime',
            )
        }),
        (_('Benefit'), {
            'fields': ('benefit_range', 'benefit_type', 'benefit_value', 'benefit_one_product_application')
        }),
        (_('Usage'), {
            'fields': (
                'total_remaining_num_of_applications', 'total_remaining_available_discount', 'num_basket_additions',
                'num_orders',
                'total_discount')}),

        ('SEO', {
            'fields': (
                'seo_title', 'seo_description', 'seo_keywords',
                'seo_og_title', 'seo_og_description', 'seo_og_image',
            ),
        }),
    )

    def num_basket_additions(self, obj):
        return obj.vouchers.aggregate(value=Sum('num_basket_additions'))['value']

    num_basket_additions.short_description = 'Количество применений в корзине'

    def num_orders(self, obj):
        return obj.vouchers.aggregate(value=Sum('num_orders'))['value']

    num_orders.short_description = 'Количество заказов'

    def total_discount(self, obj):
        return obj.vouchers.aggregate(value=Sum('total_discount'))['value']

    total_discount.short_description = 'Общая скидка'

    def total_remaining_num_of_applications(self, obj):
        total_remaining_num = None
        for voucher in obj.vouchers.all():
            offer = voucher.offers.all()[0]
            if offer.remaining_num_of_applications is not None:
                if total_remaining_num is None:
                    total_remaining_num = 0
                total_remaining_num += offer.remaining_num_of_applications
        if total_remaining_num is None:
            total_remaining_num = '-'
        return total_remaining_num

    total_remaining_num_of_applications.short_description = 'Общее количество оставшихся применений'

    def total_remaining_available_discount(self, obj):
        total_remaining_discount = None
        for voucher in obj.vouchers.all():
            offer = voucher.offers.all()[0]
            if offer.remaining_available_discount is not None:
                if total_remaining_discount is None:
                    total_remaining_discount = 0
                total_remaining_discount += offer.remaining_available_discount
        if total_remaining_discount is None:
            total_remaining_discount = '-'
        return total_remaining_discount

    total_remaining_available_discount.short_description = 'Общий остаток по скидке'

    def save_formset(self, request, form, formset, change):
        formset.save(commit=False)

        index = 0
        if change:
            parent = form.instance
            for instance in parent.vouchers.order_by('pk'):
                instance_form = formset.forms[index]
                code = form.cleaned_data['code'] + '_' + instance_form.cleaned_data['code']

                offer = instance.offers.all()[0]
                offer.description = form.cleaned_data['description']
                offer.max_global_applications = instance_form.cleaned_data['offer_max_global_applications']
                offer.max_basket_applications = 1
                offer.max_discount = instance_form.cleaned_data['offer_max_discount']
                offer.save()
                offer.condition.range = form.cleaned_data['benefit_range']
                offer.condition.save()

                benefit = instance.benefit
                benefit.range = form.cleaned_data['benefit_range']
                benefit.type = form.cleaned_data['benefit_type']
                benefit.value = form.cleaned_data['benefit_value']
                benefit.one_product_application = form.cleaned_data['benefit_one_product_application']
                benefit.save()

                instance.name = instance_form.cleaned_data['name']
                instance.enabled = instance_form.cleaned_data['enabled']
                instance.code = code
                instance.max_applications_per_user = instance_form.cleaned_data['max_applications_per_user']
                instance.start_datetime = form.cleaned_data['start_datetime']
                instance.end_datetime = form.cleaned_data['end_datetime']
                instance.usage = Voucher.MULTI_USE
                instance.save()
                index += 1

        for instance in formset.new_objects:
            instance_form = formset.forms[index]
            code = form.cleaned_data['code'] + '_' + instance_form.cleaned_data['code']

            condition = Condition.objects.create(
                range=form.cleaned_data['benefit_range'],
                type=Condition.COUNT,
                value=1
            )
            benefit = Benefit.objects.create(
                range=form.cleaned_data['benefit_range'],
                type=form.cleaned_data['benefit_type'],
                value=form.cleaned_data['benefit_value'],
                one_product_application=form.cleaned_data['benefit_one_product_application']
            )
            offer, created = ConditionalOffer.objects.get_or_create(
                name=code,
                defaults={
                    'offer_type': ConditionalOffer.VOUCHER,
                    'benefit': benefit,
                    'condition': condition,
                    'description': form.cleaned_data['description'],
                    'max_global_applications': instance_form.cleaned_data['offer_max_global_applications'],
                    'max_discount': instance_form.cleaned_data['offer_max_discount'],
                }
            )

            instance.enabled = instance_form.cleaned_data['enabled']
            instance.code = code
            instance.max_applications_per_user = instance_form.cleaned_data['max_applications_per_user']
            instance.start_datetime = form.cleaned_data['start_datetime']
            instance.end_datetime = form.cleaned_data['end_datetime']
            instance.save()
            instance.offers.add(offer)
            index += 1
        for instance in formset.deleted_objects:
            instance.delete()
        formset.save_m2m()

    inlines = [VoucherAdminInline, ]


class GiftCardAdminForm(forms.ModelForm):
    def clean_code(self):
        code = self.cleaned_data['code'].strip().upper()
        if not code:
            raise forms.ValidationError(_("Please enter a voucher code"))
        try:
            voucher = Voucher.objects.get(code=code)
        except Voucher.DoesNotExist:
            pass
        else:
            if (not self.instance) or (voucher.id != self.instance.id):
                raise forms.ValidationError(_("The code '%s' is already in"
                                              " use") % code)
        return code

    class Meta:
        fields = '__all__'
        model = GiftCard


@admin.register(GiftCard)
class GiftCardAdmin(admin.ModelAdmin):
    form = GiftCardAdminForm
    readonly_fields = ('spent', 'order')
    fields = ('name', 'code', 'description', 'value', 'active', 'spent', 'order', )

    list_display = ('name', 'code', 'spent', 'order', 'active')
    list_editable = ('active',)
    search_fields = ('name', 'code')
    list_filter = ('spent', 'active',)
