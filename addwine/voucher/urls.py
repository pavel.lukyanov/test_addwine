from django.conf.urls import url

from common.views import get_admin_autocomplete_view
from voucher.models import GiftCard
from voucher.views import VoucherFetcherView

urlpatterns = [
    url(r'ajax/fetch$', VoucherFetcherView.as_view(), name='voucher-fetch'),
    url(r'autocomplete/gift-card$', get_admin_autocomplete_view(GiftCard, 'code').as_view(), name='autocomplete_gift_card'),
]
