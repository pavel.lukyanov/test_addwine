# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-08 14:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('voucher', '0014_giftcard_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='giftcard',
            name='value',
            field=models.DecimalField(decimal_places=2, max_digits=12, verbose_name='Номинал'),
        ),
    ]
