# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-08 11:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('voucher', '0008_auto_20171007_1858'),
    ]

    operations = [
        migrations.AddField(
            model_name='voucher',
            name='enabled',
            field=models.BooleanField(default=True, verbose_name='Активна'),
        ),
        migrations.AddField(
            model_name='vouchergroup',
            name='enabled',
            field=models.BooleanField(default=True, verbose_name='Активна'),
        ),
    ]
