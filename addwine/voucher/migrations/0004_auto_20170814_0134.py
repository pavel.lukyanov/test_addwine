# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-08-13 22:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('voucher', '0003_auto_20170814_0129'),
    ]

    operations = [
        migrations.AlterField(
            model_name='voucher',
            name='max_usage_count',
            field=models.PositiveIntegerField(blank=True, default=0, help_text='Используйте это поле, чтобы указать максимальное количество использований купона', null=True, verbose_name='Максимальное количество использований'),
        ),
    ]
