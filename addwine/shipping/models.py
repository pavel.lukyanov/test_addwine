from ckeditor.fields import RichTextField
from django.forms import model_to_dict

from django.db import models, OperationalError

from oscar.core import prices
from embed_video.fields import EmbedVideoField

from oscar.apps.shipping import abstract_models

from shipping.areas import AreaResolver


class OrderAndItemCharges(abstract_models.AbstractOrderAndItemCharges):
    use_for_pickup = models.BooleanField('Отображать как точку самомвывоза', default=False, blank=True)
    require_address = models.BooleanField('Необходим адрес', default=True)
    description = RichTextField('Описание', blank=True)
    use_shipping_table = models.BooleanField('использовать таблицу доставки', default=False)

    def calculate(self, basket, point=None):
        return self.calculate_with_total(basket.total_excl_tax, basket.currency, point)

    def calculate_with_total(self, total, currency, point=None):
        charge = None
        if self.use_shipping_table:
            if point:
                charge = ShippingTable.get_instance().calculate_price(point, total)
            resolved = charge is not None
            if not resolved:
                charge = 0
        else:
            charge = self.price_per_order
            resolved = True

        # Zero tax is assumed...

        return prices.Price(
            currency=currency,
            excl_tax=charge,
            incl_tax=charge), resolved

    def as_dict(self):
        return model_to_dict(self, fields=('name', 'description'))

    class Meta:
        verbose_name = 'способ доставки'
        verbose_name_plural = 'способы доставки'


from oscar.apps.shipping.models import *


class ShippingTable(models.Model):
    column1_min = models.DecimalField('колонка 1 мин', max_digits=11, decimal_places=2, default=0)
    column1_max = models.DecimalField('колонка 1 макс', max_digits=11, decimal_places=2, default=0)

    column2_min = models.DecimalField('колонка 2 мин', max_digits=11, decimal_places=2, default=0)
    column2_max = models.DecimalField('колонка 2 макс', max_digits=11, decimal_places=2, default=0)

    column3_min = models.DecimalField('колонка 3 мин', max_digits=11, decimal_places=2, default=0)
    column3_max = models.DecimalField('колонка 3 макс', max_digits=11, decimal_places=2, default=0)

    def as_dict(self):
        data = {
            'column1_min': round(self.column1_min),
            'column1_max': round(self.column1_max),
            'column2_min': round(self.column2_min),
            'column2_max': round(self.column2_max),
            'column3_min': round(self.column3_min),
            'column3_max': round(self.column3_max),
            'rows': []
        }
        for row in self.rows.all().order_by('area'):
            data['rows'].append(row.as_dict())
        return data

    @staticmethod
    def get_instance():
        if ShippingTable.objects.count() == 0:
            ShippingTable.objects.create()
        return ShippingTable.objects.first()

    def calculate_price(self, point, basket_total):
        available_areas = list(self.rows.order_by('area').distinct('area').values_list('area', flat=True))
        area_name = AreaResolver.resolve(point, available_areas)
        if not area_name:
            return None

        column_number = -1
        for i in range(1, 4):
            min_price = getattr(self, 'column%s_min' % (i,))
            max_price = getattr(self, 'column%s_max' % (i,))

            available = basket_total >= min_price
            if available and min_price < max_price:
                available = basket_total < max_price

            if available:
                column_number = i
                break

        if column_number == -1:
            return None

        return self.rows.get(area=area_name).get_price(column_number)

    def __str__(self):
        return 'таблица доставки'

    class Meta:
        verbose_name = 'таблица доставки'
        verbose_name_plural = 'таблица доставки'


class ShippingTableRow(models.Model):
    AREAS = (
        (AreaResolver.AREA_TTK, 'Третье транспортное кольцо'),
        (AreaResolver.AREA_MKAD, 'МКАД'),
        (AreaResolver.AREA_MMK, 'Московское малое кольцо'),
        (AreaResolver.AREA_BMK, 'Большком московское кольцо'),
    )

    table = models.ForeignKey(ShippingTable, verbose_name='таблица', on_delete=models.CASCADE, related_name='rows')
    title = models.CharField('название', max_length=1024)
    order_field = models.PositiveIntegerField('нажать и тащить', default=0)
    area = models.PositiveSmallIntegerField('территория', choices=AREAS)
    column1 = models.DecimalField('колонка 1', max_digits=11, decimal_places=2, default=0)
    column2 = models.DecimalField('колонка 2', max_digits=11, decimal_places=2, default=0)
    column3 = models.DecimalField('колонка 3', max_digits=11, decimal_places=2, default=0)

    def get_area_coordinates(self):
        return AreaResolver.AREAS[self.area] if self.area else []

    def get_price(self, column_number):
        return getattr(self, 'column' + str(column_number))

    def as_dict(self):
        return {
            'title': self.title,
            'column1': round(self.column1),
            'column2': round(self.column2),
            'column3': round(self.column3),
            'area': self.get_area_coordinates()
        }

    def __str__(self):
        return 'цена'

    class Meta:
        verbose_name = 'цена'
        verbose_name_plural = 'цена'
        ordering = ['order_field', ]


class ShippingVideoReview(models.Model):
    video = EmbedVideoField(verbose_name='видео')
    title = models.CharField(verbose_name='заголовок', max_length=255)
    subtitle = models.CharField(verbose_name='подзаголовок', max_length=255)

    class Meta:
        verbose_name = 'видео отзыв'
        verbose_name_plural = 'видео отзывы'


try:
    ShippingTable.get_instance()
except OperationalError:
    # catch errors to migrate empty db
    pass
