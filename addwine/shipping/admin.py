from adminsortable2.admin import SortableInlineAdminMixin
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import reverse

from oscar.apps.shipping import admin as shipping_admin

from shipping.models import OrderAndItemCharges, ShippingVideoReview, ShippingTable, ShippingTableRow

admin.site.unregister(OrderAndItemCharges)


@admin.register(OrderAndItemCharges)
class OrderChargesAdmin(shipping_admin.OrderChargesAdmin):
    fields = ['name', 'description', 'price_per_order', 'use_for_pickup', 'require_address', 'use_shipping_table']
    list_display = ['name', 'price_per_order', 'use_for_pickup']


@admin.register(ShippingVideoReview)
class ShippingVideoReviewAdmin(admin.ModelAdmin):
    fields = ['video', 'title', 'subtitle']
    list_display = ['title']


class ShippingTableRowAdmin(SortableInlineAdminMixin, admin.TabularInline):
    model = ShippingTableRow
    min_num = 1
    extra = 0


@admin.register(ShippingTable)
class ShippingTableAdmin(admin.ModelAdmin):
    fields = [('column1_min', 'column1_max'), ('column2_min', 'column2_max'), ('column3_min', 'column3_max')]
    inlines = [ShippingTableRowAdmin, ]

    def response_change(self, request, obj):
        return HttpResponseRedirect(reverse('admin:index'))

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False
