import html

from dal import autocomplete
from django import forms
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.utils.html import strip_tags

from sommeliers.models import SommelierTask, is_sommelier, SommelierTaskSubmited


class SommelierTaskAdminBaseForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        widgets = {
            'product': autocomplete.ModelSelect2(url='autocomplete_product'),
            'sommelier': autocomplete.ModelSelect2(url='autocomplete_sommelier'),
        }


class SommelierTaskAdminBase(admin.ModelAdmin):
    AUTO_FILLED_MARK = '(автозаполнение)'

    list_display = ('name', 'cta_name', 'amocrm_id', 'is_complete', 'is_free', 'created_date')
    list_filter_default = ('is_complete', 'is_free', 'created_date')
    list_filter_sommelier = ('is_complete', 'created_date',)
    readonly_fields = ('cta_name', 'cta_question', 'name', 'email', 'phone', 'amocrm_id', 'created_date')
    form = SommelierTaskAdminBaseForm

    def _mark_as_auto_filled(self, data):
        return data + ' ' + self.AUTO_FILLED_MARK

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['is_sommelier'] = is_sommelier(request.user)
        return super(SommelierTaskAdminBase, self).changeform_view(request, object_id, form_url, extra_context)

    def get_list_filter(self, request):
        if is_sommelier(request.user):
            return self.list_filter_sommelier
        return self.list_filter_default

    def get_fields(self, request, obj=None):
        if obj:
            target_field = ('product',)
        else:
            target_field = ('product',)

        base_task_info = ('sommelier', 'cta_name', 'cta_question',)
        base_task_contacts = ('name', 'email', 'phone')
        if request.user.is_superuser or (is_sommelier(request.user) and request.user == obj.sommelier):
            task_info = base_task_info + base_task_contacts
        else:
            task_info = base_task_info + ('name',)

        fields = task_info + target_field
        if obj:
            fields += ('created_date',)
        return fields

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser:
            return super().get_readonly_fields(request, obj=obj)
        return self.get_fields(request, obj)

    def name(self, obj):
        full_name = obj.contacts.name
        if obj.contacts.auto_filled_name:
            full_name = self._mark_as_auto_filled(full_name)
        return full_name if full_name else 'Имя неизвестно'

    name.short_description = 'имя клиента'

    def email(self, obj):
        email = obj.contacts.email
        if obj.contacts.auto_filled_email:
            email = self._mark_as_auto_filled(email)
        return email

    email.short_description = 'email'

    def phone(self, obj):
        phone_number = obj.contacts.phone_number.as_international
        phone_number = '<a href=\"tel:'+phone_number+'\">'+phone_number+'</a>'
        if obj.contacts.auto_filled_phone_number:
            phone_number = self._mark_as_auto_filled(phone_number)
        return phone_number

    phone.allow_tags = True
    phone.short_description = 'телефон'

    def cta_name(self, obj):
        return obj.cta.title

    cta_name.short_description = 'название вопроса'

    def cta_question(self, obj):
        return html.unescape(strip_tags(obj.cta.text))

    cta_question.short_description = 'содержание вопроса'

    def has_add_permission(self, request):
        return False


@admin.register(SommelierTask)
class SommelierTaskAdmin(SommelierTaskAdminBase):
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if is_sommelier(request.user):
            qs = qs.filter(is_free=True)
        return qs

    def is_accept_request(self, request):
        return '_accept' in request.POST

    def save_model(self, request, obj, form, change):
        if self.is_accept_request(request):
            if not obj.sommelier_id:
                obj.sommelier = request.user
        super().save_model(request, obj, form, change)

    def response_change(self, request, obj):
        if not self.is_accept_request(request):
            return super().response_change(request, obj)

        return HttpResponseRedirect(reverse('admin:sommeliers_sommeliertasksubmited_change', args=(obj.pk,)))


@admin.register(SommelierTaskSubmited)
class SommelierTaskSubmitedAdmin(SommelierTaskAdminBase):
    list_display = ('name', 'cta_name', 'is_complete', 'submited_date', 'created_date')
    list_filter_default = ('created_date',)

    def get_actions(self, request):
        actions = super(SommelierTaskSubmitedAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        return super().get_queryset(request).filter(sommelier=request.user)

    def is_complete_request(self, request):
        return '_complete' in request.POST

    def save_model(self, request, obj, form, change):
        if self.is_complete_request(request):
            obj.is_complete = True
        super().save_model(request, obj, form, change)

    def response_change(self, request, obj):
        if not self.is_complete_request(request):
            return super().response_change(request, obj)

        return HttpResponseRedirect(reverse('admin:sommeliers_sommeliertasksubmited_changelist', ))
