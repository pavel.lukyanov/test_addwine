from admin_tools.menu.items import MenuItem

from sommeliers.models import SommelierTask


class SommelierMenuItem(MenuItem):
    template = "admin/sommeliers/menu/item.html"

    def __init__(self, title=None, url=None, **kwargs):
        super().__init__(title, url, **kwargs)
        self.only_my = kwargs.get('only_my', False)

    def init_with_context(self, context):
        super().init_with_context(context)

        if self.only_my:
            tasks = SommelierTask.objects.filter(is_complete=False, sommelier=context['request'].user)
        else:
            tasks = SommelierTask.objects.filter(is_free=True)
        self.remaining_count = tasks.count()