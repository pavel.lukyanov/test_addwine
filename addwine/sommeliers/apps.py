from django.apps import AppConfig


class SommeliersConfig(AppConfig):
    name = 'sommeliers'
    verbose_name = 'Сомелье'
