from ratelimit import RateLimitException

from yandex_market.utils import yandex_market_update_catalog, google_merchant_update_feed


class YandexMarketUpdateAdminMixin(object):
    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        try:
            yandex_market_update_catalog()
            google_merchant_update_feed()
        except RateLimitException:
            pass


class YandexMarketUpdateRelatedAdminMixin(object):
    def save_related(self, request, form, formsets, change):
        super().save_related(request, form, formsets, change)
        try:
            yandex_market_update_catalog()
            google_merchant_update_feed()
        except RateLimitException:
            pass
