from django.apps import AppConfig


class YandexMarketConfig(AppConfig):
    name = 'yandex_market'

    def ready(self):
        from . import receivers


