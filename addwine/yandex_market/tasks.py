from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.contrib.sites.models import Site
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.timezone import now

from yandex_market import settings as app_settings
from django.test import RequestFactory

request_factory = RequestFactory()

logger = get_task_logger(__name__)

YANDEX_MARKET_EXPORTER_TEMPLATE = 'yandex_market/market_exporter.html'

TASKS_CONFIGS_UPDATE_CATALOG = {
    'name': "yandex_market.update",
    'retry_kwargs': {'max_retries': 3},
    'ignore_result': True,
    'default_retry_delay': 30
}
TASKS_CONFIGS_UPDATE_CATALOG.update(settings.YANDEX_MARKET_TASK_CONFIG)


@shared_task(**TASKS_CONFIGS_UPDATE_CATALOG)
def update_catalog():
    from partner.strategy import Selector
    from catalogue.models import Category, Product

    request = request_factory.get(reverse('yandex_market:yandex_market_export'))
    request.META['HTTP_HOST'] = Site.objects.get(id=settings.SITE_ID).domain
    request.strategy = Selector().strategy()
    context = {
        'request': request,
        'scheme': 'https',
        'categories': Category.objects.filter(is_active=True),
        'products': Product.objects.filter(is_active=True, is_exported_in_ym=True),
        'products_with_presents': Product.objects.filter(is_active=True, is_exported_in_ym=True)
                                                 .exclude(present=None).select_related('present'),
        'now': now()
    }

    content = render_to_string(YANDEX_MARKET_EXPORTER_TEMPLATE, context)
    with open(app_settings.CATALOG_FILE_PATH_TMP, 'w+', encoding="utf-8") as static_file:
        static_file.write(content)


TASKS_CONFIGS_UPDATE_CATALOG.update({
    'name': 'google_merchant.update',
})

GOOGLE_MERCHANT_EXPORTER_TEMPLATE = 'yandex_market/google_exporter.html'


@shared_task(**TASKS_CONFIGS_UPDATE_CATALOG)
def update_google_merchant():
    from partner.strategy import Selector
    from catalogue.models import Category, Product

    request = request_factory.get(reverse('yandex_market:google_merchant_export'))
    request.META['HTTP_HOST'] = Site.objects.get(id=settings.SITE_ID).domain
    request.strategy = Selector().strategy()
    context = {
        'request': request,
        'categories': Category.objects.filter(is_active=True),
        'products': Product.objects.filter(is_active=True, is_exported_in_gm=True),
        'now': now()
    }

    content = render_to_string(GOOGLE_MERCHANT_EXPORTER_TEMPLATE, context)
    with open(app_settings.GOOGLE_MERCHANT_FILE_PATH_TMP, 'w+', encoding="utf-8") as static_file:
        static_file.write(content)
