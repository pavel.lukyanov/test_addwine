from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from ratelimit import RateLimitException

from catalogue.models import Product
from catalogue.signals import product_saved_related
from common.models import Settings
from yandex_market.utils import yandex_market_update_catalog, google_merchant_update_feed


@receiver(post_save, sender=Settings)
def handle_save_settings(sender, instance, **kwargs):
    try:
        yandex_market_update_catalog()
        google_merchant_update_feed()
    except RateLimitException:
        pass


@receiver(product_saved_related, sender=Product)
def handle_save(sender, instance, **kwargs):
    try:
        yandex_market_update_catalog()
        google_merchant_update_feed()
    except RateLimitException:
        pass


@receiver(post_delete, sender=Product)
def handle_delete(sender, instance, **kwargs):
    try:
        yandex_market_update_catalog()
        google_merchant_update_feed()
    except RateLimitException:
        pass
