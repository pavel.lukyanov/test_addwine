import os

from django.conf import settings

CATALOG_FILE_PATH = os.path.join(settings.BASE_DIR, 'yandex_market', 'yandex_market_catalog.yml')
CATALOG_FILE_PATH_TMP = os.path.join(settings.BASE_DIR, 'yandex_market', 'yandex_market_catalog_tmp.yml')

GOOGLE_MERCHANT_FILE_PATH = os.path.join(settings.BASE_DIR, 'yandex_market', 'google_merchant_catalog.xml')
GOOGLE_MERCHANT_FILE_PATH_TMP = os.path.join(settings.BASE_DIR, 'yandex_market', 'google_merchant_catalog_tmp.xml')
