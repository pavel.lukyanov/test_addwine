from django.conf import settings

from unisender_api.tasks import sync_contact_task


def unisender_sync_user(user_id):
    if settings.UNISENDER_SYNC_ENABLED:
        sync_contact_task.apply_async(args=(user_id,), countdown=1)