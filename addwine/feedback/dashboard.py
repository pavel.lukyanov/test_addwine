from admin_tools.dashboard import modules, get_admin_site_name


class FeedbackModelList(modules.ModelList):
    template = "feedback/admin/model_list.html"

    def get_notread_url(self, model, context):
        app_label = model._meta.app_label
        return '/%s/%s/%s' % (get_admin_site_name(context),
                              app_label,
                              model.__name__.lower()) + "/?watched__exact=0"

    def __init__(self, title=None, models=None, exclude=None, counter_models=None, **kwargs):
        super(FeedbackModelList, self).__init__(title, models, exclude, **kwargs)
        self.counter_models = counter_models


    def init_with_context(self, context):
        super(FeedbackModelList, self).init_with_context(context)

        items = self._visible_models(context['request'])
        for index, data in enumerate(items):
            model = data[0]
            model_name = model.__module__+"."+model.__name__
            if model_name in self.counter_models:
                self.children[index]['notread_url'] = self.get_notread_url(model, context)
                self.children[index]['notread_count'] = len(model.objects.filter(watched=False))
