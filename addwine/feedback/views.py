import importlib
import logging

from django.http import HttpResponseRedirect
from django.views import View

from amocrm_api import utils
from feedback.forms import BackCallForm, QuestionForm, SliderCTARequestForm
from feedback.utils import is_need_notify_on_mail, send_mail, get_comm_event_obj

logger = logging.getLogger(__name__)

class FormResultHandlerMixin(object):
    def get_context_data(self, **kwargs):
        context = super(FormResultHandlerMixin, self).get_context_data(**kwargs)
        last_form_data = self.request.session.pop('feedback_last_form_data', None)
        if last_form_data:
            form_class_str = last_form_data['form_class']
            app_name, module_name, class_name = form_class_str.split(".")
            app = importlib.import_module(app_name)
            module = getattr(app, module_name)
            form_class = getattr(module, class_name)
            form = form_class(initial=last_form_data['data'])
            context['form_' + last_form_data['form_name']] = form
        return context


class FeedbackView(View):
    form_class = None
    email_template_code = None
    form_name = 'default'
    amocrm_sync_method_name = ''

    def generate_response(self, request, form, success):
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', "/") + "#success=" + str(success)
                                    + '&form_name=' + self.form_name)

    def model_to_dict(self, obj):
        return obj.as_dict()

    def get_comm_event_obj(self):
        return get_comm_event_obj(self.email_template_code)

    def get_data_from_request(self, request):
        return request.POST

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=self.get_data_from_request(request))
        if form.is_valid():
            form.instance.name = form.cleaned_data['name']
            obj = form.save()

            comm_event_obj = self.get_comm_event_obj()

            if self.amocrm_sync_method_name:
                sync_method = getattr(utils, self.amocrm_sync_method_name)
                if not sync_method:
                    logger.critical('Sync method with name defined for the model %s, but does not exists'
                                    % self.amocrm_sync_method_name)
                    return
                sync_method(obj.id)

            if is_need_notify_on_mail() and comm_event_obj:
                send_mail(comm_event_obj, self.model_to_dict(obj))
            success = True
        else:
            success = False
            request.session['feedback_last_form_data'] = {
                'form_name': self.form_name,
                'form_class': self.form_class.__module__ + "." + self.form_class.__name__,
                'data': form.data
            }
        return self.generate_response(request, form, success)


class BackCallView(FeedbackView):
    amocrm_sync_method_name = 'amocrm_sync_backcall'
    form_class = BackCallForm
    email_template_code = 'BACKCALL'
    form_name = 'backcall'


class QuestionView(FeedbackView):
    amocrm_sync_method_name = 'amocrm_sync_question'
    form_class = QuestionForm
    email_template_code = 'QUESTION'
    form_name = 'question'


class SliderCTARequestView(FeedbackView):
    form_class = SliderCTARequestForm
    email_template_code = 'SLIDER_CTA_REQUEST'
    form_name = 'slider_cta_request'


