import logging

from common.utils import get_admin_settings
from customer.models import CommunicationEventType
from .tasks import send_message_with_template

logger = logging.getLogger(__name__)


def send_mail(comm_event_obj, ctx=None, recipients=None):
    send_message_with_template.delay(comm_event_obj.id, ctx=ctx, recipients=recipients)


def is_need_notify_on_mail():
    return get_admin_settings().send_notify_on_mail


def get_comm_event_obj(template_code):
    try:
        return CommunicationEventType.objects.get(code=template_code)
    except CommunicationEventType.DoesNotExist:
        logger.error("Built-in email template with code \'%s\' does not exists" % (template_code,))
        return None
