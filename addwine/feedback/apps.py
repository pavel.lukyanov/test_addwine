#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.apps import AppConfig


class Config(AppConfig):
    name = 'feedback'
    verbose_name = u'Обратная связь'
