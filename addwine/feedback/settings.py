from django.conf import settings

FEEDBACK_TARGET = getattr(settings, 'FEEDBACK_TARGET', "netral23@yandex.ru")
FEEDBACK_ADMIN_SETTINGS_NEED_NOTIFY_FIELD = getattr(settings, 'FEEDBACK_ADMIN_SETTINGS_NEED_NOTIFY_FIELD',
                                                    "need_notify_on_mail")
