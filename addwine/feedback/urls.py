from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'question_send$', views.QuestionView.as_view(), name="question_send"),
    url(r'backcall_send$', views.BackCallView.as_view(), name="backcall_send"),
    url(r'slider_cta_request_send', views.SliderCTARequestView.as_view(), name="slider_cta_request_send"),
]
