from django.db.models import Q
from django.http import Http404, HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import reverse

from catalogue.models import Brand
from catalogue.views import PopularProductsMixin
from common.models import DocumentAboutCompany, VideoReviewAboutCompany
from common.views import PageView
from faq.models import FAQCategory
from feedback.forms import QuestionForm
from marketing.models import SEOText
from marketing.views import SommelierCardsMixin
from news import models as news_models
from news.forms import CommentForm
from news.utils import is_available_for_vote
from offer.models import RangeDiscount
from payment.models import PaymentMethod
from products_editor.utils import is_products_editor
from shipping.models import ShippingVideoReview, ShippingTable
from django.conf import settings
from django.contrib.admin.sites import site as admin_site

from sommeliers.models import is_sommelier
from voucher.models import VoucherGroup


class TemplateResponse404(TemplateResponse):
    status_code = 404


class Error404PageView(PageView):
    template_name = '404.html'
    page_code = 'error-404'
    response_class = TemplateResponse404


class AboutPageView(SommelierCardsMixin, PageView):
    template_name = 'web/about.html'
    page_code = 'about'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['videos'] = VideoReviewAboutCompany.objects.all()
        context['docs'] = DocumentAboutCompany.objects.all()
        return context


class ContactsPageView(PageView):
    page_code = 'contacts'
    template_name = 'web/contacts.html'


class FaqPageView(PageView):
    page_code = 'faq'
    template_name = 'web/faq.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = FAQCategory.objects.filter(depth=1)
        context['form_question'] = QuestionForm()
        return context


class ArticlesPageBaseMixin(PopularProductsMixin, object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = news_models.Category.objects.all()
        context['popular_articles'] = news_models.Article.objects.order_by('rating')[
                                      :settings.POPULAR_ARTICLES_COUNT]
        return context


class ArticlesPageView(ArticlesPageBaseMixin, PageView):
    page_code = 'articles'
    template_name = 'web/articles.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['articles'] = news_models.Article.objects.all().order_by('-created')
        category_slug = kwargs.get('category_slug')
        if category_slug:
            try:
                context['category'] = news_models.Category.objects.get(slug=category_slug)
                context.update(self.extract_seo_configs(context['category']))
            except news_models.Category.DoesNotExist:
                raise Http404
            context['articles'] = context['articles'].filter(category=context['category']).order_by('-created')
            context['seo_texts'] = SEOText.objects.filter(article_categories__category=context['category'])
        search_query = self.request.GET.get('search')
        if search_query:
            try:
                context['articles'] = context['articles'].filter(Q(title__search=search_query) |
                                                                 Q(text__search=search_query)).order_by('-created')
            except NotImplementedError:
                pass
        return context


class ArticlePageView(ArticlesPageBaseMixin, PageView):
    template_name = 'web/article.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category_slug = kwargs.get('category_slug')
        article_slug = kwargs.get('article_slug')
        try:
            context['category'] = news_models.Category.objects.get(slug=category_slug)
        except news_models.Category.DoesNotExist:
            raise Http404
        try:
            context['article'] = news_models.Article.objects.get(slug=article_slug)
            context.update(self.extract_seo_configs(context['article']))
        except news_models.Article.DoesNotExist:
            raise Http404

        available_for_vote, voted_articles = is_available_for_vote(self.request, context['article'])
        context['mail_cta'] = context['category'].mail_cta
        context['available_for_vote'] = available_for_vote
        context['comment_form'] = CommentForm()
        context['breadcrumbs'] = [
            {
                'title': 'статьи',
                'href': reverse('articles')
            },
            {
                'title': context['category'].title,
                'href': context['category'].get_absolute_url()
            },
            {
                'title': context['article'].title,
            },
        ]
        return context


class DeliveryPageView(SommelierCardsMixin, PageView):
    template_name = 'web/delivery.html'
    page_code = 'delivery'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['payment_methods'] = PaymentMethod.objects.filter(enabled=True)
        context['videos'] = ShippingVideoReview.objects.all()
        context['shipping_table'] = ShippingTable.get_instance()
        return context


class PersonalPageView(PageView):
    template_name = 'web/personal.html'
    page_code = 'personal'


class ConfidentialDataPageView(PageView):
    template_name = 'web/confidential.html'
    page_code = 'confidential'


class OffertPageView(PageView):
    template_name = 'web/offert.html'
    page_code = 'offert'


class RangeDiscountPageView(PageView):
    template_name = 'web/sale.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sale_slug = kwargs.get('sale_slug')
        try:
            context['sale'] = RangeDiscount.objects.get(code=sale_slug)
        except RangeDiscount.DoesNotExist:
            raise Http404()
        context.update(self.extract_seo_configs(context['sale']))

        context['mail_cta'] = context['sale'].mail_cta

        return context


class VoucherGroupPageView(PageView):
    template_name = 'web/sale.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sale_slug = kwargs.get('sale_slug')
        try:
            context['sale'] = VoucherGroup.objects.get(code__iexact=sale_slug)
        except VoucherGroup.DoesNotExist:
            raise Http404()
        context.update(self.extract_seo_configs(context['sale']))

        context['mail_cta'] = context['sale'].mail_cta

        return context


class BrandPageView(PageView):
    template_name = 'web/brand.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['brands'] = Brand.objects.all()
        brand_slug = kwargs.get('slug')
        try:
            context['brand'] = Brand.objects.get(slug=brand_slug)
            context.update(self.extract_seo_configs(context['brand']))
        except Brand.DoesNotExist:
            raise Http404
        context['mail_cta'] = context['brand'].mail_cta
        context['brand_popular_products'] = context['brand'].products.filter(is_active=True) \
                                                .order_by('stats__score')[:settings.BRAND_POPULAR_PRODUCTS_COUNT]
        return context


def admin_index_override(request):
    if not request.user.is_authenticated():
        return admin_site.login(request)
    if is_sommelier(request.user):
        return HttpResponseRedirect(reverse('admin:sommeliers_sommeliertask_changelist'))
    elif is_products_editor(request.user):
        return HttpResponseRedirect(reverse('admin:products_editor_productseditorproduct_changelist'))
    else:
        return admin_site.index(request)
