from django.contrib.sitemaps import GenericSitemap

from catalogue.models import Brand
from common.views import PageSitemap

sitemaps = {
    'web': PageSitemap(['about', 'contacts', 'faq', 'articles', 'delivery', 'personal']),
}