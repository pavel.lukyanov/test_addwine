from django.conf.urls import url

from web.views import AboutPageView, ContactsPageView, FaqPageView, ArticlesPageView, DeliveryPageView, \
    PersonalPageView, BrandPageView, ArticlePageView, ConfidentialDataPageView, \
    VoucherGroupPageView, RangeDiscountPageView, OffertPageView, admin_index_override

urlpatterns = [
    url(r'about$',
        AboutPageView.as_view(),
        name='about'
    ),
    url(r'contacts$',
        ContactsPageView.as_view(),
        name='contacts'
    ),
    url(r'faq$',
        FaqPageView.as_view(),
        name='faq'
    ),

    url(r'articles$', ArticlesPageView.as_view(), name='articles'),
    url(r'articles/(?P<category_slug>[\w-]+)$', ArticlesPageView.as_view(), name='article_category'),
    url(r'articles/(?P<category_slug>[\w-]+)/(?P<article_slug>[\w-]+)$',
        ArticlePageView.as_view(),
        name='article'
    ),

    url(r'sales/(?P<sale_slug>[\w-]+)$', RangeDiscountPageView.as_view(), name='discount'),
    url(r'voucher/(?P<sale_slug>[\w-]+)$', VoucherGroupPageView.as_view(), name='voucher'),

    url(r'delivery$',
        DeliveryPageView.as_view(),
        name='delivery'
    ),
    url(r'policy-personal-data$',
        PersonalPageView.as_view(),
        name='personal'
    ),
    url(r'confidential-policy$',
        ConfidentialDataPageView.as_view(),
        name='confident'
    ),
    url(r'offert$',
        OffertPageView.as_view(),
        name='offert'
    ),
    url(r'brands/(?P<slug>[\w-]+)',
        BrandPageView.as_view(),
        name='brand'
    ),
]
