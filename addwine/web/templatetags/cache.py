import cacheops
from cacheops.templatetags.cacheops import register
from django.conf import settings


@register.decorator_tag
def cached_forever(queryset, fragment_name, *extra):
    return cacheops.cached_as(queryset, timeout=settings.CACHE_TTL_FOREVER, extra=(fragment_name,) + extra)