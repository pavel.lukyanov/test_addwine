function inside(vs, x, y) {
    // ray-casting algorithm based on
    // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

    var inside = false;
    for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
        var xi = vs[i][0], yi = vs[i][1];
        var xj = vs[j][0], yj = vs[j][1];

        var intersect = ((yi > y) != (yj > y))
            && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
        if (intersect) inside = !inside;
    }

    return inside;
};


$(function () {
    var CHARGE_UNDEFINED = "-";
    var basketTotal=parseInt($("tr.basket-total .price").html());

    var $shippingPriceField=$("tr.shipping .price");
    var $payTotal=$("tr.pay-total .price");
    var $shippingDatePicker = $('#shippingDatePicker');
    $shippingDatePicker.datepicker({
        leftArrow: '<i class="fa fa-long-arrow-left"></i>',
        rightArrow: '<i class="fa fa-long-arrow-right"></i>',
        language: 'ru',
        startDate: 'today',
        autoclose: true
    });
    $shippingDatePicker.datepicker('update', new Date());
    $shippingDatePicker.find('.prev').html('<');

    var $widgetsSec=$('.widgets-sec');
    var $calculateBtn = $(".form-group.rasch .btn__gold");
    var $addressField = $("#address");
    var $addressRegion = $("input[name='address_region']");
    var $addressCity = $("input[name='address_city']");
    var $addressStreet = $("input[name='address_street']");
    var $addressHouse = $("input[name='address_house']");
    var $addressFlat = $("input[name='address_flat']");
    var $addressBlock = $("input[name='address_block']");
    var addressLat;
    var addressLng;

    var REQUIRED_ADDRESS_FIELDS = [
        {
            code: 'region',
            title: 'регион'
        },
        {
            code: 'city',
            title: 'город'
        },
        {
            code: 'street',
            title: 'улица'
        },
        {
            code: 'house',
            title: 'номер дома'
        }
        /*,{
            code: 'flat',
            title: 'номер квартиры'
        }*/
    ];

    $addressField.suggestions({
        token: "94229bb94c221c3a3e870f87248a6abbd24adcd8",
        type: "ADDRESS",
        onSelect: function (address, changed) {
            address = address.data;
            $addressRegion.val(address.region);
            $addressCity.val(address.city);
            $addressStreet.val(address.street);
            $addressHouse.val(address.house);
            $addressFlat.val(address.flat);
            $addressBlock.val(address.block);

            addressLat = address.geo_lat;
            addressLng = address.geo_lon;

            updateShippingPrice(addressLat, addressLng);
        }
    });
    var suggestions = $addressField.data("suggestions");

    var $addressSection = $(".sdress-sec");

    window.Parsley.addValidator('validateAddress', {
        validateString: function (value, requirement) {
            if ($addressSection.hasClass("disabled"))
                return true;

            var valid = true;
            var errorMsg="Выберите адрес из списка";
            var address = suggestions.selection;


            if (address) {
                var missedTypes="";
                address = address.data;
                REQUIRED_ADDRESS_FIELDS.forEach(function (field) {
                    if (!address[field.code]) {
                        missedTypes += field.title + ", ";
                        valid = false;
                    }
                });

                missedTypes = missedTypes.substr(0, missedTypes.length - 2);
                errorMsg='Укажите полный адрес. Вы пропустили: ' + missedTypes + '.';
            }else
                valid = false;

            if (!valid)
                window.Parsley.addMessage('ru', 'validateAddress', errorMsg);

            return valid;
        },
        requirementType: 'string'
    });

    var updateAddressSectionState = function () {
        if ($("input[name='shipping_method']:checked").attr("data-require-address") === "True") {
            $addressSection.removeClass("disabled");
        } else
            $addressSection.addClass("disabled");
        updateShippingPrice(addressLat, addressLng);
    };
    
    var calculateWithShippingTable=function (basketTotal, lat, lng) {
        var columnNumber=-1;

        for(var i=1;i<=3;i++) {
            var columnMin = SHIPPING_TABLE['column' + i + '_min'];
            var columnMax =  SHIPPING_TABLE['column' + i + '_max'];

            var available=basketTotal>=columnMin;

            if(available && columnMin<columnMax)
                available = basketTotal<=columnMax;
            if (available) {
                columnNumber = i;
                break;
            }
        }
        if(columnNumber===-1)
            return null;

        for(i=0;i<SHIPPING_TABLE.rows.length;i++){
            var row=SHIPPING_TABLE.rows[i];
            if(inside(row.area, lat, lng))
                return row['column'+columnNumber]
        }
        return null;
    };

    var updateShippingPrice=function (lat, lng) {
        var $selectedShippingMethod=$("input[name='shipping_method']:checked");
        var useShippingTable=$selectedShippingMethod.attr('data-use-shipping-table');

        var charge=null;
        if(useShippingTable==='True')
            charge=calculateWithShippingTable(basketTotal, lat, lng);
        if(charge===null) {
            charge = CHARGE_UNDEFINED;//$selectedShippingMethod.attr("data-charge");
            if($addressField.val().trim().length > 0)
                $widgetsSec.addClass('charge-undefined');
        }else {
            $widgetsSec.removeClass('charge-undefined');
            $payTotal.html(basketTotal+charge);
        }

        $shippingPriceField.html(charge);
    };

    var $shippingMethodSelectField=$("input[name='shipping_method']");
    $shippingMethodSelectField.on("change", function () {
        $('.shipping-method-row').removeClass("selected");
        $(this).closest('.shipping-method-row').addClass("selected");
        updateAddressSectionState();
    });
    $calculateBtn.click(function () {
         $addressField.trigger("change");
    });
    updateAddressSectionState();
    suggestions.fixData();
});