new Vue({
    delimiters: ['[[', ']]'],
    el: '#time-search',
    data () {
        return {
            active: true,
            width: 1024,
            text: '',
            result: false,
            results: [],
            setTime: null,
            searchActive: false,
            noresult: false,
            showSearch: false
        }
    },
    mounted () {
        this.showSearch = true
        if (window.innerWidth > 991) {
            this.active = false
        }
        window.addEventListener("resize", this.resizeSearch);
    },
    methods: {
        activeSearch () {
            let self = this;
            if (window.innerWidth >= 991) {
                document.onclick = function(e) {
                    let elem = e.target.closest('.search')
                    if (!elem) {
                        self.active = false
                        self.result = false
                    }
                }

                this.active = !this.active
                this.$nextTick(function () {
                    if (this.active) {
                        let inp = document.querySelector(".search-field")
                        inp.focus()
                    }
                })
            }
        },
        searchByText: _.debounce(async function () {
            if (this.text.length > 2) {
                this.searchActive = true
                this.result = false
                this.noresult = false
                try {
                    this.results = await axios.get('/api/v1/product/search/?q=' + this.text)
                    this.results = this.results.data
                } catch (err) {
                    this.results = []
                }
                if (this.results.length === 0) {
                    this.noresult = true
                    this.searchActive = false
                } else {
                    this.result = true
                }
                if (this.setTime !== null) {
                    clearInterval(this.setTime)
                }
                this.sendData.bind(this)
                this.setTime = setTimeout(this.sendData, 2000)
            } else {
                this.searchActive = false
                this.result = false
                this.noresult = false
            }
        }, 300),
        async sendData () {
            if (this.text.length > 3) {
                let sendText = this.text
                sendText = sendText.toLowerCase()
                sendText = sendText.split(' ').sort().join(' ')
                let res = await axios.post('/api/v1/product/search/save', {
                    text: sendText
                })
            }
        },
        resizeSearch () {
            if (window.innerWidth < 991) {
                this.active = true
            }
        },
        toSearchResult () {
            if (this.text.length > 3) {
                this.sendData()
                let urlSearch = '/search/load/' + this.text + '/'
                document.location.href = urlSearch;
            }
        }
    }
})
