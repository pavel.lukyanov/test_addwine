$(".cat-product .basket-change-form, .product .basket-change-form").livequery(function () {
    var $this=$(this);
    var listenerName=$this.attr("data-name");
    var $loadingOverlay=$this.closest(".product, .cat-product").find(".loading-overlay");

    var startLoading=function () {
        $loadingOverlay.addClass("active");
    };

    var stopLoading=function () {
        $loadingOverlay.removeClass("active");
    };

    if(!listenerName)
        return;


    BASKET.addListener(listenerName, {
        beforeSubmit: function () {
            startLoading();
        },
        success: function () {
            stopLoading()
        },
        error: function () {
            stopLoading()
        }
    });
});
