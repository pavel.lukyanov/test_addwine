$(function () {
    var $rateBlock=$(".rate-block") ;
    var $rates=$rateBlock.find(".rates");
    var $rating=$rates.find(".rating");
    var articleId=$rates.attr("data-article-id");
    var addRateUrl=$rates.attr('data-rate-url');

    if($rateBlock.hasClass("available-for-vote"))
        $rateBlock.find("a").click(function (e) {
            e.preventDefault();
            var rate=$(this).attr("data-value");
            sendRequest(addRateUrl, function (success, resp) {
                if(success) {
                    $rating.css('width', resp.rating + "%");
                    $rateBlock.removeClass("available-for-vote");
                    $rateBlock.find("a").unbind("click");
                }
            }, {article: articleId, rate: rate}, "POST")
        });
});