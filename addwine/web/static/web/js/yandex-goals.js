function YandexGoals(counterId) {
    var els = document.querySelectorAll("[data-yandex-goal]");

    var _reachGoal = function (goalName) {
        var yaCounter = window['yaCounter' + counterId];
        yaCounter.reachGoal('total');
        yaCounter.reachGoal(goalName);
    };

    for (var i = 0; i < els.length; i++) {
        var el = els[i];
        var createHandler = function () {
            var goalName = el.getAttribute("data-yandex-goal");
            return function (ev) {
                _reachGoal(goalName);
                return true;
            };
        }
        if (el.nodeName === 'FORM')
            $(el).submit(createHandler());
        else
            $(el).click(createHandler())
    }

    this.reachGoal = _reachGoal;
}
