function menuSide () {
	var slideout = new Slideout({
		'panel': document.getElementById('panel'),
		'menu': document.getElementById('menu'),
		'padding': 256,
		'tolerance': 70
	});
	var $headerTop=$(".header-top");

	// Toggle button
	var $mobileMenuPanelLocker=$('#mobileMenuPanelLocker');
	$('.humb').click(function() {
		slideout.open();
		if(!$headerTop.hasClass("affix"))
			return;
		$headerTop.css('position', 'absolute');
		$headerTop.css('top', $(window).scrollTop());
	});
	$('.slideoutClose, #mobileMenuPanelLocker').click(function() {
		slideout.close();
	});

	slideout.disableTouch();
	slideout.on("open", function() {
		$mobileMenuPanelLocker.addClass("active");
		$('body').css("overflow", 'hidden');
		slideout.enableTouch();
	});
	slideout.on("close", function() {
		$mobileMenuPanelLocker.removeClass("active");
		$('body').css("overflow", '');
		slideout.disableTouch();

		if(!$headerTop.hasClass("affix"))
			return;
		$headerTop.css('position', '');
		$headerTop.css('top', '');
	});
}
function menuCat () {
	$(".cat-btn").click(function ( e ) {
		$(".menu-aside__js").addClass('active');
		$(".menu-aside-background").addClass("active");
		$('body').css("overflow", "hidden");
	});
	$(".aside-close, .menu-aside-background").click(function ( e ) {
		$(".menu-aside__js").removeClass('active');
		$(".menu-aside-background").removeClass("active");
		$('body').css("overflow", "");
	});
}
function leaveComment () {
	$(".leaveComment-btn").click(function ( e ) {
		$(this).toggleClass('active');
		$(".leaveComment-content").slideToggle('slow');
	});
}
var toggleAffix = function(affixElement, scrollElement, wrapper) {
	var height = affixElement.outerHeight(),
		top = wrapper.offset().top;
	var $affixElement = $(affixElement);
	if($affixElement.data('offset-target'))
		top -= $($affixElement.data('offset-target')).outerHeight();

	if (scrollElement.scrollTop() >= top){
		wrapper.height(height);
		affixElement.addClass("affix");
	}
	else {
		affixElement.removeClass("affix");
		wrapper.height('auto');
	}
};
function stepsBuy () {
	$(".buy-section .dropdown-menu .nav-link").click(function ( e ) {
		var step = $(this).index()+1;
		var name = $(this).html();
		var setStep = $('.set-name').find('.shag');
		var setName = $('.set-name').find('.item-name');
		setStep.html(step);
		setName.html(name);
	});
	$(".haracters-section .dropdown-menu .nav-link").click(function ( e ) {
		var name = $(this).html();
		var setName = $('.set-name').find('.item-name');
		setName.html(name);
	});
}
function compSliderArrows () {
	var arrLeft = $(".slick-icon").html();
	var arrLeftsm = $(".slick-icon__sm").html();
	var dWidth = $(window).width();
	if (dWidth > 767){
		$(".slick-prev").html("").append(arrLeft);
		$(".slick-next").html("").prepend(arrLeft);
	}
	else{
		$(".slick-prev").html("").append(arrLeftsm);
		$(".slick-next").html("").prepend(arrLeftsm);
	}
}

function nameBtn() {
	var field = $(".name-field-js input[name=name]");
	var firstName = $(".name-field-js input[name=first-name]");
    var secondName = $(".name-field-js input[name=second-name]");

	if (field.length) {
		var initValues = field.val().split(" ");
		secondName.val(initValues[0] || "");
		firstName.val(initValues[1] || "");

		function updateValue() {
			var fullName = [secondName.val(), firstName.val()]
				.filter(function (item) { return item.length > 0 })
				.join(" ");
			field.val(fullName)
		}

		secondName.on('input', updateValue);
		firstName.on('input', updateValue);
	}
}
function dropBtn() {
	$(".dropbtn__js").click(function(e) {
		$(this).toggleClass('active');
	})
}
function faqBtn() {
	$(".questions-sec .panel").click(function(e) {
		$(this).toggleClass('active');
	})
}
function payBtnShow() {
	$(".btn-pay-more").click(function(e) {
		$('.pay-more').slideToggle();
	})
}
var allowSubmit = false;
function faqForm() {
	$(".show-faq-form").click(function(e) {
		$(this).toggleClass('active');
		$('.faq-form-sec').slideToggle();
	});

	$(".faq-form-sec form").submit(function(e) {
		if (!allowSubmit) {
			// If capcha not filled
			e.preventDefault();
		}
	});
}
function capcha_filled() {
    allowSubmit = true;
}
function capcha_expired() {
    allowSubmit = false;
}
$(document).ready(function() {
	$('.popular-slider').slick({
		infinite: true,
		slidesToShow: 4,
		dots: false,
		arrows: true,
		slidesToScroll: 4,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	$('.video-slider').slick({
		infinite: true,
		slidesToShow: 3,
		dots: false,
		arrows: true,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	$('.sommelier-slider').slick({
		infinite: true,
		slidesToShow: 3,
		dots: false,
		arrows: true,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	$('.brands-slider').slick({
		infinite: true,
		slidesToShow: 6,
		dots: false,
		arrows: true,
		slidesToScroll: 6,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				}
			}
		]
	});
	$('.documents-slider').slick({
		infinite: true,
		slidesToShow: 6,
		dots: false,
		arrows: true,
		slidesToScroll: 6,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			}
		]
	});
	compSliderArrows();
	$('.popular-slider').on('breakpoint', function(event, slick){
		compSliderArrows();
	});
	$('.brands-slider').on('breakpoint', function(event, slick){
		compSliderArrows();
	});
	$('.video-slider').on('breakpoint', function(event, slick){
		compSliderArrows();
	});
	$('.documents-slider').on('breakpoint', function(event, slick){
		compSliderArrows();
	});
	$('.slider-for1').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 3000,
		arrows: false,
		fade: true,
		asNavFor: '.slider-nav1'
	});
	$('.slider-nav1').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.slider-for1',
		dots: false,
		infinite: true,
		arrows: false,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 360,
				settings: {
					slidesToShow: 3,
					arrows: false,
					slidesToScroll: 1
				}
			}
		]
	});
	$("#carousel-main").carousel({
		interval: 7000
	});
	$('[data-toggle="affix"]').each(function() {
		var ele = $(this),
			wrapper = $('<div></div>');

		ele.before(wrapper);
		$(window).on('scroll resize', function() {
			toggleAffix(ele, $(this), wrapper);
		});

		// init
		toggleAffix(ele, $(window), wrapper);
	});



	$(document).on('click', '.btn-number', function(e){
		var type = $(this).attr('data-type');
		var field = $(this).attr('data-target');
		var input = $(field);
		var min = input.attr('min');
		var max = input.attr('max');
		min=parseInt(min);
		max=parseInt(max);
		var currentVal;
		var value = input.val();
		if(type === 'minus')
		{
			if(value>min)
			{
				currentVal = parseInt(value) - 1;
				input.val(currentVal);
				input.change();
			}
		}
		if(type === 'plus')
		{
			if(value<max)
			{
				currentVal = parseInt(value) + 1;
				input.val(currentVal);
				input.change();
			}
		}

	});
	faqForm();
	payBtnShow();
	faqBtn();
	stepsBuy();
	menuCat();
	nameBtn();
	dropBtn();
	leaveComment();
	menuSide();

	var $textSec=$('.text-sec.seo-text');
	$textSec.each(function(index, el){
		var $el=$(el);
		if($el.height()>750)
			$el.addClass("collapsed");
	});
    $textSec.find(".show-more .btn").click(function () {
        $textSec.toggleClass("collapsed");
    });

	$('input[name="phone_number"], input[name="phone"]').mask("+7 (000) 000-00-00", {placeholder: "+7 ( ___ ) ___-__-__"});
	window.Parsley.addValidator('validatePhoneNumber', {
        validateString: function (value, requirement) {
            return value.match(/\+7\s\(\d{3}\)\s\d{3}-\d{2}-\d{2}/) !== null;
        },
        requirementType: 'string',
		messages: {
        	ru: 'Введите полный номер телефона'
		}
    });

	var yaGoals = new YandexGoals(46629345);

	var $saveBirthdayModal=$('#saveBirthdayModal');
	$saveBirthdayModal.find('form').submit(function (ev) {
		ev.preventDefault();
		ev.stopImmediatePropagation();
		var $form=$(this);
		var submitArray=$form.serializeArray();
		var formData={};
		submitArray.forEach(function(value){ formData[value.name]=value.value});
		sendRequest($form.attr('action'), function (success, resp) {
			if(success)
				yaGoals.reachGoal('birthday');
		}, formData, "POST");
		$saveBirthdayModal.modal('hide');
    });

	$saveBirthdayModal.find(".input-number").keypress(function (ev) {
		var theEvent = ev || window.event;
		var key = theEvent.keyCode || theEvent.which;
		key = String.fromCharCode( key );
		var regex = /[0-9]|\./;
		if( !regex.test(key) ) {
		theEvent.returnValue = false;
		if(theEvent.preventDefault) theEvent.preventDefault();
		}
    });

	if(DISPLAY_INFORM_WINDOW){
		var repeatTime = 60 * 60 * 1000;
		var lastShowTime = getCookie("inform_modal_showed");
		var now = new Date().getTime();
		if(lastShowTime)
            lastShowTime = parseFloat(getCookie("inform_modal_showed"));
		if(!lastShowTime || now - lastShowTime > repeatTime ){
			$('#modalInform').modal("show");
			setCookie("inform_modal_showed", now)
		}

	}
});






