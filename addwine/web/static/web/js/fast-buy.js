$(function () {
    var $fastBuyModal=$("#fastbuy");
   $(document).on("click", "button.fastbuy", function () {
       var $this=$(this);
       $fastBuyModal.find("input[name='product']").val($this.attr("data-product-id"));
       $fastBuyModal.find("input[name='pack']").val($this.attr("data-pack"));
       $fastBuyModal.find("input[name='quantity']").val($this.attr("data-quantity"));
       $fastBuyModal.find(".img-block img").attr("src", $this.attr("data-product-img"));
       $fastBuyModal.find(".name").html($this.attr("data-product-title"));
       $fastBuyModal.find(".price .value").html($this.attr("data-product-price"));
   });
});