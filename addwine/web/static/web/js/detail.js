function SliderControls($slider) {
    var $primaryImg=$slider.find('.primary img');
    var $navElements=$slider.find(".nav-el");
    $navElements.click(function () {
        var $this=$(this);
        $navElements.removeClass('active');
        $this.addClass("active");
        $primaryImg.attr("src", $this.attr("data-src-medium"));
        $primaryImg.attr("alt", $this.attr("data-caption"));
        $primaryImg.attr("data-index", $this.attr("data-index"));
    });

    var galleryItems = [];
    $navElements.each(function (index, item) {
            var $item=$(item);
            galleryItems.push({
                src: $item.attr("data-src"),
                w: $item.attr("data-width"),
                h: $item.attr("data-height")
            });
        });

    $primaryImg.click(function () {
        show = document.getElementsByClassName('show')[0];
        show.click()
    });
}


function PriceController($priceField, $priceOriginalField, $countField, initialPrice, initialOriginalPrice, onPriceChange) {
    var _price=initialPrice;
    var _originalPrice=initialOriginalPrice;

    $countField.on("change", function () {
        var price=_price*$countField.val();
        var originalPrice=_originalPrice*$countField.val();
        $priceField.html(price);
        $priceOriginalField.html(originalPrice);
        if(onPriceChange)
            onPriceChange(price, originalPrice)
    });


    this.setPrice=function (newPrice, newOriginalPrice) {
        _price=newPrice;
        _originalPrice=newOriginalPrice;
        $countField.trigger("change");
    };

    this.getPrice=function () {
        return _price;
    };

    this.getOriginalPrice=function () {
        return _originalPrice;
    };
}


function PacksController($packageSection, $sliders, onPackChanged) {
    $packageSection.find(".nav-item").on('shown.bs.tab', function (e) {
          var href = $(e.target).attr("href");
          $sliders.removeClass("active");
          if(href==="#nav-home") {
              $sliders.filter(".original").addClass("active");
              onPackChanged(true, null);
          }else
              $('.package-section input[type="radio"]:checked').trigger("change");
    });


    $packageSection.find('input[type="radio"]').on("change", function () {
         var $this=$(this);
         var $targetSlider=$('#'+$this.attr("data-gallery"));
         $sliders.removeClass("active");
         if($targetSlider.length>0)
             $targetSlider.addClass("active");
         else
             $sliders.filter(".original").addClass("active");

         onPackChanged(false, {
             id: $this.val(),
             price: $this.attr("data-price"),
             priceOriginal: $this.attr("data-price-original")
         });
    });
}

function DoubleSlider($slider){
   var $titles=$slider.find(".titles .title");
   var $sliders=$slider.find(".popular-slider");

   $titles.click(function () {
        var $this=$(this);
        $titles.removeClass("active");
        $this.addClass("active");

        $sliders.removeClass("active");
        var $slider=$sliders.filter(".tab"+$this.attr('data-tab'));
        $slider.addClass("active");
        $slider[0].slick.refresh();
        compSliderArrows();
   });
}

$(function () {

    $(".double-slider").each(function () {
            new DoubleSlider($(this));
    });


    var $sliders=$(".product-section .slider");
    $sliders.each(function (index, slider) {
        new SliderControls($(slider));
    });

    var $fastBuyButton=$("#fastBuyButton");
    var $priceValueField=$(".price-sec .price-value");
    var $priceOriginalField=$(".price__old .price-original");
    // var $priceOriginalField=$(".price-sec .price-original");
    var $countField=$(".count-sec input[name='count']");
    var $basketFormPackField=$(".btns-sec form input[name='pack']");
    var $basketFormQuantityField=$(".btns-sec form input[name='quantity']");
    var initialPrice=parseInt($priceValueField.html());
    var initialOriginalPrice=parseInt($priceOriginalField.html());
    var priceController=new PriceController($priceValueField, $priceOriginalField, $countField, initialPrice, initialOriginalPrice, function (price, originalPrice) {
        $fastBuyButton.attr("data-product-price", price);
    });
    new PacksController($(".package-section"), $sliders, function (simpleMode, packData) {
        if(simpleMode){
            priceController.setPrice(initialPrice, initialOriginalPrice);
            $basketFormPackField.val('');
            $fastBuyButton.attr('data-pack', '');
        }else{
            priceController.setPrice(packData.price, packData.priceOriginal);
            $basketFormPackField.val(packData.id);
            $fastBuyButton.attr("data-pack", packData.id);
        }

    });

    var $loadingOverlay=$(".product-info .loading-overlay");

    $countField.on("change", function () {
        var count=$countField.val();
        $basketFormQuantityField.val(count);
        $fastBuyButton.attr("data-quantity", count);
    });

    var startLoading=function () {
        $loadingOverlay.addClass("active");
    };

    var stopLoading=function () {
        $loadingOverlay.removeClass("active");
    };

    BASKET.addListener("add-form", {
        beforeSubmit: function () {
            startLoading();
        },
        success: function (data) {
            stopLoading();
            $countField.val(1);
            $countField.change();
        },
        error: function () {
            stopLoading();
        }
    });
});
