from django.conf import settings
from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from oscar.core.application import Application

from checkout.views import ShippingView, ContactsView, PaymentDetailView, ProductFastCheckoutView, \
    BasketFastCheckoutView


class CheckoutApplication(Application):
    name = 'checkout'

    def get_urls(self):
        urls = [
            url(r'^$', ShippingView.as_view(), name='index'),
            url(r'^contacts/$', ContactsView.as_view(), name='contacts'),
            url(r'^payment-details/$', PaymentDetailView.as_view(), name='payment-details'),
            url(r'^fast-checkout/$', ProductFastCheckoutView.as_view(), name='fast-checkout'),
            url(r'^basket-fast-checkout/$', BasketFastCheckoutView.as_view(), name='basket-fast-checkout'),
        ]
        return self.post_process_urls(urls)

    def get_url_decorator(self, pattern):
        if not settings.OSCAR_ALLOW_ANON_CHECKOUT:
            return login_required
        if pattern.name.startswith('user-address'):
            return login_required
        return None


application = CheckoutApplication()
