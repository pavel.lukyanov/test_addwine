import json
from decimal import Decimal
from django.core.exceptions import ImproperlyConfigured
from django.forms import model_to_dict
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse_lazy, reverse
from django.views import generic, View
from django.views.decorators.http import require_http_methods
from django.views.generic.edit import BaseFormView

from amocrm_api.utils import amocrm_sync_order
from basket.models import Basket
from catalogue.models import Product, Option, Selector

from checkout.forms import ShippingForm, ContactsForm, FastCheckoutForm, BasketFastCheckoutForm
from checkout.session import CheckoutSessionMixin
from checkout.utils import resolve_coordinates_by_address, create_order, close_basket, on_order_created
from common.views import PageView, PageViewMixin, CatchFormView
from customer.models import Contacts
from order.models import Order, Line, ShippingContacts
from payment.models import PaymentMethod
from shipping.models import OrderAndItemCharges, ShippingTable


class ShippingView(CheckoutSessionMixin, PageViewMixin, generic.FormView):
    page_code = 'checkout_shipping'
    template_name = 'web/checkout/shipping.html'
    form_class = ShippingForm
    success_url = reverse_lazy('checkout:payment-details')
    pre_conditions = ['check_basket_is_not_empty',
                      'check_basket_is_valid',
                      'check_user_contacts_is_capture'
                      ]

    def get_context_data(self, **kwargs):
        context = super(ShippingView, self).get_context_data(**kwargs)
        if self.request.basket.contains_forbidden_for_delivery_products():
            context['shipping_methods'] = OrderAndItemCharges.objects.filter(use_for_pickup=True)
        else:
            context['shipping_methods'] = OrderAndItemCharges.objects.all()
        context['shipping_table'] = json.dumps(ShippingTable.get_instance().as_dict())
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        # Save shipping method
        shipping_method = data['shipping_method']
        self.checkout_session.use_shipping_method(shipping_method.pk)
        del data['shipping_method']

        if data.get('shipping_date'):
            self.checkout_session.set_shipping_date(data['shipping_date'].strftime('%d/%m/%Y'))
            del data['shipping_date']

        if shipping_method.require_address:
            # Create shipping address
            address_fields = dict(
                (k.split('address_')[1], v) for (k, v) in data.items() if k.startswith("address_")
            )

            lat, lng, resolved = resolve_coordinates_by_address(address_fields['onestring'])
            if resolved:
                address_fields['lat'] = lat
                address_fields['lng'] = lng

            self.checkout_session.ship_to_new_address(address_fields)
        else:
            self.checkout_session.ship_to_new_address({})
        return super().form_valid(form)


class ContactsView(CheckoutSessionMixin, BaseFormView):
    page_code = 'checkout_contacts'
    template_name = 'web/checkout/contacts.html'
    form_class = ContactsForm
    success_url = reverse_lazy('checkout:index')
    pre_conditions = ['check_basket_is_not_empty',
                      'check_basket_is_valid',
                      ]

    def validate_vouchers(self, data):
        vouchers_application = self.request.basket.vouchers_applications.all()
        fake_vouchers = []
        contacts = Contacts(**{
            'name': data['name'],
            'email': data.get('email'),
            'phone_number': data['phone_number']
        })
        user = contacts.recognize_user()
        for voucher_application in vouchers_application:
            if not user or voucher_application.user_id != user.id:
                fake_vouchers.append(voucher_application.voucher)
                voucher_application.delete()

        for voucher in fake_vouchers:
            self.request.basket.vouchers.remove(voucher)

    def form_valid(self, form):
        self.validate_vouchers(form.cleaned_data)

        data = form.cleaned_data
        data['phone_number'] = data['phone_number'].as_national
        self.checkout_session.set_contacts_fields(data)
        return super().form_valid(form)

    def form_invalid(self, form):
        return HttpResponseRedirect(reverse('basket:summary'))


class PaymentDetailView(CheckoutSessionMixin, PageView):
    page_code = 'checkout_payment_details'
    template_name = 'oscar/checkout/payment_detail.html'
    pre_conditions = ['check_basket_is_not_empty',
                      'check_basket_is_valid',
                      'check_shipping_data_is_captured',
                      'check_user_contacts_is_capture'
                      ]

    @staticmethod
    def get_success_url():
        return reverse('promotions:home') + "#success=True&form_name=checkout"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['payment_methods'] = PaymentMethod.objects.filter(enabled=True)
        return context

    def post(self, request, *args, **kwargs):
        try:
            method = PaymentMethod.objects.get(pk=request.POST['payment_method'])
        except PaymentMethod.DoesNotExist:
            return HttpResponseRedirect("")
        self.checkout_session.set_payment_method(method.pk)

        submission_dict = self.build_submission()
        self.submit(**submission_dict)
        return HttpResponseRedirect(self.get_success_url())

    def submit(self, basket, shipping_address, shipping_method, shipping_charge, shipping_charge_resolved,
               shipping_date,
               contacts, payment_method, *args, **kwargs):

        basket.add_presents()

        order = create_order(basket, shipping_method, shipping_charge, shipping_charge_resolved, payment_method,
                             shipping_date)

        if shipping_address:
            shipping_address.order = order
            shipping_address.save()
        contacts.order = order
        contacts.save()

        close_basket(basket)
        self.checkout_session.flush()

        on_order_created(self.request, order)
        return order


class ProductFastCheckoutView(CatchFormView):
    form_class = FastCheckoutForm
    form_name = 'fast_checkout'

    def get_success_url(self):
        return PaymentDetailView.get_success_url()

    def get_basket(self, form):
        basket = Basket()
        basket.strategy = self.request.strategy
        product = form.cleaned_data['product']
        quantity = form.cleaned_data.get('quantity')
        if quantity is None:
            quantity = 1
        options = []
        if form.cleaned_data.get('pack'):
            option, created = Option.objects.get_or_create(code=Product.PACK_OPTION_CODE,
                                                           defaults={
                                                               'name': Product.PACK_OPTION_NAME,
                                                               'code': Product.PACK_OPTION_CODE,
                                                               'type': Option.OPTIONAL,
                                                           }
                                                           )
            options.append({
                'option': option,
                'value': form.cleaned_data['pack']
            })
        basket.add_product(product, quantity, options)
        return basket

    def close_basket(self, basket):
        close_basket(basket)
        basket.delete()

    def form_valid(self, form):
        basket = self.get_basket(form)
        shipping_method = OrderAndItemCharges.objects.filter(name='Быстрый заказ').first()
        if shipping_method is None:
            shipping_methods = OrderAndItemCharges.objects.filter(require_address=False).order_by('price_per_order')
            if len(shipping_methods) == 0:
                raise ImproperlyConfigured("You must add at least one shipping method")
            shipping_method = shipping_methods[0]
        shipping_charge, shipping_charge_resolved = shipping_method.calculate(basket)

        payment_method = PaymentMethod.objects.filter(name='Быстрый заказ').first()
        if payment_method is None:
            payment_methods = PaymentMethod.objects.all()
            if len(payment_methods) == 0:
                raise ImproperlyConfigured("You must add at least one payment method")
            payment_method = payment_methods[0]
        order = create_order(basket, shipping_method, shipping_charge, shipping_charge_resolved, payment_method,
                             None)

        phone = form.cleaned_data['phone_number']
        first_name = form.cleaned_data['first_name']
        ShippingContacts.create_fast_checkout_contacts(order, first_name, phone)

        self.close_basket(basket)

        on_order_created(self.request, order)
        return super().form_valid(form)


class BasketFastCheckoutView(ProductFastCheckoutView):
    form_class = BasketFastCheckoutForm

    def get_basket(self, form):
        return self.request.basket

    def close_basket(self, basket):
        close_basket(basket)
