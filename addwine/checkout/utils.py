import json

from decimal import Decimal
from django.conf import settings
from oscar.apps.checkout.utils import CheckoutSessionData as BaseCheckoutSessionData
import requests
import logging
from amocrm_api.utils import amocrm_sync_order
from basket.models import Basket
from order.models import Line
from feedback.utils import is_need_notify_on_mail, get_comm_event_obj, send_mail
from offer.applicator import Applicator
from order.models import Order, ShippingAddress
from partner.strategy import Selector

logger = logging.getLogger(__name__)


class CheckoutSessionData(BaseCheckoutSessionData):
    def set_shipping_date(self, date):
        return self._set('shipping', 'date', date)

    def get_shipping_date(self):
        return self._get('shipping', 'date')

    def get_contacts_fields(self):
        return self._get('shipping', 'contacts_fields')

    def set_contacts_fields(self, value):
        self._set('shipping', 'contacts_fields', value)

    def get_payment_method(self):
        return self._get('shipping', 'payment_method')

    def set_payment_method(self, value):
        self._set('shipping', 'payment_method', value)


def resolve_coordinates_by_address(address):
    try:
        request = requests.post(
            url='https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address',
            data=json.dumps({
                "query": address,
                "count": 1
            }),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Token " + settings.DADATA_TOKEN
            },
            timeout=0.75
        )
    except Exception as e:
        logger.exception('Exception during connection to dadata.ru API, please check availability of the service')
        return 0, 0, False
    if request.status_code != 200:
        return 0, 0, False

    try:
        resp = request.json()
    except ValueError:
        return 0, 0, False

    suggestions = resp.get('suggestions')
    if suggestions is None:
        logger.error("Suggestions api not available now. Please check dadata.ru API")
        return 0, 0, False

    if len(suggestions) == 0:
        return 0, 0, False

    suggestion = suggestions[0]
    data = suggestion['data']
    lat, lng = data.get('geo_lat'), data.get('geo_lon')
    if lat:
        lat = float(lat)
    if lng:
        lng = float(lng)
    resolved = lat is not None and lng is not None
    return lat, lng, resolved



def close_basket(basket):
    basket.flush()
    basket.vouchers.clear()
    basket.vouchers_applications.update(basket=None)


def on_order_created(request, order):
    amocrm_sync_order(order.id)
    request.session['last_checkout_order'] = order.id


def apply_basket_to_order(order, basket):
    from oscar.apps.order.utils import OrderCreator

    if basket.gift_card:
        order.gift_card = basket.gift_card
        order.gift_card.spent = True
        order.gift_card.save()

        basket.gift_card = None
        basket.save()

    order_creator = OrderCreator()
    # Record any discounts associated with this order
    for application in basket.offer_applications:
        # Trigger any deferred benefits from offers and capture the
        # resulting message
        application['message'] \
            = application['offer'].apply_deferred_benefit(basket, order,
                                                          application)
        # Record offer application results
        if application['result'].affects_shipping:
            # Skip zero shipping discounts
            shipping_discount = order.shipping_method.discount(basket)
            if shipping_discount <= Decimal('0.00'):
                continue
            # If a shipping offer, we need to grab the actual discount off
            # the shipping method instance, which should be wrapped in an
            # OfferDiscount instance.
            application['discount'] = shipping_discount
        order_creator.create_discount_model(order, application)
        order_creator.record_discount(application)

    for voucher in basket.vouchers.all():
        order_creator.record_voucher_usage(order, voucher, None)

    for line in basket.all_lines():
        purchase_info = basket.strategy.fetch_for_product(line.product)

        if line.has_range_discount:
            range_discount = purchase_info.price.range_discount
            range_discount.num_orders += 1

            if line.is_tax_known:
                discount = line.line_price_incl_tax - line.line_price_incl_tax_incl_range_discount
            else:
                discount = line.line_price_excl_tax - line.line_price_excl_tax_incl_range_discount
            range_discount.total_discount += discount
            range_discount.save()

        order_line = Line.objects.create(
            order=order,
            stockrecord=line.stockrecord,
            title=line.product.title,
            upc=line.product.upc,
            product=line.product,
            quantity=line.quantity,
            unit_price_incl_tax=line.unit_price_incl_tax,
            unit_price_excl_tax=line.unit_price_excl_tax,
            unit_price_incl_tax_incl_range_discount=line.unit_price_incl_tax_incl_range_discount,
            unit_price_excl_tax_incl_range_discount=line.unit_price_excl_tax_incl_range_discount,
            unit_price_retail=line.unit_price_retail,
            line_price_before_discounts_incl_tax=line.line_price_incl_tax,
            line_price_before_discounts_excl_tax=line.line_price_excl_tax,
            line_price_incl_tax=line.line_price_incl_tax_incl_discounts,
            line_price_excl_tax=line.line_price_excl_tax_incl_discounts,
            line_price_retail=line.line_price_retail,
            selected_pack=line.selected_pack,
            is_present=line.is_present
        )

        for attr in line.attributes.all():
            order_line.attributes.create(
                option=attr.option,
                type=attr.option.code,
                value=attr.value)


def create_order(basket, shipping_method, shipping_charge, shipping_charge_resolved, payment_method,
                 shipping_date=None):
    order = Order.objects.create(
        shipping_method=shipping_method,
        shipping_date=shipping_date,
        payment_method=payment_method,
        total_incl_tax=basket.total_incl_tax,
        total_excl_tax=basket.total_excl_tax,
        total_retail=basket.total_retail,
        shipping_incl_tax=shipping_charge.incl_tax,
        shipping_excl_tax=shipping_charge.excl_tax,
        shipping_charge_resolved=shipping_charge_resolved,
    )

    apply_basket_to_order(order, basket)

    if is_need_notify_on_mail():
        template = get_comm_event_obj(Order.TEMPLATE_CODE)
        if template:
            send_mail(template, {
                'price': round(order.total_excl_tax),
                'shipping_date': order.shipping_date.strftime("%d.%m.%Y") if order.shipping_date else '',
                'shipping_method': order.shipping_method.as_dict() if order.shipping_method else {},
                'payment_method': order.payment_method.as_dict() if order.payment_method else {},
                'date_placed': order.date_placed.strftime("%H:%M  %d.%m.%Y") if order.date_placed else '',
            })

    return order


def update_order(order, deleted_products_pks, changed_dict):
    """Used only in admin for order processing
    """
    if changed_dict['lines'] or changed_dict['discounts']:
        temp_basket = Basket.objects.create()
        temp_basket.strategy = Selector().strategy()

        for line in order.lines.all():
            if line.is_present:
                temp_basket.add_present_product(line.product, line.quantity, line.options)
            else:
                temp_basket.add_product(line.product, line.quantity, line.options)

        # We working only with vouchers so collect them to basket
        for discount in order.discounts.all():
            voucher = discount.voucher
            if voucher:
                temp_basket.vouchers.add(voucher)

        # Detach gift card from order
        gift_card = order.gift_card
        if gift_card:
            order.gift_card.spent = False
            order.gift_card.save()

            order.gift_card = None
            order.save()

        presents_ids = [l.product_id for l in order.lines.filter(is_present=True)]
        exclude_products = order.lines\
            .filter(is_present=False)\
            .exclude(product__present=None)\
            .filter(product__present_id__in=presents_ids + deleted_products_pks)
        excluded_pks = [l.product_id for l in exclude_products] + presents_ids

        # Attach gift card to basket
        temp_basket.gift_card = gift_card
        temp_basket.add_presents(exclude_ids=excluded_pks, clear=False)
        for l in temp_basket.lines.filter(product_id__in=deleted_products_pks):
            l.delete()

        # Delete all discounts, they will rollback in pre_delete signal receiver
        order.discounts.all().delete()

        # Clear lines in order
        order.lines.all().delete()

        # Apply vouchers to basket
        Applicator().apply(temp_basket, order.client)

        # Init order from temp basket
        order.total_incl_tax = temp_basket.total_incl_tax
        order.total_excl_tax = temp_basket.total_excl_tax
        order.total_retail = temp_basket.total_retail
        apply_basket_to_order(order, temp_basket)

        temp_basket.delete()

    try:
        shipping_address = order.shipping_address
    except ShippingAddress.DoesNotExist:
        shipping_address = None

    if changed_dict['address'] and shipping_address:
        lat, lng, resolved = resolve_coordinates_by_address(shipping_address.concat_in_onestring())
        shipping_address.resolved = resolved
        if resolved:
            shipping_address.lat = lat
            shipping_address.lng = lng
        else:
            shipping_address.lat = None
            shipping_address.lng = None

        shipping_address.save()

    if changed_dict['shipping_method'] or changed_dict['address'] or changed_dict['lines'] or  changed_dict['discounts']:
        coord = None
        if shipping_address:
            coord = shipping_address.get_coordinates()
        shipping_charge, shipping_charge_resolved = order.shipping_method.calculate_with_total(
            order.total_excl_tax, order.currency, coord
        )
        order.shipping_charge_resolved = shipping_charge_resolved
        order.shipping_excl_tax = shipping_charge.excl_tax
        order.shipping_incl_tax = shipping_charge.incl_tax

    # override total_excl_tax to support custom price from admin
    order.total_excl_tax = sum(line.line_price_excl_tax for line in order.lines.all())
    order.save()
