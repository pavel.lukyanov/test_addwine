function BasketController(){
    var $contentContainer=$(".buy-section .content");
    var $updatablePart=$(".table-body .updatable");
    var $loadingOverlay=$(".table-body .loading-overlay");

    var $itogContainer=$(".discont-section .itog");
    var $priceField=$itogContainer.find(".price .cena");
    var $priceOldField=$itogContainer.find(".old-price");

    var $tableSection=$(".buy-section .table-section");

    var _attachFormListener=function () {
        $updatablePart.find(".basket-change-form").ajaxForm({
            beforeSubmit: function () {
                _startLoading();
            },
            success: function (resp) {
                _updateContent(resp);
                _stopLoading();
            }
        });
    };

    var _checkIsEmpty=function () {
        if($updatablePart.find(".table-row").length===0)
            $contentContainer.addClass("empty");
        else
            $contentContainer.removeClass("empty");
    };

    var _getForbiddenForDeliveryState=function(){
        var $rows=$updatablePart.find(".table-row");
        var $forbbidenForDeliveryRows=$updatablePart.find(".table-row.forbidden-for-delivery")
        return {
            forbiddenForDelivery: $forbbidenForDeliveryRows.length>0,
            allForbidden: $forbbidenForDeliveryRows.length === $rows.length
        }
    };

    var _updateContent=function (resp) {
        $updatablePart.html(resp.html);
        $priceField.html(resp.total);
        $priceOldField.html(resp.total_excl_gift_card);

        var forbiddenForDeliveryState=_getForbiddenForDeliveryState();
        if(forbiddenForDeliveryState.forbiddenForDelivery) {
            $tableSection.addClass("forbidden-for-delivery");
            if(forbiddenForDeliveryState.allForbidden)
                $tableSection.addClass("all");
            else
                $tableSection.removeClass("all");
        }else{
            $tableSection.removeClass("forbidden-for-delivery");
            $tableSection.removeClass("all");
        }

        if(resp.has_gift_card)
            $itogContainer.addClass("gift-card");
        _checkIsEmpty();
       _attachFormListener();
    };

    var _startLoading=function () {
        $loadingOverlay.addClass("active");
    };

    var _stopLoading=function () {
        setTimeout(function () {
            $loadingOverlay.removeClass("active");
        }, 500);

    };

    $updatablePart.on('change', '.update-form input[name="quantity"]', function () {
        $(this).closest(".update-form").submit();
    });
    _attachFormListener();

    this.startLoading=_startLoading;
    this.stopLoading=_stopLoading;
    this.updateContent=_updateContent;
}

function VoucherController($voucherContainer, $contactsContainer, basket){
    var $voucherForm=$voucherContainer.find("form");
    var $voucherFormSubmit=$voucherForm.find("button[type='submit']");
    var $voucherCodeField=$voucherForm.find("input[name='code']");
    var $voucherTextField=$voucherContainer.find(".text");
    var defaultText=$voucherTextField.html();

    var _resetState=function () {
        $voucherForm.removeClass('success');
        $voucherForm.removeClass('info');
        $voucherForm.removeClass('error');
    };

    var _resetForm=function () {
        $voucherFormSubmit.prop("disabled", true);
        $voucherForm[0].reset();
        $voucherTextField.html(defaultText);
        _resetState();
    };

    var doubleTry = true;

    $voucherForm.ajaxForm(
        {
            beforeSubmit: function () {
                basket.startLoading();
            },
            success: function (data) {
                 basket.updateContent(data);
                 _resetForm();
                 basket.stopLoading();
                 doubleTry = true;
            },
            error: function (data) {
                if (doubleTry) {
                    doubleTry = false;
                    $voucherForm.submit();
                }
                basket.stopLoading();
            }
        }
    );

    var getContacts=function () {
        return {
            email: $contactsContainer.find('input[name="email"]').val(),
            phone: $contactsContainer.find('input[name="phone_number"]').val()
        }
    };

    var fetchVoucherUrl=$voucherForm.attr('data-fetch-url');
    function formatDate(dateStr){
        var date = new Date(Date.parse(dateStr));

        var day = ('0'+(date.getDate())).slice(-2);
        var month = ('0'+(date.getMonth()+1)).slice(-2);
        var year = ('0'+(date.getYear())).slice(-2);
        var hours = ('0'+(date.getHours())).slice(-2);
        var minutes = ('0'+(date.getMinutes())).slice(-2);
        return day+"."+month+"."+year+" "+hours+":"+minutes;
    }

    var fetchVoucher=function (code) {
        if(code.trim().length===0) {
            _resetForm();
            return;
        }

        var contacts=getContacts();
        if(!contacts.email && !contacts.phone){
            $voucherForm.addClass("error");
            $voucherForm.find(".error").html('Прежде чем активировать скидку, укажите свой email или номер телефона');
            return;
        }

        var requestUrl = fetchVoucherUrl+"?";
        if(contacts.email)
            requestUrl+="email="+contacts.email+"&";
        if(contacts.phone)
            requestUrl+="phone="+contacts.phone;

        $voucherForm.find('input[name="email"]').val(contacts.email);
        $voucherForm.find('input[name="phone"]').val(contacts.phone);

        sendRequest(requestUrl, function (success, resp) {
            if(!success)
                return;
            var text;
            $voucherFormSubmit.prop("disabled", true);
            var state;
            if(resp.fetched){
                if(!resp.enabled) {
                    text = resp.is_gift_card ? "Ваша карта не активна в текущий момент" : "К сожалению данный купон недействителен";
                    state = 'disabled';
                }else if(resp.expired || !resp.active) {
                    if(resp.is_gift_card)
                        text = "Ваша карта уже использована" ;
                    else
                        text = "К сожалению акция недействительна.<br> Дата проведения акции "+formatDate(resp.start_datetime)+ " - "+formatDate(resp.end_datetime)
                    state = 'info';
                }else if(resp.already_applied){
                    text=resp.is_gift_card?'Вы уже использовали данную карту':"Вы уже применили этот купон";
                    state = 'info';
                }else if(resp.max_applications_exceeded) {
                    text='Превышено максимальное количество применений';
                    state = 'error';
                }else if(resp.no_credentials) {
                    text='Прежде чем активировать скидку, укажите свой email или номер телефона';
                    state = 'error';
                }
                else{
                    text = resp.description;
                    state = 'success';
                    $voucherFormSubmit.prop("disabled", false);
                }
            }else {
                state = 'error';
                text = 'Неверный код купона';
            }

            _resetState();
            $voucherForm.addClass(state);

            switch(state){
                case 'error':
                    $voucherForm.find(".error").html(text);
                break;
                case 'success':
                    $voucherForm.find(".success").html(text);
                break;
                case 'info':
                    $voucherForm.find(".info").html(text);
                break;
            }
        }, {'code': code}, "POST");
    };

    var lastTimeoutId;
    var _updateVaucherCode=function () {
        clearTimeout(lastTimeoutId);
        lastTimeoutId=setTimeout(function () {
            fetchVoucher($voucherCodeField.val());
        }, 500);
    };

    $voucherCodeField.on("input", _updateVaucherCode);

    this.updateVaucherCode=_updateVaucherCode;
}

function ContactsController($contactsForm, voucher) {
    $contactsForm.find("input").on("change", function () {
        voucher.updateVaucherCode();
    })
}

$(function(){
    var $contactsForm = $('#contactsForm');

    var basket=new BasketController();
    var voucher = new VoucherController($(".discont-section .disc"), $contactsForm, basket);
    var contacts = new ContactsController($contactsForm, voucher);

    $('#submitNextButton').click(function () {
        $contactsForm.submit();
    });
});
