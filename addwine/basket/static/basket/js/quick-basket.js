function QuickBasket($basket){

    var $updatablePart=$basket.find(".updatable");
    var $loadingOverlay=$basket.find(".loading-overlay");
    var $count=$basket.find(".bag-snippet .count");
    var isEmpty=false;
    var listeners={};

    $basket.find(".bag-snippet").click(function (ev) {
        if($(window).width()<768 && !isEmpty)
            return;

        ev.preventDefault();
        ev.stopImmediatePropagation();
        if(!isEmpty)
            $basket.toggleClass("active");
    });

    var _checkBasketOnEmpty=function () {
        isEmpty=$updatablePart.find(".product-row").length===0;
        if(isEmpty)
            $basket.removeClass("active");
    };

    var _getListeners=function (name) {
        return listeners[name];
    };

    var _addListener=function (name, listener) {
        var _listeners = listeners[name];
        if(!_listeners) {
            _listeners = [];
            listeners[name] = _listeners;
        }
        _listeners.push(listener);
    };

    var _removeListeners=function (name) {
        delete listeners[name];
    };

    var _attachFormListener=function () {
        $(".basket-change-form").each(function (index, form) {
            var $form=$(form);
            $form.ajaxForm({
                beforeSubmit: function () {
                    var listeners=_getListeners($form.attr("data-name"));
                    if(listeners){
                        listeners.forEach(function (listener) {
                            listener.beforeSubmit();
                        })
                    }
                    _startLoading();
                },
                success: function (data) {
                    var listeners=_getListeners($form.attr("data-name"));
                    if(listeners){
                        listeners.forEach(function (listener) {
                            listener.success(data);
                        })
                    }

                     _updateContent(data.html);
                    _stopLoading();
                },
                error: function (data) {
                    var listeners=_getListeners($form.attr("data-name"));
                    if(listeners){
                        listeners.forEach(function (listener) {
                            listener.error(data);
                        })
                    }
                    _stopLoading();
                }
            });
        });
    };

    var _updateContent=function (resp) {
        $updatablePart.find('.simplebar-content').html(resp);
        $count.html($updatablePart.find(".product-row").length);
        _checkBasketOnEmpty();
       _attachFormListener();
    };

    var _startLoading=function () {
        $loadingOverlay.addClass("active");
    };

    var _stopLoading=function () {
        setTimeout(function () {
            $loadingOverlay.removeClass("active");
        }, 500);
    };

    this.addListener=_addListener;
    this.getListeners=_getListeners;
    this.removeListener=_removeListeners;
    this.updateFormsListener=_attachFormListener;

    $basket.on('change', '.update-form input[name="quantity"]', function () {
        $(this).closest(".update-form").submit();
    });
    _attachFormListener();
    _checkBasketOnEmpty();
}

BASKET = new QuickBasket($(".bag-section"));