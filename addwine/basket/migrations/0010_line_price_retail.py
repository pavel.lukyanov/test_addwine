# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-04 11:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('basket', '0009_auto_20171008_1728'),
    ]

    operations = [
        migrations.AddField(
            model_name='line',
            name='price_retail',
            field=models.DecimalField(decimal_places=2, max_digits=12, null=True, verbose_name='Себестоимость'),
        ),
    ]
