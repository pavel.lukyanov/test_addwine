import json

from django.http import JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.template.loader import render_to_string
from django.views import View
from math import ceil

from extra_views import ModelFormSetView
from oscar.apps.basket import views as basket_views
from oscar.apps.basket.forms import BasketLineForm
from oscar.apps.basket.formsets import SavedLineFormSet, BasketLineFormSet
from oscar.apps.checkout.calculators import OrderTotalCalculator
from oscar.core import ajax
from oscar.core.compat import user_is_authenticated
from oscar.core.loading import get_model
from oscar.core.utils import safe_referrer

from checkout.session import CheckoutSessionMixin
from .forms import BasketVoucherForm

from basket.models import Line, Basket

from checkout.forms import BasketFastCheckoutForm
from checkout.utils import CheckoutSessionData
from common.views import PageViewMixin
from customer.models import Contacts
from offer.applicator import Applicator
from shipping.repository import Repository
from voucher.models import GiftCard, VoucherUserApplication


class BasketView(CheckoutSessionMixin, PageViewMixin, ModelFormSetView):
    page_script = 'basket/js/basket.js'
    page_code = 'checkout_basket'

    model = get_model('basket', 'Line')
    basket_model = get_model('basket', 'Basket')
    formset_class = BasketLineFormSet
    form_class = BasketLineForm
    extra = 0
    can_delete = True
    template_name = 'basket/basket.html'

    def get_formset_kwargs(self):
        kwargs = super(BasketView, self).get_formset_kwargs()
        kwargs['strategy'] = self.request.strategy
        return kwargs

    def get_queryset(self):
        return self.request.basket.all_lines()

    def get_shipping_methods(self, basket):
        return Repository().get_shipping_methods(
            basket=self.request.basket, user=self.request.user,
            request=self.request)

    def get_default_shipping_method(self, basket):
        return Repository().get_default_shipping_method(
            basket=self.request.basket, user=self.request.user,
            request=self.request)

    def get_basket_warnings(self, basket):
        """
        Return a list of warnings that apply to this basket
        """
        warnings = []
        for line in basket.all_lines():
            warning = line.get_warning()
            if warning:
                warnings.append(warning)
        return warnings

    def get_upsell_messages(self, basket):
        offers = Applicator().get_offers(basket, self.request.user,
                                         self.request)
        applied_offers = list(basket.offer_applications.offers.values())
        msgs = []
        for offer in offers:
            if offer.is_condition_partially_satisfied(basket) \
                    and offer not in applied_offers:
                data = {
                    'message': offer.get_upsell_message(basket),
                    'offer': offer}
                msgs.append(data)
        return msgs

    def get_basket_voucher_form(self):
        """
        This is a separate method so that it's easy to e.g. not return a form
        if there are no vouchers available.
        """
        return BasketVoucherForm()

    def get_context_data(self, **kwargs):
        context = super(BasketView, self).get_context_data(**kwargs)
        context['voucher_form'] = self.get_basket_voucher_form()

        # Shipping information is included to give an idea of the total order
        # cost.  It is also important for PayPal Express where the customer
        # gets redirected away from the basket page and needs to see what the
        # estimated order total is beforehand.
        context['shipping_methods'] = self.get_shipping_methods(
            self.request.basket)
        method = self.get_default_shipping_method(self.request.basket)
        context['shipping_method'] = method
        shipping_charge, resolved = method.calculate(self.request.basket)
        context['shipping_charge'] = shipping_charge
        if method.is_discounted:
            excl_discount = method.calculate_excl_discount(self.request.basket)
            context['shipping_charge_excl_discount'] = excl_discount

        context['order_total'] = OrderTotalCalculator().calculate(
            self.request.basket, shipping_charge)
        context['basket_warnings'] = self.get_basket_warnings(
            self.request.basket)
        context['upsell_messages'] = self.get_upsell_messages(
            self.request.basket)

        context['user_contacts'] = self.get_contacts(self.request.basket)

        if user_is_authenticated(self.request.user):
            try:
                saved_basket = self.basket_model.saved.get(
                    owner=self.request.user)
            except self.basket_model.DoesNotExist:
                pass
            else:
                saved_basket.strategy = self.request.basket.strategy
                if not saved_basket.is_empty:
                    saved_queryset = saved_basket.all_lines()
                    formset = SavedLineFormSet(strategy=self.request.strategy,
                                               basket=self.request.basket,
                                               queryset=saved_queryset,
                                               prefix='saved')
                    context['saved_formset'] = formset

        context['form_basket_fast_checkout'] = BasketFastCheckoutForm()
        return context

    def get_success_url(self):
        return safe_referrer(self.request, 'basket:summary')

    def formset_valid(self, formset):
        # Store offers before any changes are made so we can inform the user of
        # any changes
        offers_before = self.request.basket.applied_offers()
        save_for_later = False

        # Keep a list of messages - we don't immediately call
        # django.contrib.messages as we may be returning an AJAX response in
        # which case we pass the messages back in a JSON payload.
        flash_messages = ajax.FlashMessages()

        for form in formset:
            if (hasattr(form, 'cleaned_data') and
                    form.cleaned_data['save_for_later']):
                line = form.instance
                if user_is_authenticated(self.request.user):
                    self.move_line_to_saved_basket(line)

                    msg = render_to_string(
                        'basket/messages/line_saved.html',
                        {'line': line})
                    flash_messages.info(msg)

                    save_for_later = True
                else:
                    msg = _("You can't save an item for later if you're "
                            "not logged in!")
                    flash_messages.error(msg)
                    return redirect(self.get_success_url())

        if save_for_later:
            # No need to call super if we're moving lines to the saved basket
            response = redirect(self.get_success_url())
        else:
            # Save changes to basket as per normal
            response = super(BasketView, self).formset_valid(formset)

        # If AJAX submission, don't redirect but reload the basket content HTML
        if self.request.is_ajax():
            # Reload basket and apply offers again
            self.request.basket = get_model('basket', 'Basket').objects.get(
                id=self.request.basket.id)
            self.request.basket.strategy = self.request.strategy
            Applicator().apply(self.request.basket, self.request.user,
                               self.request)
            offers_after = self.request.basket.applied_offers()

            # Reload formset - we have to remove the POST fields from the
            # kwargs as, if they are left in, the formset won't construct
            # correctly as there will be a state mismatch between the
            # management form and the database.
            kwargs = self.get_formset_kwargs()
            del kwargs['data']
            del kwargs['files']
            if 'queryset' in kwargs:
                del kwargs['queryset']
            formset = self.get_formset()(queryset=self.get_queryset(),
                                         **kwargs)
            ctx = self.get_context_data(formset=formset,
                                        basket=self.request.basket)
            return self.json_response(ctx, flash_messages)

        return response

    def json_response(self, ctx, flash_messages):
        basket_html = render_to_string(
            'basket/partials/basket_content.html',
            context=ctx, request=self.request)

        payload = {
            'content_html': basket_html,
            'messages': flash_messages.as_dict()}
        return HttpResponse(json.dumps(payload),
                            content_type="application/json")

    def move_line_to_saved_basket(self, line):
        saved_basket, _ = get_model('basket', 'basket').saved.get_or_create(
            owner=self.request.user)
        saved_basket.merge_line(line)

    def formset_invalid(self, formset):
        flash_messages = ajax.FlashMessages()
        flash_messages.warning(_(
            "Your basket couldn't be updated. "
            "Please correct any validation errors below."))

        if self.request.is_ajax():
            ctx = self.get_context_data(formset=formset,
                                        basket=self.request.basket)
            return self.json_response(ctx, flash_messages)

        flash_messages.apply_to_request(self.request)
        return super(BasketView, self).formset_invalid(formset)


class BasketAjaxUpdateViewMixin(object):
    def post(self, request, *args, **kwargs):
        super_obj = super(BasketAjaxUpdateViewMixin, self)
        if hasattr(super_obj, 'post'):
            super_obj.post(request, *args, **kwargs)
        context = {
            'total': ceil(
                request.basket.total_incl_tax if request.basket.is_tax_known else request.basket.total_excl_tax),
            'total_excl_gift_card': ceil(
                request.basket.total_incl_tax_excl_gift_card if request.basket.is_tax_known else request.basket.total_excl_tax_excl_gift_card),
            'has_gift_card': request.basket.has_gift_card
        }
        if request.GET.get('render_quick_basket'):
            context['html'] = render_to_string('web/basket/ajax/basket_quick_content.html', context, request)
        if request.GET.get('render_basket'):
            context['html'] = render_to_string('basket/partials/basket_content.html', context, request)
        return JsonResponse(context)


class BasketAddView(BasketAjaxUpdateViewMixin, basket_views.BasketAddView):
    def form_valid(self, form):
        lines_num = self.request.basket.num_lines
        self.request.basket.add_product(
            form.product, form.cleaned_data['quantity'], form.cleaned_options())
        if lines_num != self.request.basket.num_lines:
            CheckoutSessionData(self.request).flush()

        # Send signal for basket addition
        self.add_signal.send(
            sender=self, product=form.product, user=self.request.user,
            request=self.request)

        return super(basket_views.BasketAddView, self).form_valid(form)


class VoucherAddView(BasketAjaxUpdateViewMixin, basket_views.VoucherAddView):
    form_class = BasketVoucherForm

    def apply_voucher_to_basket(self, voucher, **kwargs):
        if voucher.is_expired() or not voucher.is_active() or not voucher.is_enabled():
            return

        email = kwargs.get('email')
        phone = kwargs.get('phone')
        user = None
        contacts = None
        if email or phone:
            contacts = Contacts(email=email, phone_number=phone)
            user = contacts.recognize_user()
            if user and user.voucher_applications.filter(voucher=voucher).count() > voucher.max_applications_per_user:
                return

        self.request.basket.vouchers.add(voucher)

        # Raise signal
        self.add_signal.send(
            sender=self, basket=self.request.basket, voucher=voucher)

        # Recalculate discounts to see if the voucher gives any
        Applicator().apply(self.request.basket, self.request.user,
                           self.request)
        discounts_after = self.request.basket.offer_applications

        # Look for discounts from this new voucher
        for test_voucher in self.request.basket.vouchers.all():
            found_discount = False
            for discount in discounts_after:
                if discount['voucher'] and discount['voucher'] == test_voucher:
                    found_discount = True
                    break
            if not found_discount:
                self.request.basket.vouchers.remove(test_voucher)
        if voucher in self.request.basket.vouchers.all():
            voucher.num_basket_additions += 1
            voucher.save()

            if not contacts:
                return

            if not user:
                contacts.save()

                user = contacts.recognize_user()

            VoucherUserApplication.objects.create(
                basket=self.request.basket,
                user=user,
                voucher=voucher
            )

    def apply_gift_card(self, gift_card):
        if not gift_card.is_available():
            return

        gift_basket = None
        try:
            gift_basket = gift_card.basket
        except Basket.DoesNotExist:
            pass
        if gift_basket == self.request.basket:
            return

        self.request.basket.gift_card = gift_card
        self.request.basket.save()

    def form_valid(self, form):
        code = form.cleaned_data['code']
        if not self.request.basket.id:
            return

        email = form.cleaned_data.get('email')
        phone = form.cleaned_data.get('phone')

        gift_card = None
        try:
            gift_card = GiftCard.objects.get(code=code)
        except GiftCard.DoesNotExist:
            pass

        if gift_card:
            self.apply_gift_card(gift_card)
            return

        if not self.request.basket.contains_voucher(code):
            try:
                voucher = self.voucher_model._default_manager.get(code=code)
            except self.voucher_model.DoesNotExist:
                pass
            else:
                self.apply_voucher_to_basket(voucher, email=email, phone=phone)

    def form_invalid(self, form):
        pass


class LineOperationView(BasketAjaxUpdateViewMixin, View):
    def get_line(self, line_pk):
        return get_object_or_404(Line, pk=line_pk)

    def reset_session(self):
        CheckoutSessionData(self.request).flush()


class LineDeleteView(LineOperationView):
    def post(self, request, *args, **kwargs):
        self.get_line(kwargs['pk']).delete()
        self.reset_session()
        return super(LineDeleteView, self).post(request, *args, **kwargs)


class LineUpdateView(LineOperationView):
    def post(self, request, *args, **kwargs):
        line = self.get_line(kwargs['pk'])
        quantity = request.POST.get('quantity')
        if quantity is not None:
            try:
                quantity = int(quantity)
            except ValueError:
                quantity = None
        if quantity:
            line.quantity = request.POST['quantity']
            if quantity > 0:
                line.save()
            else:
                line.delete()
            self.reset_session()
        return super(LineUpdateView, self).post(request, *args, **kwargs)
