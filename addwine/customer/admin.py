from ckeditor.widgets import CKEditorWidget
from django import forms
from django.contrib import messages
from django.contrib.auth.models import Group
from django.http import HttpResponseRedirect
from django.template import Template, TemplateSyntaxError
from django.urls import reverse
from django.utils import six
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from oscar.apps.customer.admin import *
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from amocrm_api.utils import amocrm_sync_user
from common.utils import get_admin_settings
from customer.models import User, CommunicationEventTypeTestVariable, Email, Phone, ProductAlert
from feedback.utils import send_mail
from permissions.admin import PermissionAdminMixin
from unisender_api.utils import unisender_sync_user


class CommunicationEventTypeTestVariableInlineAdmin(admin.TabularInline):
    model = CommunicationEventTypeTestVariable
    readonly_fields = ('code', 'description')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class CommunicationEventTypeForm(forms.ModelForm):
    email_subject_template = forms.CharField(
        label=_("Email subject template"))
    email_body_template = forms.CharField(
        label=_("Email body text template"), required=True,
        widget=CKEditorWidget())

    def validate_template(self, value):
        try:
            Template(value)
        except TemplateSyntaxError as e:
            raise forms.ValidationError(six.text_type(e))

    def clean_email_subject_template(self):
        subject = self.cleaned_data['email_subject_template']
        self.validate_template(subject)
        return subject

    def clean_email_body_template(self):
        body = self.cleaned_data['email_body_template']
        self.validate_template(body)
        return body

    class Meta:
        model = CommunicationEventType
        fields = [
            'name', 'email_subject_template', 'email_body_template',
        ]


admin.site.unregister(CommunicationEventType)


@admin.register(CommunicationEventType)
class CommunicationEventTypeAdmin(admin.ModelAdmin):
    form = CommunicationEventTypeForm
    inlines = [CommunicationEventTypeTestVariableInlineAdmin, ]

    def _process_test_variable(self, variable, context):
        codes = variable.code.split('.')
        parent_dict = {}
        current_dict = context
        last_code = codes.pop()
        for code in codes:
            if not current_dict.get(code):
                current_dict[code] = {}
            current_dict = current_dict[code]

        current_dict[last_code] = variable.value

    def _generate_preview_context(self, obj):
        context = {}
        for test_variable in obj.test_variables.all():
            self._process_test_variable(test_variable, context)
        return context

    def _render_preview(self, obj, context):
        html = obj.get_messages(context)
        return html

    def is_make_preview_request(self, request):
        return '_make_preview' in request.POST

    def is_send_test_mail(self, request):
        return '_send_test_mail' in request.POST

    def _display_require_email_error_message(self, request):
        message = format_html(
            'Письмо не было отправлено. Вы должны указать Вашу почту в <a href="{}" target="_blank">настройках '
            'сайта</a>', reverse('admin:common_contacts_change'))
        messages.error(request, message)

    def save_model(self, request, obj, form, change):
        if self.is_send_test_mail(request):
            admin_settings = get_admin_settings()
            if admin_settings.email:
                send_mail(obj, self._generate_preview_context(obj))
            else:
                self._display_require_email_error_message(request)
        super().save_model(request, obj, form, change)

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        if request.GET.get('preview') and object_id:
            obj = CommunicationEventType.objects.get(id=object_id)
            preview = self._render_preview(obj, self._generate_preview_context(obj))
            extra_context = {
                'subject': preview['subject'],
                'html_preview': preview['body']
            }
        return super().changeform_view(request, object_id, form_url, extra_context)

    def response_change(self, request, obj):
        if self.is_make_preview_request(request) or self.is_send_test_mail(request):
            postfix = ''
            if self.is_make_preview_request(request):
                postfix = "?preview=True"
            return HttpResponseRedirect(reverse(
                'admin:customer_communicationeventtype_change', args=(obj.pk,)) + postfix)

        return super().response_change(request, obj)

    class Media:
        css = {
            'all': ('admin/customer/css/change_form.css',)
        }


class EmailAdmin(admin.TabularInline):
    model = Email


class PhoneAdmin(admin.TabularInline):
    model = Phone


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {'fields': ('username',  'password',)}),
        ('AmoCRM', {'fields': ('amocrm_id', )}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'phone_number', 'email', 'birthday',)}),
        ('Telegram', {'fields': ('telegram_username', 'telegram_chat_id')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'phone_number', 'password1', 'password2', 'groups'),
        }),
    )
    list_display = ('username', 'first_name', 'last_name', 'birthday', 'amocrm_id',  'is_staff')
    list_filter = ('groups', 'is_staff', 'is_superuser', 'is_active')
    search_fields = ('first_name', 'last_name', 'email')
    ordering = ('email',)
    readonly_fields = ('amocrm_id',)
    inlines = BaseUserAdmin.inlines + [EmailAdmin, PhoneAdmin]

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        amocrm_sync_user(obj.pk)
        unisender_sync_user(obj.pk)


admin.site.unregister(Group)


@admin.register(Group)
class GroupAdmin(PermissionAdminMixin, admin.ModelAdmin):
    filter_horizontal = ('permissions',)
    fields = ('name', 'permissions')


@admin.register(ProductAlert)
class ProductAlertAdmin(admin.ModelAdmin):
    fields = ('phone_number', 'email', 'first_name', 'last_name', 'product', 'count', 'date_created', 'amocrm_id')
    list_display = ('product', 'email', 'phone_number', 'first_name', 'last_name', 'amocrm_id')
    search_fields = ('product__name', 'email', 'phone_number', 'first_name', 'last_name')
    readonly_fields = ('product','count', 'email', 'phone_number', 'first_name', 'last_name', 'date_created', 'amocrm_id')
