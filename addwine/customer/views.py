import datetime
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from amocrm_api.utils import amocrm_sync_user, amocrm_sync_product_alert, amocrm_sync_birthday
from common.views import CatchFormView
from customer.forms import ProductAlertForm, BirthdaySaveForm
from unisender_api.utils import unisender_sync_user


class ProductAlertAddView(CatchFormView):
    form_class = ProductAlertForm
    form_name = 'product_alert'

    def render_to_response(self, context_data):
        return HttpResponseRedirect(self.get_result_url(True))

    def form_valid(self, form):
        form.instance.name = form.cleaned_data['name']
        form.instance.save()
        amocrm_sync_product_alert(form.instance.id)
        return super().form_valid(form)


class BirthdaySaveView(View):
    def post(self, request, *args, **kwargs):
        form = BirthdaySaveForm(data=request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = data['user']
            birthday = datetime.datetime(year=data['year'], month=data['month'], day=data['day'])
            if not user.birthday:
                user.birthday = birthday
                user.save()

                unisender_sync_user(user.id)
                amocrm_sync_user(user.id)
                # sync BD amoCRM
                amocrm_sync_birthday(user.id)

                success = True
            else:
                success = False
        else:
            success = False

        http_status = 200
        if not success:
            http_status = 400
        return JsonResponse({}, status=http_status)

