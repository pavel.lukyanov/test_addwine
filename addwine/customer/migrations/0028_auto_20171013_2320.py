# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-13 20:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0027_auto_20171012_1941'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='telegram_username',
            field=models.CharField(blank=True, max_length=255, null=True, unique=True, verbose_name='Имя пользователя'),
        ),
    ]
