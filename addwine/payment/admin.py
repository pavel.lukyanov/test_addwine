from django.contrib import admin

from payment.models import PaymentMethod


@admin.register(PaymentMethod)
class PaymentMethodAdmin(admin.ModelAdmin):
    fields = ('name', 'logo', 'enabled')
    list_display = ('name', 'enabled')
    list_editable = ('enabled', )
