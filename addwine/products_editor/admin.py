from django.contrib import admin

from products_editor.models import ProductsEditorProduct


@admin.register(ProductsEditorProduct)
class ProductsEditorProductAdmin(admin.ModelAdmin):
    fields = ('description',)
    list_display = (
        'get_title', 'upc', 'get_product_class', 'brand', 'partner', 'date_created', 'forbidden_for_delivery',
        'is_discountable', 'is_available',
        'is_active', 'is_exported_in_ym')
    list_filter = (
        'brand', 'partner', 'categories', 'forbidden_for_delivery', 'is_discountable', 'is_available', 'is_active',
        'is_exported_in_ym')
    search_fields = ('title', 'upc')

    def has_change_permission(self, request, obj=None):
        return True
