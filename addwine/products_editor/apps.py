from django.apps import AppConfig


class ProductsEditorConfig(AppConfig):
    name = 'products_editor'
