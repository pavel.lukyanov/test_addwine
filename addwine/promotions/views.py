from django.shortcuts import render

from oscar.apps.promotions.views import HomeView as CoreHomeView

from catalogue.models import Brand
from catalogue.views import PopularProductsMixin
from common.views import PageViewMixin
from feedback.forms import SliderCTARequestForm
from marketing.models import SliderCTA
from marketing.views import SommelierCardsMixin


class HomeView(SommelierCardsMixin, PopularProductsMixin, PageViewMixin, CoreHomeView):
    page_code = 'index'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['brands'] = Brand.objects.all()
        context['slides'] = SliderCTA.objects.filter(enabled=True)
        context['form_product_test_drive'] = SliderCTARequestForm()
        return context
