import mock
from model_mommy import mommy
from model_mommy.recipe import Recipe, foreign_key

from .models import Order, Line, ShippingContacts
from catalogue.mommy_recipes import rcp_product
from customer.models import User
from faker import Factory
from faker.providers import internet, phone_number, person

fake = Factory.create('ru_RU')
fake.add_provider(internet)
fake.add_provider(phone_number)
fake.add_provider(person)

rcp_user = Recipe(
    User,
    email=fake.free_email,
    phone_number=fake.phone_number,
    first_name=fake.first_name,
    last_name=fake.last_name
)

rcp_shipping_contact = Recipe(
    ShippingContacts,
    email=fake.free_email
)

rcp_order = Recipe(
    Order,
    client=foreign_key(rcp_user),
    shipping_contacts=foreign_key(rcp_shipping_contact)
)

rcp_line = Recipe(
    Line,
    order=foreign_key(rcp_order),
    product=foreign_key(rcp_product)
)

mommy.generators.add('oscar.models.fields.slugfield.SlugField', lambda: 'slug')
mommy.generators.add('ckeditor_uploader.fields.RichTextUploadingField', lambda: '<html></html>')


def create_line(order, rec_count=10):
    line = mommy.make_recipe('order.rcp_line', order=order)
    mommy.make_recipe('partner.rcp_stock_record', product=line.product)
    for i in range(rec_count):
        r = mommy.make_recipe('catalogue.rcp_product_recommendation', primary=line.product)
        mommy.make_recipe('partner.rcp_stock_record', product=r.recommendation)
    return line


def create_order(user=None, shipping_contacts=None, lines_count=0):
    user = user or mommy.make_recipe('order.rcp_user')

    func = mock.MagicMock()
    with mock.patch("customer.models.Contacts.after_save", func):
        order = mommy.make_recipe('order.rcp_order', client=user)

        shipping_contacts = order.shipping_contacts
        shipping_contacts.order_id = order.id
        shipping_contacts.save()
        user.orders.add(order)

    for _ in range(lines_count):
        create_line(order)

    return order
