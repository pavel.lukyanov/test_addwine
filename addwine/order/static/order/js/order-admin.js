$(document).ready(function () {
  var $select = $('#id_status');
  var $submitRow = $('.submit-row');

  function addBackButton() {
    $submitRow.append('<a href="/admin/order/order/" class="button back-button" style="line-height: 15px; display: inline-block;padding: 10px 15px;">Назад</a>')
  }

  function showSaveButtons() {
    $submitRow.find('[type="submit"]').show();
    $submitRow.find('.back-button').hide();
  }

  function hideSaveButtons() {
    $submitRow.find('[type="submit"]').hide();
    $submitRow.find('.back-button').show();
  }

  var statusesForSave = ['-1', '0', '6', '7'];

  function setState() {
    console.log($select.val(), statusesForSave.indexOf($select.val()), statusesForSave)
    if (statusesForSave.indexOf($select.val()) !== -1) {
      showSaveButtons()
    } else {
      hideSaveButtons()
    }
  }
  
  $select.change(setState);

  addBackButton();
  setState();
});
