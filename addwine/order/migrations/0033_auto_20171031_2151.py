# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-31 18:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0032_auto_20171016_0224'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shippingaddress',
            name='flat',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='Квартира'),
        ),
    ]
