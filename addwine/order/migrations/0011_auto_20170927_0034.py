# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-09-26 21:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0010_auto_20170927_0014'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shippingaddress',
            name='first_name',
        ),
        migrations.RemoveField(
            model_name='shippingaddress',
            name='last_name',
        ),
        migrations.AddField(
            model_name='shippingaddress',
            name='name',
            field=models.CharField(default='', max_length=255, verbose_name='Фамилия Имя Отчество'),
            preserve_default=False,
        ),
    ]
