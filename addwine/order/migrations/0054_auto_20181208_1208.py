# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-12-08 09:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0053_line_is_present'),
    ]

    operations = [
        migrations.AddField(
            model_name='shippingaddress',
            name='amocrm_notes_id',
            field=models.BigIntegerField(default=-1, verbose_name='AmoCRM notes id'),
        ),
        migrations.AddField(
            model_name='shippingcontacts',
            name='amocrm_notes_id',
            field=models.BigIntegerField(default=-1, verbose_name='AmoCRM notes id'),
        ),
        migrations.AlterField(
            model_name='line',
            name='line_price_excl_tax',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=12, null=True, verbose_name='Price (excl. tax)'),
        ),
    ]
