from django.db.models.signals import pre_delete, pre_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from oscar.apps.address.abstract_models import *
from oscar.apps.order.abstract_models import *

from amocrm_api.models import AbstractAmoCRMFields, AbstractAmoCRMNotesMixin
from catalogue.models import Product, Pack, Option
from customer.models import Contacts
from feedback.models import BaseFeedbackModel, WatchedMixinModel
from payment.models import PaymentMethod
from shipping.models import OrderAndItemCharges


class Line(models.Model):
    """
    An order line
    """
    order = models.ForeignKey(
        'order.Order',
        on_delete=models.CASCADE,
        related_name='lines',
        verbose_name=_("Order"))

    # We keep a link to the stockrecord used for this line which allows us to
    # update stocklevels when it ships
    stockrecord = models.ForeignKey(
        'partner.StockRecord', on_delete=models.SET_NULL, blank=True,
        null=True, verbose_name=_("Stock record"))

    # PRODUCT INFORMATION
    # -------------------

    # We don't want any hard links between orders and the products table so we
    # allow this link to be NULLable.
    product = models.ForeignKey(
        'catalogue.Product', on_delete=models.SET_NULL, blank=True, null=True,
        verbose_name=_("Product"))
    title = models.CharField(
        pgettext_lazy(u"Product title", u"Title"), max_length=255)
    # UPC can be null because it's usually set as the product's UPC, and that
    # can be null as well
    upc = models.CharField(_("UPC"), max_length=128, blank=True, null=True)

    quantity = models.PositiveIntegerField(_("Quantity"), default=1)

    # REPORTING INFORMATION
    # ---------------------

    # Price information (these fields are actually redundant as the information
    # can be calculated from the LinePrice models
    line_price_incl_tax = models.DecimalField(
        _("Price (inc. tax)"), decimal_places=2, max_digits=12, null=True)
    line_price_excl_tax = models.DecimalField(
        _("Price (excl. tax)"), decimal_places=2, max_digits=12, blank=True, null=True)

    # Price information before discounts are applied
    line_price_before_discounts_incl_tax = models.DecimalField(
        _("Price before discounts (inc. tax)"),
        decimal_places=2, max_digits=12, null=True)
    line_price_before_discounts_excl_tax = models.DecimalField(
        _("Price before discounts (excl. tax)"),
        decimal_places=2, max_digits=12, null=True)

    line_price_retail = models.DecimalField(
        'Себестоимость', decimal_places=2, max_digits=12, null=True)

    # Normal site price for item (without discounts)
    unit_price_incl_tax = models.DecimalField(
        _("Unit Price (inc. tax)"), decimal_places=2, max_digits=12,
        blank=True, null=True)
    unit_price_excl_tax = models.DecimalField(
        _("Unit Price (excl. tax)"), decimal_places=2, max_digits=12,
        blank=True, null=True)
    unit_price_excl_tax_incl_range_discount = models.DecimalField(
        'Цена единицы включая скидки на товар (без НДС)', decimal_places=2, max_digits=12,
        blank=True, null=True)
    unit_price_incl_tax_incl_range_discount = models.DecimalField(
        'Цена единицы включая скидки на товар (с НДС)', decimal_places=2, max_digits=12,
        blank=True, null=True)
    unit_price_retail = models.DecimalField(
        'Себестоимость единицы товара', decimal_places=2, max_digits=12,
        blank=True, null=True)

    selected_pack = models.ForeignKey(Pack, verbose_name='Комплект', blank=True, null=True)

    is_present = models.BooleanField('Подарок?', default=False)

    @property
    def options(self):
        return [
            {'option': attr.option, 'value': attr.value} for attr in self.attributes.all()
        ]

    @property
    def options_summary(self):
        options_str = ''
        for attr in self.attributes.all():
            value = attr.value
            if attr.option.code == Product.PACK_OPTION_CODE and value:
                try:
                    value = self.product.packs.get(pk=value)
                except Pack.DoesNotExist:
                    continue
            if value:
                options_str += '%s: %s,' % (attr.option.name, value)
        if len(options_str) > 0:
            options_str = options_str[:-1]
        return options_str

    class Meta:
        app_label = 'order'
        # Enforce sorting in order of creation.
        ordering = ['pk']
        verbose_name = _("Order Line")
        verbose_name_plural = _("Order Lines")

    def __str__(self):
        if self.product:
            title = self.product.title
        else:
            title = _('<missing product>')
        return _("Product '%(name)s', quantity '%(qty)s'") % {
            'name': title, 'qty': self.quantity}

    @property
    def discount_incl_tax(self):
        return self.line_price_before_discounts_incl_tax \
               - self.line_price_incl_tax

    @property
    def discount_excl_tax(self):
        return self.line_price_before_discounts_excl_tax \
               - self.line_price_excl_tax

    @property
    def line_price_tax(self):
        return self.line_price_incl_tax - self.line_price_excl_tax

    @property
    def unit_price_tax(self):
        return self.unit_price_incl_tax - self.unit_price_excl_tax

    @property
    def is_product_deleted(self):
        return self.product is None

    @staticmethod
    def pre_save(sender, **kwargs):
        instance = kwargs.get('instance')
        if instance.pk is not None:
            attribute, created = instance.attributes.get_or_create(
                option=Option.objects.get(code=Product.PACK_OPTION_CODE),
                type=Product.PACK_OPTION_CODE
            )

            if instance.selected_pack_id:
                attribute.value = instance.selected_pack_id
                attribute.save()
            else:
                attribute.delete()


pre_save.connect(Line.pre_save, sender=Line)


class Order(AbstractAmoCRMFields, WatchedMixinModel, BaseFeedbackModel):
    TEMPLATE_CODE = 'ORDER'

    STATUSES_PRE = -1
    STATUSES_NEW = 0
    STATUSES_SUBMITTED = 1
    STATUSES_DELIVER = 2
    STATUSES_DELIVERED = 3
    STATUSES_CLOSED = 4
    STATUSES_REJECTED = 5
    STATUSES_NEW_PHONE = 6
    STATUSES_CHANGED = 7
    STATUSES_PICKUP = 8
    STATUSES_PICKUP_COMPLETE = 9

    DO_NOT_SYNC_STATUSES = [STATUSES_PRE]
    SEND_RECEIPT_STATUS = STATUSES_SUBMITTED

    ADMIN_CREATE_STATUSES = (
        (STATUSES_PRE, 'Предварительная сделка'),
    )

    ADMIN_UPDATE_FROM_PRE_STATUSES = (
        (STATUSES_PRE, 'Предварительная сделка'),
        (STATUSES_NEW, 'Заявки'),
    )

    STATUSES = (
        (STATUSES_PRE, 'Предварительная сделка'),
        (STATUSES_NEW, 'Заявки'),
        (STATUSES_NEW_PHONE, 'Заявки телефон'),
        (STATUSES_CHANGED, 'Изменения'),
        (STATUSES_SUBMITTED, 'Подтвержденные'),
        (STATUSES_PICKUP, 'Самовывоз'),
        (STATUSES_DELIVER, 'Доставка'),
        (STATUSES_DELIVERED, 'Доставлены/Оплачены'),
        (STATUSES_PICKUP_COMPLETE, 'Самозабор'),
        (STATUSES_CLOSED, 'Успешно реализовано'),
        (STATUSES_REJECTED, 'Закрыто и не реализовано'),
    )

    status = models.IntegerField(verbose_name='Статус сделки', choices=STATUSES, default=0)

    # Total price looks like it could be calculated by adding up the
    # prices of the associated lines, but in some circumstances extra
    # order-level charges are added and so we need to store it separately
    currency = models.CharField(
        _("Currency"), max_length=12, default=get_default_currency)
    total_incl_tax = models.DecimalField(
        _("Order total (inc. tax)"), decimal_places=2, max_digits=12, default=0)
    total_excl_tax = models.DecimalField(
        _("Order total (excl. tax)"), decimal_places=2, max_digits=12, default=0)
    total_retail = models.DecimalField(
        'Себестоимоисть заказа', decimal_places=2, max_digits=12, default=0)

    # Shipping charges
    shipping_incl_tax = models.DecimalField(
        _("Shipping charge (inc. tax)"), decimal_places=2, max_digits=12,
        default=0)
    shipping_excl_tax = models.DecimalField(
        _("Shipping charge (excl. tax)"), decimal_places=2, max_digits=12,
        default=0)
    shipping_charge_resolved = models.BooleanField(verbose_name='Стоимость доставки определена', default=True)

    gift_card = models.OneToOneField('voucher.GiftCard', verbose_name='Подарочная карта', related_name='order',
                                     null=True, blank=True, on_delete=models.SET_NULL)

    # Not all lines are actually shipped (such as downloads), hence shipping
    # address is not mandatory.
    shipping_method = models.ForeignKey(OrderAndItemCharges, verbose_name='Способ доставки')
    shipping_date = models.DateField(verbose_name='Дата доставки', null=True, blank=True)
    payment_method = models.ForeignKey(PaymentMethod, verbose_name='Способ оплаты')

    client = models.ForeignKey('customer.User', verbose_name='Клиент', null=True, blank=True,
                               related_name='orders', on_delete=models.SET_NULL)

    # Index added to this field for reporting
    date_placed = models.DateTimeField(verbose_name='Дата создания', db_index=True, auto_now_add=True, blank=True)

    @property
    def number(self):
        return self.pk

    @property
    def address(self):
        """AMOCRM field value
        """
        try:
            return str(self.shipping_address)
        except ShippingAddress.DoesNotExist:
            return ''

    @property
    def phone(self):
        """AMOCRM field value
        """
        try:
            return self.shipping_contacts.phone_number.as_international
        except ShippingContacts.DoesNotExist:
            return ''


    @property
    def fio(self):
        """AMOCRM field value
        """
        try:
            return self.shipping_contacts.first_name
        except ShippingContacts.DoesNotExist:
            return ''

    @property
    def basket_total_before_discounts_incl_tax(self):
        """
        Return basket total including tax but before discounts are applied
        """
        total = D('0.00')
        for line in self.lines.all():
            total += line.line_price_excl_tax
        return total

    @property
    def basket_total_before_discounts_excl_tax(self):
        """
        Return basket total excluding tax but before discounts are applied
        """
        total = D('0.00')
        for line in self.lines.all():
            total += line.line_price_excl_tax
        return total

    @property
    def basket_total_incl_tax_excl_gift_card(self):
        """
        Return basket total including tax
        """
        total = self.shipping_incl_tax
        for line in self.lines.all():
            total += line.line_price_incl_tax
        return total

    @property
    def basket_total_excl_tax_excl_gift_card(self):
        """
        Return basket total excluding tax
        """
        total = self.shipping_excl_tax
        for line in self.lines.all():
            total += line.line_price_excl_tax
        return total

    @property
    def basket_total_incl_tax(self):
        return self.total_incl_tax + self.shipping_incl_tax if self.total_incl_tax is not None else None

    @property
    def basket_total_excl_tax(self):
        return self.total_excl_tax + self.shipping_excl_tax if self.total_excl_tax is not None else None

    @property
    def total_before_discounts_incl_tax(self):
        return (self.basket_total_before_discounts_incl_tax +
                self.shipping_incl_tax) if self.basket_total_before_discounts_incl_tax is not None else None

    @property
    def total_before_discounts_excl_tax(self):
        return (self.basket_total_before_discounts_excl_tax +
                self.shipping_excl_tax) if self.basket_total_before_discounts_excl_tax is not None else None

    @property
    def total_discount_incl_tax(self):
        """
        The amount of discount this order received
        """
        discount = D('0.00')
        for line in self.lines.all():
            discount += line.discount_incl_tax
        return discount

    @property
    def total_discount_excl_tax(self):
        discount = D('0.00')
        for line in self.lines.all():
            discount += line.discount_excl_tax
        return discount

    @property
    def total_tax(self):
        return self.total_incl_tax - self.total_excl_tax

    @property
    def num_lines(self):
        return self.lines.count()

    @property
    def num_items(self):
        """
        Returns the number of items in this order.
        """
        num_items = 0
        for line in self.lines.all():
            num_items += line.quantity
        return num_items

    @property
    def shipping_tax(self):
        return self.shipping_incl_tax - self.shipping_excl_tax

    @property
    def has_shipping_discounts(self):
        return len(self.shipping_discounts) > 0

    @property
    def shipping_before_discounts_incl_tax(self):
        # We can construct what shipping would have been before discounts by
        # adding the discounts back onto the final shipping charge.
        total = D('0.00')
        for discount in self.shipping_discounts:
            total += discount.amount
        return self.shipping_incl_tax + total

    class Meta:
        app_label = 'order'
        ordering = ['-date_placed']
        verbose_name = _("Order")
        verbose_name_plural = _("Orders")

    def __str__(self):
        return "#%s" % (self.id,)

    def verification_hash(self):
        key = '%s%s' % (self.id, settings.SECRET_KEY)
        hash = hashlib.md5(key.encode('utf8'))
        return hash.hexdigest()

    @property
    def basket_discounts(self):
        # This includes both offer- and voucher- discounts.  For orders we
        # don't need to treat them differently like we do for baskets.
        return self.discounts.filter(
            category=AbstractOrderDiscount.BASKET)

    @property
    def shipping_discounts(self):
        return self.discounts.filter(
            category=AbstractOrderDiscount.SHIPPING)

    @property
    def post_order_actions(self):
        return self.discounts.filter(
            category=AbstractOrderDiscount.DEFERRED)

    def set_date_placed_default(self):
        if self.date_placed is None:
            self.date_placed = now()


class ShippingAddress(AbstractAmoCRMNotesMixin, models.Model):
    order = models.OneToOneField(Order, verbose_name='Заказ', related_name='shipping_address', null=True, blank=True)
    onestring = models.CharField(verbose_name='Исходная строка с адресом', max_length=1024, default='', blank=True)
    region = models.CharField(verbose_name='Регион', max_length=1024, null=True, blank=True)
    city = models.CharField(verbose_name='Город', max_length=1024)
    street = models.CharField(verbose_name='Улица', max_length=1024)
    house = models.CharField(verbose_name='Дом', max_length=10)
    block = models.CharField(verbose_name='Строение', max_length=10, null=True, blank=True)
    flat = models.CharField(verbose_name='Квартира', max_length=10, null=True, blank=True)
    lat = models.FloatField(verbose_name='Широта', null=True, blank=True)
    lng = models.FloatField(verbose_name='Долгота', null=True, blank=True)
    notes = models.TextField(
        blank=True, verbose_name=_('Instructions'),
        help_text=_("Tell us anything we should know when delivering "
                    "your order."))

    @property
    def element_id(self):
        return self.order.amocrm_id

    def concat_in_onestring(self):
        onestirng = ''

        if self.region:
            onestirng += self.region + " "

        if self.city:
            onestirng += 'город %s ' % self.city

        if self.street:
            onestirng += 'улица %s ' % self.street

        if self.house:
            onestirng += "дом %s " % self.house

        if self.block:
            onestirng += "стр %s " % self.block

        if self.flat:
            onestirng += "квартира %s " % self.flat

        return onestirng

    def get_coordinates(self):
        return {'lat': self.lat, 'lng': self.lng} if self.lat and self.lng else None

    @property
    def summary(self):
        return self.onestring

    def generate_hash(self):
        """
        Returns a hash of the address summary
        """
        # We use an upper-case version of the summary
        return zlib.crc32(self.summary.strip().upper().encode('UTF8'))

    def __str__(self):
        return self.onestring or self.concat_in_onestring()

    class Meta:
        verbose_name = 'Адрес доставки'
        verbose_name_plural = 'Адреса доставки'


class ShippingContacts(AbstractAmoCRMNotesMixin, Contacts):
    order = models.OneToOneField(Order, verbose_name='Заказ', related_name='shipping_contacts', null=True, blank=True)
    notes = models.TextField(
        blank=True, verbose_name=_('Instructions'), default="",
        help_text=_("Tell us anything we should know when delivering "
                    "your order."))

    @property
    def element_id(self):
        return self.order.amocrm_id

    @staticmethod
    def create_fast_checkout_contacts(order, first_name, phone):
        return ShippingContacts.objects.create(
            first_name=first_name,
            order=order,
            phone_number=phone,
            notes=""
        )

    def save(self, **kwargs):
        super(ShippingContacts, self).save(**kwargs)

        # We place it here, because we override parent method and it's necessary to call
        # these operation after parent save method finished
        if self.order_id:
            order = self.order
            order.client = self.recognize_user()
            order.save()

    class Meta:
        verbose_name = 'Контакты клиента'
        verbose_name_plural = 'Контакты клиентов'


from oscar.apps.order.models import *


@receiver(pre_delete, sender=OrderDiscount)
def on_order_discount_delete(sender, instance, using, **kwargs):
    offer = instance.offer
    voucher = instance.voucher

    if offer:
        offer.rollback_usage({
            'offer': offer,
            'voucher': voucher,
            'freq': instance.frequency,
            'discount': instance.amount
        })

    if voucher:
        voucher.rollback_discount(instance.amount)
        voucher.rollback_usage(instance.order)
