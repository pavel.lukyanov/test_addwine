import logging
import json
from django.core.paginator import InvalidPage
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.template import loader
from django.urls import reverse
from django.views.generic import TemplateView
from oscar.apps.catalogue.search_handlers import get_product_search_handler_class

from amocrm_api.utils import amocrm_sync_subscription_for_discount
from catalogue.views import CataloguePageMixin, CatalogueSearchMixin
from common.views import CatchFormView
from marketing.models import SEOText
from offer.forms import SubscriptionForDiscountForm
from offer.models import Range
from offer.utils import sort_products_with_range_order
from seo.views import SeoParamsMixin

logger = logging.getLogger(__name__)


class RangeDetailView(CatalogueSearchMixin, CataloguePageMixin, SeoParamsMixin, TemplateView):
    context_object_name = 'products'
    template_name = 'catalogue/category.html'
    enforce_paths = True

    def get(self, request, *args, **kwargs):
        self.range = self.get_range()
        try:
            self.search_handler = self.get_search_handler(
                self.request.GET, request.get_full_path(), **self.get_search_kwargs())
        except InvalidPage:
            return redirect('catalogue:index')
        return super(RangeDetailView, self).get(request, *args, **kwargs)

    def get_search_kwargs(self):
        return {'product_ranges': [self.range]}

    def get_context_data(self, **kwargs):
        context = super(CatalogueSearchMixin, self).get_context_data(**kwargs)
        context.update(self.search_handler.get_search_context_data(
            self.context_object_name))
        context = self.patch_context(context, self.request.is_ajax())

        range_obj = self.range
        context['range'] = range_obj
        context['summary'] = range_obj.name
        context.update(self.extract_seo_configs(range_obj))
        context['seo_texts'] = SEOText.objects.filter(ranges__in=range_obj.seo_texts.all())
        context['mail_cta'] = range_obj.mail_cta
        context['product_parent_pointer'] = '?range=%s' % (range_obj.slug,)
        context['breadcrumbs'] = [
            {
                'title': 'каталог',
                'href': reverse('catalogue:index')
            },
            {
                'title': range_obj.name,
                'href': range_obj.get_absolute_url()
            },
        ]

        # Sort using the RangeOrder model manual_order_id field from admin panel
        if self.request.GET.get('sort_by', 'manual_order') == 'manual_order':
            try:
                order_set = range_obj.rangeorder_set.all()
                if order_set:
                    products_list = sort_products_with_range_order(context['products'], order_set)
                    if self.request.is_ajax():
                        template = loader.get_template(self.AJAX_PRODUCTS_TEMPLATE)
                        data = json.loads(context['filters_state_json'])
                        data['products_html'] = template.render(
                            {'products': products_list}, self.request)
                        context['filters_state_json'] = json.dumps(data)
                    else:
                        context['products'] = products_list
            except Exception as e:
                logger.debug('Catch exception in ordering range for Range: {}'.format(range_obj.pk))

        return context

    def get_search_handler(self, *args, **kwargs):
        return get_product_search_handler_class()(*args, **kwargs)

    def get_range(self):
        if 'pk' in self.kwargs:
            return get_object_or_404(Range, pk=self.kwargs['pk'], is_public=True)
        elif 'slug' in self.kwargs:
            return get_object_or_404(Range, slug=self.kwargs['slug'], is_public=True)

        raise Http404


class SubscribeForDiscountView(CatchFormView):
    form_class = SubscriptionForDiscountForm
    form_name = 'subscribe_for_discount'

    def render_to_response(self, context_data):
        return HttpResponseRedirect(self.get_result_url(True))

    def form_valid(self, form):
        form.instance.name = form.cleaned_data['name']
        form.instance.save()
        amocrm_sync_subscription_for_discount(form.instance.id)
        return super().form_valid(form)

    def form_invalid(self, form):
        return super().form_invalid(form)


