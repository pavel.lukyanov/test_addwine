from customer.models import CommunicationEventType
from feedback.utils import send_mail
from .tasks import update_discounts as update_discounts_task


def update_discounts(discount_id):
    update_discounts_task.apply_async(args=(discount_id,), countdown=1)


def send_notification_about_discount(product):
    from .models import SubscriptionForDiscount
    email_template = CommunicationEventType.objects.get(code=SubscriptionForDiscount.EMAIL_TEMPLATE_CODE)
    subscriptions = product.subscription_for_discount.all()
    for subscription in subscriptions:
        send_mail(email_template, subscription.as_dict(), [subscription.email, ])
    subscriptions.delete()


def sort_products_with_range_order(products, range_order_list):
    range_order_product_dict = {
        range_order.product_id: range_order.manual_order_id
        for range_order in range_order_list
    }
    products_list_len = len(products)
    for p in products:
        range_order_product_dict[p.pk] = (range_order_product_dict[p.pk], p)
    ordered_products = [rop for rop in range_order_product_dict.values() if isinstance(rop, tuple)]
    ordered_products = sorted(ordered_products, key=lambda x: x[0])
    products_list = [pr[1] for pr in ordered_products]

    if products_list_len == len(products_list):
        return products_list
    else:
        return products
