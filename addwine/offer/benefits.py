from decimal import Decimal

from math import ceil
from oscar.apps.offer import benefits


def apply_discount(line, line_discount, quantity_affected, offer):
    line.discount(line_discount, quantity_affected, incl_tax=False, offer=offer)


class BestPercentageDiscountBenefit(benefits.PercentageDiscountBenefit):
    def apply(self, basket, condition, offer, discount_percent=None,
              max_total_discount=None):
        if discount_percent is None:
            discount_percent = self.value

        discount_amount_available = max_total_discount

        line_tuples = self.get_applicable_lines(offer, basket)
        discount_percent = min(discount_percent, Decimal('100.0'))
        discount = Decimal('0.00')
        affected_items = 0
        max_affected_items = self._effective_max_affected_items()
        for price, line in line_tuples:
            if affected_items >= max_affected_items:
                break
            if discount_amount_available == 0:
                break

            quantity_affected = min(line.quantity, max_affected_items - affected_items)
            if self.one_product_application:
                line_discount = self.round(discount_percent / Decimal('100.0') * price)
            else:
                line_discount = self.round(discount_percent / Decimal('100.0') * price
                                           * int(quantity_affected))

            if discount_amount_available is not None:
                line_discount = min(line_discount, discount_amount_available)

            if line_discount <= line.discount_value:
                continue

            if discount_amount_available is not None:
                discount_amount_available -= line_discount

            line.clear_discount()
            apply_discount(line, line_discount, quantity_affected, offer)

            affected_items += quantity_affected
            discount += line_discount

        return benefits.results.BasketDiscount(discount)


class BestAbsoluteDiscountBenefit(benefits.AbsoluteDiscountBenefit):

    MAX_DISCOUNT_PERCENT = Decimal('0.5')

    def apply(self, basket, condition, offer, discount_amount=None,
              max_total_discount=None):
        discount_amount = self.value
        discount_amount_available = max_total_discount

        line_tuples = self.get_applicable_lines(offer, basket)
        discount = Decimal('0.00')
        affected_items = 0
        max_affected_items = self._effective_max_affected_items()
        for price, line in line_tuples:
            if affected_items >= max_affected_items:
                break
            if discount_amount_available == 0:
                break

            current_discount = min(self.round(self.MAX_DISCOUNT_PERCENT*price), discount_amount)
            quantity_affected = min(line.quantity, max_affected_items - affected_items)

            if self.one_product_application:
                line_discount = current_discount
            else:
                line_discount = self.round(current_discount * int(quantity_affected))

            if discount_amount_available is not None:
                line_discount = min(line_discount, discount_amount_available)

            if line_discount <= line.discount_value:
                continue

            if discount_amount_available is not None:
                discount_amount_available -= line_discount

            line.clear_discount()
            apply_discount(line, line_discount, quantity_affected, offer)

            affected_items += quantity_affected
            discount += line_discount

        return benefits.results.BasketDiscount(discount)
