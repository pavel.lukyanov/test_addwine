from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.test import RequestFactory

request_factory = RequestFactory()

logger = get_task_logger(__name__)
TASKS_CONFIGS_UPDATE_DISCOUNTS = {
    'name': "catalogue_discounts.update",
}
TASKS_CONFIGS_UPDATE_DISCOUNTS.update(settings.CATALOGUE_DISCOUNTS_TASK_CONFIG)


@shared_task(**TASKS_CONFIGS_UPDATE_DISCOUNTS)
def update_discounts(discount_id):
    from offer.models import RangeDiscount
    try:
        discount = RangeDiscount.objects.get(id=discount_id)
    except RangeDiscount.DoesNotExist:
        return

    discount.selected_products.update(discount=None)
    products = discount.range.all_products()

    discounts = RangeDiscount.objects.filter(active=True).select_related('range', )
    discounts_info = []
    for discount in discounts:
        if not discount.is_available():
            continue
        discounts_info.append({
            'discount': discount,
            'products': discount.range.all_products()
        })

    for product in products:
        selected_discount = None
        for discount_info in discounts_info:
            if product in discount_info['products']:
                selected_discount = discount_info['discount']
                break

        product.apply_discount(selected_discount)


