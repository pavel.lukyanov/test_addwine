# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-05 21:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('offer', '0007_range_rules_guarantee'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='range',
            name='included_products'
        ),
        migrations.AddField(
            model_name='range',
            name='included_products',
            field=models.ManyToManyField(blank=True, related_name='includes', to='catalogue.Product', verbose_name='Included Products'),
        ),
    ]
