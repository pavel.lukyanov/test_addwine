from django.test import TestCase
from model_mommy import mommy

from offer.utils import sort_products_with_range_order
from ..models import Range, RangeOrder


class TestRangeOrderUpdate(TestCase):
    def setUp(self):
        self.range = mommy.make_recipe('offer.rcp_range')
        self.product_list = mommy.make_recipe('catalogue.rcp_product', _quantity=10)
        self.range.included_products.set(self.product_list)

    def test_create_range_orders_on_range_save(self):
        self.range.save()

        range_obj = Range.objects.get(pk=self.range.pk)
        self.assertEqual(range_obj.all_products().count(), 10)
        self.assertEqual(RangeOrder.objects.count(), 10)

    def test_do_not_double_range_orders_on_second_save(self):
        self.range.save()
        self.range.save()

        self.assertEqual(self.range.all_products().count(), 10)
        self.assertEqual(RangeOrder.objects.count(), 10)

    def test_have_default_order_after_first_save(self):
        self.range.save()
        self.assertNotEqual([r.manual_order_id for r in RangeOrder.objects.only('manual_order_id')], [0 for _ in range(10)])


class TestRangeOrderSortingFunction(TestCase):
    def setUp(self):
        self.range = mommy.make_recipe('offer.rcp_range')
        self.product_list = mommy.make_recipe('catalogue.rcp_product', _quantity=20)
        self.range.included_products.set(self.product_list)
        self.range.save()

    def test_sort_all_products(self):
        sorted_products = sort_products_with_range_order(self.product_list, self.range.rangeorder_set.all())

        self.assertEqual(
            [p.pk for p in sorted_products],
            [ro.product_id for ro in RangeOrder.objects.all().order_by('manual_order_id')]
        )

    def test_sort_part_of_products(self):
        sorted_products = sort_products_with_range_order(self.product_list[2:10], self.range.rangeorder_set.all())

        self.assertEqual(
            [p.pk for p in sorted_products],
            [ro.product_id for ro in RangeOrder.objects.filter(product__in=sorted_products).order_by('manual_order_id')]
        )
