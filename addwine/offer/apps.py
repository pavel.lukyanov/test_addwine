from django.apps import AppConfig


class OfferConfig(AppConfig):
    name = 'offer'
    verbose_name = 'Скидочные программы'
