from adminsortable2.admin import SortableAdminMixin
from ckeditor.widgets import CKEditorWidget
from dal import autocomplete
from django import forms
from django.db import models
from django.utils.html import strip_tags
from treebeard.admin import TreeAdmin
from treebeard.forms import movenodeform_factory

from offer.models import Range, RangeOrder, RangeDiscount, RangeSEOText, RangeGroup, RangeInnerCategory, SubscriptionForDiscount
from oscar.apps.offer.admin import *

from offer.utils import update_discounts
from search.admin import IndexUpdateRelatedAdminMixin
from yandex_market.admin import YandexMarketUpdateRelatedAdminMixin
from django.contrib import admin
from adminsortable2.admin import SortableInlineAdminMixin



class RangeForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = Range
        widgets = {
            'mail_cta': autocomplete.ModelSelect2(url='autocomplete_mail_cta'),
            'sommelier_cta': autocomplete.ModelSelect2(url='autocomplete_sommelier_cta'),
            'inner_category': autocomplete.ModelSelect2(url='autocomplete_range_inner_categories'),
        }


class RangeSEOTextInlineForm(forms.ModelForm):
    class Meta:
        model = RangeSEOText
        fields = '__all__'
        widgets = {
            'seo_text': autocomplete.ModelSelect2(url='autocomplete_seo_text'),
        }


class RangeSEOTextInline(admin.StackedInline):
    model = RangeSEOText
    form = RangeSEOTextInlineForm
    min_num = 0
    extra = 1


@admin.register(RangeInnerCategory)
class RangeInnerCategoryAdmin(admin.ModelAdmin):
    list_display = ('title',)


admin.site.unregister(Range)


class SortProductInline(SortableInlineAdminMixin, admin.TabularInline):
    model = RangeOrder
    can_delete = False
    extra = 0
    readonly_fields = ('product',)

    def get_queryset(self, request):
        self.model.objects.select_related('product__title')


@admin.register(Range)
class RangeAdmin(YandexMarketUpdateRelatedAdminMixin, IndexUpdateRelatedAdminMixin, SortableAdminMixin, admin.ModelAdmin):
    form = RangeForm
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget()},
    }
    prepopulated_fields = {
        'slug': ('name', )
    }

    list_display = ('name', 'list_display_comment', 'displayed_in_menu', 'inner_category', 'is_public')
    list_filter = ('inner_category', 'group', 'is_public',)
    search_fields = ('name', 'slug')
    filter_horizontal = ('included_categories', 'included_products', 'included_brands', 'classes', 'excluded_products')
    inlines = [RangeSEOTextInline, SortProductInline, ]

    def displayed_in_menu(self, instance):
        return instance.group is not None

    displayed_in_menu.boolean = True
    displayed_in_menu.short_description = 'Отображается в меню'

    def list_display_comment(self, instance):
        return strip_tags(instance.admin_comment)

    list_display_comment.short_description = 'Комментарий'

    fieldsets = (
        (None, {
            'fields': ('group', 'inner_category', 'name',  'slug', 'is_public', 'admin_comment', 'mail_cta', 'sommelier_cta', 'rules_guarantee')
        }),
        ('Содержимое', {
            'classes': ('collapse',),
            'fields': (
                'included_categories', 'classes', 'included_products', 'excluded_products', 'included_brands',
            ),
        }),
        ('SEO', {
            'classes': ('collapse',),
            'fields': (
                'seo_title', 'seo_description', 'seo_keywords',
                'seo_og_title', 'seo_og_description', 'seo_og_image',
            ),
        }),
    )

    def get_queryset(self, request):
        return super().get_queryset(request).filter(hidden_in_admin=False)


class RangeDiscountForm(forms.ModelForm):
    class Meta:
        exclude = ('hidden_in_admin',)
        model = RangeDiscount
        widgets = {
            'range': autocomplete.ModelSelect2(url='autocomplete_range'),
            'mail_cta': autocomplete.ModelSelect2(url='autocomplete_mail_cta'),
        }


@admin.register(RangeDiscount)
class RangeDiscountAdmin(admin.ModelAdmin):
    form = RangeDiscountForm
    exclude = ('hidden_in_admin',)
    prepopulated_fields = {'code': ('name',), }
    list_display = ('name', 'code', 'type', 'range', 'value', 'priority', 'display_for_users')
    list_editable = ('priority', 'display_for_users')
    readonly_fields = ('total_discount', 'num_orders', 'date_created')

    fieldsets = (
        ('Общее', {
            'fields': [
                        'cover', 'name', 'code', 'rules', 'mail_cta', 'display_for_users',
                       ]
        }),
        ('Настройки', {
            'fields': [
                'range', 'type', 'value', 'priority', 'end_datetime',
                'active', 'date_created'
            ]
        }),
        ('SEO', {
            'fields': (
                'seo_title', 'seo_description', 'seo_keywords',
                'seo_og_title', 'seo_og_description', 'seo_og_image',
            ),
        }),
    )

    def get_queryset(self, request):
        return super().get_queryset(request).filter(hidden_in_admin=False)


@admin.register(RangeGroup)
class RangeGroupAdmin(TreeAdmin):
    form = movenodeform_factory(RangeGroup)
    fields = ('title', '_position', '_ref_node_id',)
    list_display = ('title',)
    search_fields = ('title',)


@admin.register(SubscriptionForDiscount)
class SubscriptionForDiscountAdmin(admin.ModelAdmin):
    fields = ('phone_number', 'email', 'first_name', 'last_name', 'product', 'amocrm_id', 'date_created')
    list_display = ('product', 'email', 'phone_number', 'first_name', 'last_name', 'amocrm_id')
    search_fields = ('product__name', 'email', 'phone_number', 'first_name', 'last_name')
    readonly_fields = ('product', 'email', 'phone_number', 'first_name', 'last_name', 'amocrm_id', 'date_created', )