from django.conf.urls import url

from common.views import get_admin_autocomplete_view
from offer.models import Range, RangeInnerCategory
from offer.views import SubscribeForDiscountView

urlpatterns = [
    url(r'autocomplete/offer/ranges$', get_admin_autocomplete_view(Range, 'name', additional_filter={'hidden_in_admin': False}).as_view()
        , name='autocomplete_range'),
    url(r'autocomplete/offer/ranges_inner_categories', get_admin_autocomplete_view(RangeInnerCategory, 'title').as_view()
        , name='autocomplete_range_inner_categories'),
    url(r'subscribe-for-discount$', SubscribeForDiscountView.as_view(), name='subscribe-for-discount'),
]
