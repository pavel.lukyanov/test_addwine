from django.apps import AppConfig


class CkeditorCustomizationConfig(AppConfig):
    name = 'ckeditor_customization'
