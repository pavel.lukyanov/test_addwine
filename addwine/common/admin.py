from dal import autocomplete
from django import forms
from django.contrib import admin

from common.models import Settings, AboutCompany, Contacts, VideoReviewAboutCompany, DocumentAboutCompany, PageConfigs, \
    PageConfigsSEOText, WorkTimeInterval
from site_configs.admin import SingletonModelAdmin


class PageConfigsSEOTextInlineForm(forms.ModelForm):
    class Meta:
        model = PageConfigsSEOText
        fields = '__all__'
        widgets = {
            'seo_text': autocomplete.ModelSelect2(url='autocomplete_seo_text'),
        }


class PageConfigsSEOTextInline(admin.StackedInline):
    model = PageConfigsSEOText
    form = PageConfigsSEOTextInlineForm
    min_num = 0
    max_num = 3
    extra = 1


class PageConfigsForm(forms.ModelForm):
    class Meta:
        model = PageConfigs
        fields = '__all__'
        widgets = {
            'mail_cta': autocomplete.ModelSelect2(url='autocomplete_mail_cta'),
        }


@admin.register(PageConfigs)
class PageConfigsAdmin(admin.ModelAdmin):
    form = PageConfigsForm
    fieldsets = (
        (None, {
            'fields': ('page_code', 'mail_cta')
        }),
        ('SEO', {
            'fields': (
                'seo_title', 'seo_description', 'seo_keywords',
                'seo_og_title', 'seo_og_description', 'seo_og_image',
            ),
        }),
    )
    list_display = ('page_code', 'seo_title')
    readonly_fields = ('page_code',)
    inlines = [PageConfigsSEOTextInline, ]

    def get_actions(self, request):
        actions = super().get_actions(request)
        del actions['delete_selected']
        return actions

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class WorkTimeIntervalInline(admin.TabularInline):
    model = WorkTimeInterval
    min_num = 0
    extra = 0
    fields = (('start_day_of_week', 'end_day_of_week'), ('start_work_time', 'end_work_time'))


@admin.register(AboutCompany)
class AboutCompanyAdmin(SingletonModelAdmin):
    fields = ['company_story', 'rules_pay_and_delivery', 'map_icon', 'probich_image', 'delivery_info',
              'rules_return', 'rules_guarantee',
              'rules_personal', 'rules_confidential', 'rules_offert']


@admin.register(Contacts)
class ContactsAdmin(SingletonModelAdmin):
    fieldsets = (
        (
            None, {
                'fields': ('email', 'phone', 'whatsapp',
                           'address', 'short_address', 'ogrnip', 'inn', 'legal_information')
            }
        ),
        (
            'Данные GPS навигатора', {
                'fields': ('gps_lat', 'gps_lng')
            }
        ),
        (
            'Социальные сети', {
                'fields': ('networks_facebook', 'networks_instagram', 'networks_youtube')
            }
        )
    )

    inlines = [WorkTimeIntervalInline, ]


@admin.register(Settings)
class SettingsAdmin(SingletonModelAdmin):
    fieldsets = (
        (
            None, {
                'fields': ('send_notify_on_mail', 'maintenance_mode', 'networks_instagram_token')
            }
        ),
        (
            'Модальное окно', {
                'fields': ('window_inform_image', 'window_inform_text',
                           'window_inform_text_special', 'window_inform_text_bottom', 'window_inform_active')
            }
        ),
        (
            'Телеграм', {
                'fields': ('telegram_test_channel_id', 'telegram_channel_id')
            }
        ),
        (
            'Yandex.Market', {
                'fields': ('yandex_market_shop_name', 'yandex_market_shop_company')
            }
        ),
    )


@admin.register(DocumentAboutCompany)
class DocumentAboutCompanyAdmin(admin.ModelAdmin):
    fields = ['title', 'doc']
    list_display = ['title']


@admin.register(VideoReviewAboutCompany)
class VideoReviewAboutCompanyAdmin(admin.ModelAdmin):
    fields = ['video', 'title', 'subtitle']
    list_display = ['title']
