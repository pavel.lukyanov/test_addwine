from dal import autocomplete
from django.conf import settings
from django.contrib.sitemaps import Sitemap
from django.core.exceptions import ImproperlyConfigured
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.views.generic import TemplateView
from django.views.generic.edit import BaseFormView

from checkout.forms import FastCheckoutForm
from common.models import PageConfigs
from customer.forms import ProductAlertForm, BirthdaySaveForm
from feedback.forms import BackCallForm
from marketing.forms import MailCTAForm
from marketing.models import SEOText
from news.models import Article
from order.models import Order, ShippingContacts
from seo.views import SeoParamsMixin
from web.mixins import MenuViewMixin
from django.utils import timezone
from datetime import timedelta

class PageSitemap(Sitemap):
    changefreq = "weekly"
    priority = 1

    pages = []

    def __init__(self, pages):
        self.pages = pages

    def items(self):
        return self.pages

    def location(self, obj):
        return "/" + obj


def robots(request):
    return render_to_response('common/robots.txt', content_type="text/plain")


class PageViewMixin(SeoParamsMixin, MenuViewMixin):
    page_code = ''
    page_css = ''
    page_script = ''
    context_params = []

    def get_context_data(self, **kwargs):
        context = super(PageViewMixin, self).get_context_data(**kwargs)

        context['page_style'] = self.page_css
        context['page_script'] = self.page_script
        for param in self.context_params:
            context[param['key']] = param['value']

        context['form_backcall'] = BackCallForm()
        context['form_fast_checkout'] = FastCheckoutForm()
        context['form_product_alert'] = ProductAlertForm()
        context['form_mail_cta'] = MailCTAForm()
        context['form_birthday_save'] = BirthdaySaveForm()

        # Was set after order checkout
        last_checkout_order_id = self.request.session.pop('last_checkout_order', None)
        last_checkout_order = None
        if last_checkout_order_id:
            try:
                last_checkout_order = Order.objects.get(pk=last_checkout_order_id)
            except Order.DoesNotExist:
                pass

        if last_checkout_order:
            context['last_order_id'] = last_checkout_order.id
            context['last_client_id'] = last_checkout_order.client_id
            contacts = ShippingContacts.objects.get(order=last_checkout_order)
            context['last_order_contact_email'] = contacts.email
            shipping_date = last_checkout_order.shipping_date
            if not shipping_date:
                shipping_date = timezone.localdate() + timedelta(days=1)

            context['last_order_shipping_date'] = shipping_date.strftime('%Y-%m-%d')
            context['last_order_gtins'] = [line.product.gtin for line in last_checkout_order.lines.all() if line.product.gtin]

        if self.page_code:
            page_configs = PageConfigs.objects.get_or_create(page_code=self.page_code)[0]
            page_configs_dict = self.extract_seo_configs(page_configs)
            page_configs_dict['mail_cta'] = page_configs.mail_cta
            page_configs_dict['seo_texts'] = SEOText.objects.filter(page_configs__in=page_configs.seo_texts.all())
            context.update(page_configs_dict)

        return context


class PageView(PageViewMixin, TemplateView):
    pass


def get_admin_autocomplete_view(model_class, field_name, additional_filter=None):
    if additional_filter is None:
        additional_filter = {}

    class AutocompleteView(autocomplete.Select2QuerySetView):
        def get_queryset(self):
            if not self.request.user.is_staff:
                return model_class.objects.none()

            qs = model_class.objects.all()
            qs = qs.filter(**additional_filter)

            if self.q:
                qs = qs.filter(**{field_name + '__icontains': self.q})

            return qs

    return AutocompleteView


class CatchFormView(BaseFormView):
    form_name = ''

    def get_result_url(self, success):
        return self.request.META.get('HTTP_REFERER', "/") + self.generate_tail(success)

    def get_success_url(self):
        try:
            super(BaseFormView, self).get_success_url()
        except ImproperlyConfigured:
            return self.get_result_url(True)

    def render_to_response(self, context_data):
        # Always false because it calls only when form is invalid
        return HttpResponseRedirect(self.get_result_url(False))

    def generate_tail(self, success):
        tail_str = "#success=" + str(success)
        if self.form_name:
            tail_str += "&form_name=" + self.form_name
        return tail_str
