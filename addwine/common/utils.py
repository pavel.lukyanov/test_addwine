from unidecode import unidecode
from django.utils.text import slugify

from common.models import Settings


def slugify_with_translit(text):
    return slugify(unidecode(text))

def get_admin_settings():
    return Settings.get_instance()