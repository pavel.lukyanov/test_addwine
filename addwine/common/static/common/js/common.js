// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires === "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString)
        options.expires = expires.toUTCString();

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
          updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});

var sendRequest = function (url, callback, data, methodName, isJSON) {
    if (typeof(isJSON) === "undefined")
        isJSON = true;
    var options = {
        context: this,
        cache: false,
        type: methodName,
        url: url,
        dataType: 'text',
        error: function (data) {
            callback(false, data.statusText);
        },
        success: function (data) {
            var resp = null;
            var success = true;
            if (isJSON)
                try {
                    resp = JSON.parse(data);
                } catch (e) {
                    success = false;
                    resp = "Can't parse response";
                }
            else
                resp = data;
            callback(success, resp);
        }
    };
    if (methodName == "POST") options.data = data;
    $.ajax(options);
};

function checkIsIOS() {

    var iDevices = [
        'iPad Simulator',
        'iPhone Simulator',
        'iPod Simulator',
        'iPad',
        'iPhone',
        'iPod'
    ];

    if (!!navigator.platform) {
        while (iDevices.length) {
            if (navigator.platform === iDevices.pop()) {
                return true;
            }
        }
    }

    return false;
}

function parseQueryString(query) {
    var vars = query.split("&");
    var query_string = {};
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (typeof pair[0] === "undefined" || typeof pair[1] === "undefined")
            continue;
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            query_string[pair[0]] = [query_string[pair[0]], decodeURIComponent(pair[1])];
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}

var getScreenSizeType = function () {
    var screenType = "";
    if ($(".visible-xs").css("display") == "block")
        screenType = "xs";
    else if ($(".visible-sm").css("display") == "block")
        screenType = "sm";
    else if ($(".visible-md").css("display") == "block")
        screenType = "md";
    else if ($(".visible-lg").css("display") == "block")
        screenType = "lg";
    return screenType;
};

var isMobileScreen = function () {
    var screenSize = getScreenSizeType();
    return screenSize == "xs";
};

var smoothScrollTo = function (y, time, callback) {
    $("html, body").stop().animate({
        scrollTop: y
    }, time, callback);
};

$(function () {
    var modal = $('.modal');
    modal.on('show.bs.modal', function () {
        $("body").css("overflow", "hidden");
        $(".content-overlay").show();
    });
    modal.on('hidden.bs.modal', function () {
        $("body").css("overflow", "auto");
        $(".content-overlay").hide();
    });
});

var checkBrowser = function (browserName) {
    return navigator.userAgent.match(new RegExp(browserName, "i")) != null;
};

var extractTextFromContentEditableDiv = function (contentEditableDiv) {
    var ce = $("<div></div>").html(contentEditableDiv.html());
    if (checkBrowser("webkit"))
        ce.find("div").replaceWith(function () {
            return " \n" + this.innerHTML;
        });
    if (checkBrowser("msie"))
        ce.find("p").replaceWith(function () {
            return this.innerHTML + " \n";
        });

    return ce.text();
};

var parseHash = function (hash) {
    var query_string = {};
    var query = ((hash) ? hash : window.location.hash).substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair.length == 1) {
            pair[1] = pair[0];
            pair[0] = "default";
        }

        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
        } else if (typeof query_string[pair[0]] === "string") {
            query_string[pair[0]] = [query_string[pair[0]], decodeURIComponent(pair[1])];
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
};

var makeHash = function (data) {
    var hash = '';
    for (var key in data)
        if (data.hasOwnProperty(key))
            hash += "&" + ((key !== "default") ? key + "=" : '') + data[key];
    if (hash.length > 0)
        hash = hash.substr(1);
    return hash;
};

var setHashValue = function (key, value) {
    var hashArgs = parseHash();
    hashArgs[key] = value;
    window.location.hash = makeHash(hashArgs);
};

var deleteHashValue = function (key) {
    var hashArgs = parseHash();
    delete hashArgs[key];
    window.location.hash = makeHash(hashArgs);
};

var getHashValue = function (key) {
    return parseHash()[key];
};

if (!String.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/\{(\d+)\}/g, function (m, n) {
            return args[n] ? args[n] : m;
        });
    };
}


jQuery('img.svg').each(function () {
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    jQuery.get(imgURL, function (data) {
        // Get the SVG tag, ignore the rest
        var $svg = jQuery(data).find('svg');

        // Add replaced image's ID to the new SVG
        if (typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if (typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass + ' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Replace image with new SVG
        $img.replaceWith($svg);

    }, 'xml');

});


var $completeModal = $("#completeModal");
$(function () {
    if (getHashValue("success") === "True") {
        var formName=getHashValue('form_name');
        var $modalWindow=$completeModal;
        if(formName==='checkout')
            $modalWindow=$('#saveBirthdayModal');
        else {
            $completeModal.addClass(formName);
             setTimeout(function () {
                $modalWindow.modal("hide");
            }, 5000);
        }
        $modalWindow.modal("show");
        deleteHashValue("success");
        deleteHashValue("form_name");
    }
});