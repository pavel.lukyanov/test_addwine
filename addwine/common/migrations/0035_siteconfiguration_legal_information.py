# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-01-31 20:06
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0034_siteconfiguration_rules_offert'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfiguration',
            name='legal_information',
            field=ckeditor.fields.RichTextField(blank=True, default='', verbose_name='Юридическая информация'),
        ),
    ]
