# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-09-08 15:46
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0016_pageconfigsseotext'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='pageconfigsseotext',
            options={'verbose_name': 'SEO текст', 'verbose_name_plural': 'SEO тексты'},
        ),
        migrations.AlterField(
            model_name='pageconfigsseotext',
            name='page_configs',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='seo_texts', to='common.PageConfigs', verbose_name='страница'),
        ),
        migrations.AlterField(
            model_name='pageconfigsseotext',
            name='seo_text',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='page_configs', to='marketing.SEOText', verbose_name='SEO тект'),
        ),
    ]
