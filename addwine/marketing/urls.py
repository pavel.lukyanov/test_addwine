from django.conf.urls import url

from common.views import get_admin_autocomplete_view
from marketing.models import MailCTA, SommelierCTA, SEOText, SEOTextCategory
from marketing.views import MailCTAFormView, SommelierCTAFormView

urlpatterns = [
    url(r'autocomplete/marketing/mail_cta', get_admin_autocomplete_view(MailCTA, 'title').as_view()
        , name='autocomplete_mail_cta'),
    url(r'autocomplete/marketing/sommelier_cta', get_admin_autocomplete_view(SommelierCTA, 'title').as_view()
        , name='autocomplete_sommelier_cta'),
    url(r'autocomplete/marketing/seo_text$', get_admin_autocomplete_view(SEOText, 'title').as_view()
        , name='autocomplete_seo_text'),
    url(r'autocomplete/marketing/seo_text_category', get_admin_autocomplete_view(SEOTextCategory, 'title').as_view()
        , name='autocomplete_seo_text_category'),
    url(r'^mail_cta$', MailCTAFormView.as_view(), name='mail-cta'),
    url(r'^sommelier_cta$', SommelierCTAFormView.as_view(), name='sommelier-cta'),
]