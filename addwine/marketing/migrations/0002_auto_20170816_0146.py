# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-08-15 22:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('marketing', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailcta',
            name='text',
            field=models.TextField(max_length=2048, verbose_name='Текст'),
        ),
        migrations.AlterField(
            model_name='sommeliercta',
            name='text',
            field=models.TextField(max_length=2048, verbose_name='Текст'),
        ),
    ]
