from django import forms
from django.core.exceptions import ValidationError

from catalogue.models import Product, Category
from customer.forms import ContactsForm
from marketing.models import MailCTA, SommelierCTA
from offer.models import Range


class MailCTAForm(forms.Form):
    cta = forms.ModelChoiceField(MailCTA.objects.all())
    email = forms.EmailField(max_length=255)

class SommelierCTAForm(forms.Form):
    cta = forms.ModelChoiceField(SommelierCTA.objects.all())
    category = forms.ModelChoiceField(Category.objects.all(), required=False)
    range = forms.ModelChoiceField(Range.objects.all(), required=False)
    product = forms.ModelChoiceField(Product.objects.all(), required=False)
    contact = forms.CharField(max_length=255)

    def clean_contact(self):
        contact = self.cleaned_data['contact']
        contacts_form = ContactsForm({'email': contact, 'phone_number': contact})
        contacts_form.is_valid()

        email = contacts_form.cleaned_data.get('email')
        phone_number = contacts_form.cleaned_data.get('phone_number')
        if not email and not phone_number:
            raise ValidationError(
                'Invalid value for field contact: %(value)s must be phone_number or email',
                params={'value': contact},
            )

        if email:
            self.contact_type = 'email'
            return email
        if phone_number:
            self.contact_type = 'phone_number'
            return phone_number
