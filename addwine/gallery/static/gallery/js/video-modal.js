function VideoModal(videoModal){

    var $video=videoModal.find("video");
    var $source=$("<source></source>");

    var videoPlayer=$video[0];

    var _showVideo=function(url){
        $source.attr("src", url);
        if($source.parent().length==0)
            $video.append($source);
        _playVideo();
        videoModal.modal("show");
    };

    var _playVideo=function(){
        videoPlayer.load();
        videoPlayer.play();
    };

    var _stopVideo=function(){
        videoPlayer.pause();
    };


    videoModal.on('hidden.bs.modal', function(){
        _stopVideo();
    });

    this.showVideo=_showVideo;
    this.playVideo=_playVideo;
    this.stopVideo=_stopVideo;
}
