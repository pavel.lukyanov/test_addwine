/*
    Mark target node with class youtube-video-source
    and
    set attribute data-youtube-source
 */

function YoutubeVideoModal(videoModal){
    var _showVideo=function(url){
        videoModal.find("iframe").attr("src", url);
        videoModal.modal("show");
    };

    var _stopVideo=function(){
        videoModal.find("iframe").attr("src", '');
    };


    videoModal.on('hidden.bs.modal', function(){
        _stopVideo();
    });

    this.showVideo=_showVideo;
    this.stopVideo=_stopVideo;
}

$(function () {
    var $videoModal=$("#youtube-video-modal");
    var videoModalView=new YoutubeVideoModal($videoModal);
    $(document).on('click', '.youtube-video-source', function () {
        var that=$(this);
        videoModalView.showVideo(that.attr('data-youtube-source'));
    });
});
