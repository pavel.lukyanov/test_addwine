import os

from django.conf import settings
from celery import Celery

# set the default Django settings module for the 'celery' program.
if not settings.configured:
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings.development')


app = Celery('addwine', backend=settings.BROKER_BACKEND, broker=settings.CELERY_BROKER_URL)

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

app.conf.update(CELERY_ALWAYS_EAGER=settings.CELERY_ALWAYS_EAGER)
app.conf.update(CELERY_EAGER_PROPAGATES_EXCEPTIONS=settings.CELERY_EAGER_PROPAGATES_EXCEPTIONS)

app.conf.task_queues = settings.CELERY_QUEUES
app.conf.task_routes = settings.CELERY_ROUTES

app.conf.task_default_queue = settings.CELERY_DEFAULT_QUEUE
app.conf.task_default_exchange = settings.CELERY_DEFAULT_EXCHANGE
app.conf.task_default_routing_key = settings.CELERY_DEFAULT_ROUTING_KEY

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
