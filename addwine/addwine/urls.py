"""addwine URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

from addwine.app import application
from web.views import Error404PageView, admin_index_override
from search.views_api import SearchResultView

handler404 = Error404PageView.as_view()

urlpatterns = [
    url(r'admin/$', admin_index_override, name='index', ),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^', include('common.urls')),
    url(r'^', include('sommeliers.urls')),
    url(r'^api/', include('search.urls')),
    url(r'^search/load/(?P<query>[\w ]+)', SearchResultView.as_view(), name='product-search-load'),
    url(r'^', include('recommendation_emails.urls')),
    url(r'', include(application.urls)),


    url(r'^utils/marketing/', include('marketing.urls')),
    url(r'^utils/catalogue/', include('catalogue.urls')),
    url(r'^utils/catalogue/', include('offer.urls')),
    url(r'^utils/checkout/', include('checkout.urls')),
    url(r'^utils/news/', include('news.urls', namespace='news')),
    url(r'^utils/customer_extension/', include('customer.urls', namespace='customer_extension')),
    url(r'^utils/feedback/', include('feedback.urls', namespace='feedback')),
    url(r'^utils/basket/', include('basket.urls', namespace='basket_extension')),
    url(r'^utils/ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^utils/ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^utils/vouchers/', include('voucher.urls', namespace='voucher_extension')),
    url(r'^utils/sommelier_bot/', include('sommelier_bot.urls', namespace='sommelier_bot')),
    url(r'^utils/amocrm_api/', include('amocrm_api.urls', namespace='amocrm_api')),
    url(r'^utils/', include('yandex_market.urls', namespace='yandex_market')),

    url(r'^', include('web.urls')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns = [
                      url(r'^__debug__/', include(debug_toolbar.urls)),
                  ] + urlpatterns