from . import tasks


def notify_about_task(sommelier_task):
    return tasks.notify_about_task.delay(sommelier_task.id)


def send_message(chat_id, text):
    return tasks.send_message.delay(chat_id, text)
