from django.contrib import admin, messages
from django.http import HttpResponseRedirect
from django.template import engines
from django.urls import reverse
from django.utils.html import format_html

from common.utils import get_admin_settings
from sommelier_bot.bot_methods import send_message
from sommelier_bot.models import TelegramMessageTemplate, TelegramMessageTemplateTestVariable


class TelegramMessageTemplateTestVariableInlineAdmin(admin.TabularInline):
    model = TelegramMessageTemplateTestVariable
    readonly_fields = ('code', 'description')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(TelegramMessageTemplate)
class TelegramMessageTemplateAdmin(admin.ModelAdmin):
    fields = ('name', 'text')
    list_display = ('name', 'code')
    inlines = [TelegramMessageTemplateTestVariableInlineAdmin, ]

    def _process_test_variable(self, variable, context):
        codes = variable.code.split('.')
        parent_dict = {}
        current_dict = context
        last_code = codes.pop()
        for code in codes:
            if not current_dict.get(code):
                current_dict[code] = {}
            current_dict = current_dict[code]

        current_dict[last_code] = variable.value

    def _generate_preview_context(self, obj):
        context = {}
        for test_variable in obj.test_variables.all():
            self._process_test_variable(test_variable, context)
        return context

    def _render_preview(self, obj, context):
        template = engines['django'].from_string(obj.text)
        html = template.render(context)
        return html

    def is_make_preview_request(self, request):
        return '_make_preview' in request.POST

    def is_send_test_message(self, request):
        return '_send_test_message' in request.POST

    def _display_require_telegram_test_channel_id_error_message(self, request):
        message = format_html(
            'Сообщение не было отправлено. Вы должны указать "ID тестового канала в Telegram" в <a href="{}" target="_blank">настройках '
            'сайта</a>', reverse('admin:common_settings_change'))
        messages.error(request, message)

    def save_model(self, request, obj, form, change):
        if self.is_send_test_message(request):
            admin_settings = get_admin_settings()
            if admin_settings.telegram_test_channel_id:
                send_message(admin_settings.telegram_test_channel_id,
                             self._render_preview(obj, self._generate_preview_context(obj)))
            else:
                self._display_require_telegram_test_channel_id_error_message(request)
        super().save_model(request, obj, form, change)

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        if request.GET.get('preview') and object_id:
            obj = TelegramMessageTemplate.objects.get(id=object_id)
            extra_context = {
                'text': self._render_preview(obj, self._generate_preview_context(obj))
            }
        return super().changeform_view(request, object_id, form_url, extra_context)

    def response_change(self, request, obj):
        if self.is_make_preview_request(request) or self.is_send_test_message(request):
            postfix = ''
            if self.is_make_preview_request(request):
                postfix = "?preview=True"
            return HttpResponseRedirect(reverse(
                'admin:sommelier_bot_telegrammessagetemplate_change', args=(obj.pk,)) + postfix)

        return super().response_change(request, obj)

    class Media:
        css = {
            'all': ('admin/sommelier_bot/css/change_form.css',)
        }
