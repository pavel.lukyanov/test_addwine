from django.conf import settings
from django.conf.urls import url

from sommelier_bot.views import webhook

urlpatterns = [
    url(r'webhook/' + settings.TELEGRAM_BOT_TOKEN, webhook, name='webhook'),
]
