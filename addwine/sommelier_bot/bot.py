from sommelier_bot import commands
from django.conf import settings
from django.contrib.sites.models import Site
from django.urls import reverse
from telegram import Bot
from telegram.ext import Dispatcher, CommandHandler
import logging

TELEGRAM_BOT = None



logger = logging.getLogger(__name__)

def setup_bot():
    global TELEGRAM_BOT
    bot = Bot(settings.TELEGRAM_BOT_TOKEN)
    bot.dispatcher = Dispatcher(bot, None, workers=0)

    # Register handlers
    handler = CommandHandler('start', commands.start)
    bot.dispatcher.add_handler(handler)

    if settings.TELEGRAM_BOT_SET_WEBHOOK:
        bot.set_webhook(
            url=''.join(['https://', Site.objects.get_current().domain, reverse('sommelier_bot:webhook')])
        )

    TELEGRAM_BOT = bot
