from django.db import models


class AbstractAmoCRMFields(models.Model):
    amocrm_id = models.BigIntegerField(verbose_name='AmoCRM id', default=-1)

    class Meta:
        abstract = True


class AbstractAmoCRMNotesMixin(models.Model):
    amocrm_notes_id = models.BigIntegerField(verbose_name='AmoCRM notes id', default=-1)

    @property
    def element_id(self):
        raise NotImplementedError('Subclasses must implement property element_id')

    class Meta:
        abstract = True
