import factory


class UserFactory(factory.django.DjangoModelFactory):
    email = factory.Sequence(lambda n: 'email-{0}@example.com'.format(n))
    phone_number = factory.Sequence(lambda n: '+7999{:07d}'.format(n))
    password = factory.PostGenerationMethodCall('set_password', 'password')

    class Meta:
        model = 'customer.User'
