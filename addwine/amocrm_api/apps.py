from django.apps import AppConfig


class AmocrmApiConfig(AppConfig):
    name = 'amocrm_api'
