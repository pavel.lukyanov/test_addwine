import json
import sys

import time
from celery import shared_task
from celery.exceptions import MaxRetriesExceededError
from celery.utils.log import get_task_logger
from django.apps import apps
from django.conf import settings
from django.db import transaction

from amocrm_api.api.contacts import ContactsApi
from amocrm_api.api.leads import OrderLeadsApi, BackCallLeadsApi, SommelierTaskLeadsApi, \
    SubscriptionForDiscountLeadsApi, ProductAlertLeadsApi, FAQQuestionLeadsApi, BirthdayLeadsApi
from amocrm_api.api.notes import NotesApi

logger = get_task_logger(__name__)

TASKS_CONFIGS_API_REQUEST = {
    'ignore_result': True,
    'default_retry_delay': 1,
    'retry_kwargs': {'max_retries': 5},
}


def create_task_config(name):
    config = {
        'name': name,
    }
    config.update(TASKS_CONFIGS_API_REQUEST)
    return config


TASKS_CONFIGS_SYNC_CONTACT = create_task_config("amocrm.sync_contact")
TASKS_CONFIGS_SYNC_ORDER = create_task_config("amocrm.sync_order")
TASKS_CONFIGS_SYNC_ORDER_ADDRESS_NOTE = create_task_config("amocrm.sync_order_address")
TASKS_CONFIGS_SYNC_ORDER_CONTACT_NOTE = create_task_config("amocrm.sync_order_contact")
TASKS_CONFIGS_SYNC_BACKCALL = create_task_config("amocrm.sync_backcall")
TASKS_CONFIGS_SYNC_SOMMELIER_CTA = create_task_config("amocrm.sommelier_cta")
TASKS_CONFIGS_SYNC_QUESTION = create_task_config("amocrm.question")
TASKS_CONFIGS_SYNC_SUBSCRIPTION = create_task_config("amocrm.subscription_for_discount")
TASKS_CONFIGS_SYNC_PRODUCT_ALERT = create_task_config("amocrm.product_alert")
TASKS_CONFIGS_SYNC_BIRTHDAY = create_task_config("amocrm.sync_birtday")


class BaseApiTaskRequest(object):
    api_class = None
    model_class = None

    amocrm_instance_name = None
    model_name = None

    attr_name = 'amocrm_id'

    def __init__(self):
        super().__init__()
        self.model_class = apps.get_model(*self.model_name.split('.'))

    def get_model_class(self):
        return apps.get_model(*self.model_name.split('.'))

    def get_model_instance(self, id):
        try:
            instance = self.model_class.objects.get(pk=id)
        except self.model_class.DoesNotExist:
            return None

        return instance

    def process(self, id, task):
        instance = self.get_model_instance(id)

        if instance is None:
            logger.error('Trying to sync %s of not existing %s.' % (self.amocrm_instance_name, self.model_name))
            return

        try:
            api = self.api_class()
            amocrm_obj_id = getattr(instance, self.attr_name)
            if amocrm_obj_id != -1:
                resp = self.update(api, instance, amocrm_obj_id)
                if resp is None:
                    logger.error(
                        'Unable to update amocrm %s with id %s '
                        % (self.amocrm_instance_name, amocrm_obj_id,)
                    )
            else:
                resp = self.add(api, instance)
                if resp:
                    resp = resp[0]
                    setattr(instance, self.attr_name, resp['id'])
                else:
                    logger.error(
                        'Unable to add amocrm %s for %s with ID %s'
                        % (self.amocrm_instance_name, self.model_name, instance.id,)
                    )
        except Exception as e:
            # If something get wrong just catch it and then unlock model
            logger.error("Unexpected error", exc_info=e)
            resp = None

        instance.save()

        if resp:
            return instance

        try:
            retry_kwargs = TASKS_CONFIGS_API_REQUEST['retry_kwargs'].copy()
            retry_kwargs['countdown'] = TASKS_CONFIGS_API_REQUEST['default_retry_delay'] * task.request.retries
            task.retry(**retry_kwargs)
        except MaxRetriesExceededError as e:
            logger.exception("Unable to sync %s with id %s" % (self.model_name, instance.id))
            return None

    def add(self, api, instance):
        raise NotImplementedError('Subclasses must implement method add')

    def update(self, api, instance, amocrm_id):
        raise NotImplementedError('Subclasses must implement method update')


class ContactsApiTaskRequest(BaseApiTaskRequest):
    api_class = ContactsApi

    amocrm_instance_name = 'Contact'
    model_name = 'customer.User'

    def get_leads_ids(self, instance):
        ids = list(instance.orders.all().exclude(amocrm_id=-1).values_list('amocrm_id', flat=True))
        ids.extend(instance.contacts.all().exclude(amocrm_id=-1).values_list('amocrm_id', flat=True))
        return ids

    def add(self, api, instance):
        leads_ids = self.get_leads_ids(instance)
        return api.add(instance.get_full_name(),
                       instance.get_all_phone_numbers(), instance.get_all_emails(),
                       instance.birthday, instance.address, leads_ids)

    def update(self, api, instance, amocrm_id):
        leads_ids = self.get_leads_ids(instance)
        ids = ContactsApi().get_contacts_leeds_id(instance.amocrm_id)
        leads_ids.extend(ids)
        return api.update(amocrm_id, instance.get_full_name(),
                          instance.get_all_phone_numbers(), instance.get_all_emails(),
                          instance.birthday, instance.address, list(set(leads_ids)))

    def update_with_leads(self, api, instance, amocrm_id, lead_ids):
        leads_ids = self.get_leads_ids(instance)
        leads_ids.extend(lead_ids)
        return api.update(amocrm_id, instance.get_full_name(),
                          instance.get_all_phone_numbers(), instance.get_all_emails(),
                          instance.birthday, instance.address, list(set(leads_ids)))


class BaseNotesApiTaskRequest(BaseApiTaskRequest):
    """model_name must inherit from AbstractAmoCRMNotesMixin
    and override element_id property method
    """

    api_class = NotesApi
    amocrm_instance_name = 'Note'
    attr_name = 'amocrm_notes_id'

    def add(self, api, instance):
        return api.add_lead_comment(
            instance.element_id,
            instance.notes
        )

    def update(self, api, instance, amocrm_id):
        return api.update_lead_comment(
            amocrm_id,
            instance.notes
        )


class ContactsNotesApiTaskRequest(BaseNotesApiTaskRequest):
    model_name = 'order.ShippingContacts'


class AddressNotesApiTaskRequest(BaseNotesApiTaskRequest):
    model_name = 'order.ShippingAddress'


class BirthdayLeadApiTaskRequest(ContactsApiTaskRequest):
    api_class = BirthdayLeadsApi
    amocrm_instance_name = 'Lead'
    model_name = 'customer.User'

    def update(self, api, instance, amocrm_id):
        resp = super(BirthdayLeadApiTaskRequest, self).update(api, instance, amocrm_id)
        ContactsApiTaskRequest().update_with_leads(ContactsApi(), instance, amocrm_id, [resp[0]['id']])
        return resp


class LeadsApiTaskRequest(BaseApiTaskRequest):
    api_class = OrderLeadsApi

    amocrm_instance_name = 'Lead'
    model_name = 'order.Order'

    def add(self, api, instance):
        return api.add(
            str(instance),
            OrderLeadsApi.STATUS_PIPELINE[instance.status],
            instance.basket_total_excl_tax,
            price_retail=instance.total_retail,
            site_id=instance.id,
            admin_url=instance.get_admin_url(),
            address=instance.address,
            phone=instance.phone,
            fio=instance.fio,
            date_delivery=instance.shipping_date
        )

    def update(self, api, instance, amocrm_id):
        return api.update(
            amocrm_id,
            str(instance),
            OrderLeadsApi.STATUS_PIPELINE[instance.status],
            instance.basket_total_excl_tax,
            price_retail=instance.total_retail,
            site_id=instance.id,
            admin_url=instance.get_admin_url(),
            address=instance.address,
            phone=instance.phone,
            fio=instance.fio,
            date_delivery=instance.shipping_date
        )


class SiteInstanceTaskRequest(BaseApiTaskRequest):
    amocrm_instance_name = 'Lead'

    def __init__(self, api_class, model_name):
        self.api_class = api_class
        self.model_name = model_name
        super().__init__()

    def add(self, api, instance):
        return api.add(
            '#%s' % instance.id,
            instance.get_admin_url()
        )

    def update(self, api, instance, amocrm_id):
        return api.update(
            amocrm_id,
            '#%s' % instance.id,
            instance.get_admin_url()
        )


@shared_task(**TASKS_CONFIGS_SYNC_CONTACT)
def sync_contact_task(user_id, address=None):
    ContactsApiTaskRequest().process(user_id, sync_contact_task)


@shared_task(**TASKS_CONFIGS_SYNC_ORDER_CONTACT_NOTE)
def sync_lead_shipping_contact_notes_task(shipping_contacts_id):
    ContactsNotesApiTaskRequest().process(shipping_contacts_id, sync_lead_shipping_contact_notes_task)


@shared_task(**TASKS_CONFIGS_SYNC_ORDER_ADDRESS_NOTE)
def sync_lead_shipping_address_notes_task(shipping_address_id):
    AddressNotesApiTaskRequest().process(shipping_address_id, sync_lead_shipping_address_notes_task)


@shared_task(**TASKS_CONFIGS_SYNC_ORDER)
def sync_lead_task(order_id):
    instance = LeadsApiTaskRequest().process(order_id, sync_lead_task)
    if instance:
        from order.models import ShippingAddress, ShippingContacts

        if instance.client_id:
            sync_contact_task.delay(instance.client_id)

        try:
            if instance.shipping_address.notes or instance.shipping_address.amocrm_notes_id:
                sync_lead_shipping_address_notes_task.delay(instance.shipping_address.id)
        except ShippingAddress.DoesNotExist:
            pass

        try:
            if instance.shipping_contacts.notes or instance.shipping_contacts.amocrm_notes_id:
                sync_lead_shipping_contact_notes_task.delay(instance.shipping_contacts.id)
        except ShippingContacts.DoesNotExist:
            pass


def __sync_contacts_instance(contacts_instance):
    user = contacts_instance.recognize_user()

    if user:
        contacts_instance.recognized_user = user
        contacts_instance.save()
        sync_contact_task.apply_async((user.id,), countdown=1)


@shared_task(**TASKS_CONFIGS_SYNC_BIRTHDAY)
def sync_birthday_task(user_id):
    BirthdayLeadApiTaskRequest().process(user_id, sync_birthday_task)


@shared_task(**TASKS_CONFIGS_SYNC_BACKCALL)
def sync_backcall_task(backcall_id):
    instance = SiteInstanceTaskRequest(BackCallLeadsApi, 'feedback.BackCall').process(backcall_id, sync_backcall_task)
    if instance:
        __sync_contacts_instance(instance)


@shared_task(**TASKS_CONFIGS_SYNC_SOMMELIER_CTA)
def sync_sommelier_cta_task(sommelier_task_id):
    instance = SiteInstanceTaskRequest(SommelierTaskLeadsApi, 'sommeliers.SommelierTask').process(sommelier_task_id,
                                                                                                  sync_sommelier_cta_task)
    if instance:
        __sync_contacts_instance(instance.contacts)


@shared_task(**TASKS_CONFIGS_SYNC_QUESTION)
def sync_question_task(question_id):
    instance = SiteInstanceTaskRequest(FAQQuestionLeadsApi, 'feedback.Question').process(question_id,
                                                                                         sync_question_task)
    if instance:
        __sync_contacts_instance(instance)


@shared_task(**TASKS_CONFIGS_SYNC_SUBSCRIPTION)
def sync_subscription_for_discount_task(subscription_for_discount_id):
    instance = SiteInstanceTaskRequest(SubscriptionForDiscountLeadsApi, 'offer.SubscriptionForDiscount').process(
        subscription_for_discount_id, sync_subscription_for_discount_task)
    if instance:
        __sync_contacts_instance(instance)


@shared_task(**TASKS_CONFIGS_SYNC_PRODUCT_ALERT)
def sync_product_alert_task(product_alert_id):
    instance = SiteInstanceTaskRequest(ProductAlertLeadsApi, 'customer.ProductAlert').process(product_alert_id,
                                                                                              sync_product_alert_task)
    if instance:
        __sync_contacts_instance(instance)
