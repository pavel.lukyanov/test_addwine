from django.conf import settings

from amocrm_api.tasks import sync_contact_task, sync_lead_task, sync_backcall_task, sync_sommelier_cta_task, \
    sync_question_task, sync_subscription_for_discount_task, sync_product_alert_task, sync_birthday_task


def amocrm_sync_user(instance_id):
    if settings.AMOCRM_SYNC_ENABLED:
        sync_contact_task.apply_async(args=(instance_id,), countdown=1)


def amocrm_sync_birthday(instance_id):
    if settings.AMOCRM_SYNC_ENABLED:
        sync_birthday_task.apply_async(args=(instance_id,), countdown=1)


def amocrm_sync_order(instance_id):
    if settings.AMOCRM_SYNC_ENABLED:
        sync_lead_task.apply_async(args=(instance_id,), countdown=1)


def amocrm_sync_backcall(instance_id):
    if settings.AMOCRM_SYNC_ENABLED:
        sync_backcall_task.apply_async(args=(instance_id,), countdown=1)


def amocrm_sync_sommelier_cta(instance_id):
    if settings.AMOCRM_SYNC_ENABLED:
        sync_sommelier_cta_task.apply_async(args=(instance_id,), countdown=1)


def amocrm_sync_question(instance_id):
    if settings.AMOCRM_SYNC_ENABLED:
        sync_question_task.apply_async(args=(instance_id,), countdown=1)


def amocrm_sync_subscription_for_discount(instance_id):
    if settings.AMOCRM_SYNC_ENABLED:
        sync_subscription_for_discount_task.apply_async(args=(instance_id,), countdown=1)


def amocrm_sync_product_alert(order_id):
    if settings.AMOCRM_SYNC_ENABLED:
        sync_product_alert_task.apply_async(args=(order_id,), countdown=1)
