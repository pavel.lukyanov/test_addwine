from django.conf import settings
from django.conf.urls import url

from amocrm_api.views import webhook

urlpatterns = [
    url('webhook/LkXbeFn5Mqo7BeaE', webhook, name='webhook'),
]