import logging
from amocrm_api.api.base import Client
from time import time

logger = logging.getLogger(__name__)


class NotesApi(Client):
    """Notes object api
    Docs: https://www.amocrm.ru/developers/content/api/note
    """
    ENDPOINT = 'notes'

    # Виды событий
    COMMON = 4  # Обычное примечание

    # Типы сущностей
    LEAD_TYPE = 2  # Сделка

    def add_lead_comment(self, lead_id, text):
        return self._add(lead_id, self.LEAD_TYPE, text, self.COMMON)

    def update_lead_comment(self, note_id, text):
        return self._update(note_id, text)

    def _add(self, element_id, element_type, text, note_type):
        data = {
            'element_id': element_id,
            'element_type': element_type,
            'text': text,
            'note_type': note_type,
        }
        resp = self.send('POST', self.TYPE_SET, self.OPERATION_ADD, [data])
        return resp

    def _update(self, note_id, text):
        contact_data = {
            'id': note_id,
            'last_modified': time(),
            'text': text,
        }
        resp = self.send('POST', self.TYPE_SET, self.OPERATION_UPDATE, [contact_data])
        return resp
