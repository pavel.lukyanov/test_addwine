# Addwine

## Achtung

Jeden Tag um 3 Uhr morgens
wir starten **Supervosorctl restart all** 
�ber **crontab** vom **root** neu!

## Requirements

 - Redis server
 - Elastic Search 2.4.6
 - PostgreSQL
 - Python > 3.5

## Installation

Create virtual env and install python dependencies
```
virtualenv venv

source venv/bin/activate
pip install -r requirements/local.txt
```
Create .env file
```
cp addwine/env.example addwine/.env
```
Edit the .env file with your postgres credentials

Setup database
```
psql -U postgres
CREATE DATABASE addwine;
```
[Download dump file](https://storage.www.wrike.com/attachments_download/62497049?accountId=2091799)

Release the dump
```
psql -U postgres addwine < addwine_dump.sql
```
Open shell and run index into elastic
```
python addwine/manage.py shell_plus
```
```
from haystack.management.commands import update_index as update_index_command
update_index_command.Command().handle(using=['default'], interactive=False)
```
Now you are ready to start server
```
python addwine/manage.py runserver
```