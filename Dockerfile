FROM python:3.5.2

WORKDIR /addwine

COPY ./addwine/requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

ADD ./addwine ./addwine
ADD ./celery ./celery
ADD ./public_html ./public_html
ADD ./locale ./locale


