from .base import Gear, print_status


class Supervisorctl(Gear):
    def update(self):
        result = self.c.sudo('supervisorctl update')
        print_status(result)

    def restart_gunicorn(self):
        result = self.c.sudo('supervisorctl restart gunicorn')
        print_status(result)
