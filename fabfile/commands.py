from fabric import Connection, Config
from gears import Machine, Git, Manage, Supervisorctl
import os


def deploy():
    host_string = os.environ['HOST_STRING']
    sudo_password = os.environ['SUDO_PASSWORD']
    work_dir = '/var/www/'
    releases_dir = 'releases/'
    manage_dir = 'addwine'
    link_name = 'addwine.ru'
    venv_name = 'venv'

    config = Config(overrides={'sudo': {'password': sudo_password}})
    with Connection(host_string,  config=config) as c:
        # init gears
        machine = Machine(c, work_dir)
        git = Git(c)
        supervisorctl = Supervisorctl(c)
        # deploying steps
        with c.cd(work_dir):
            with c.cd(releases_dir):
                source_dir, new_dir = machine.make_seq_dir()
                machine.cp(source_dir, new_dir)
                with c.cd(new_dir):
                    git.pull()
                    with c.cd(manage_dir):
                        venv_path = machine.build_path(releases_dir, new_dir, venv_name)
                        manage = Manage(c, venv_path)
                        manage.migrate()
                        manage.collectstatic()
            target_path = machine.build_path(releases_dir, new_dir)
            machine.ln_fsn(link_name, target_path)
        supervisorctl.update()
        supervisorctl.restart_gunicorn()





